package app.ui;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.RecordDaily;
import app.ui.console.MainMenuUI;
import app.ui.console.utils.SaveReadCompany;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */

public class Main {

    public Main(){
    }

    public static void main(String[] args){

        Company company = App.getInstance().getCompany();

        //Objetos adicionados manualmente aos ArrayLists! Após a implementação da serialização, já não são precisos.
        /*
        company.getUserStore().addToArrayList();
        company.getVaccineTypeStore().addToArrayList();
        company.getVaccinationCenterStore().addToArrayList();
        company.getEmployeeStore().addToArrayList();

        company.getVaccinationScheduleStore().addToArrayList(company.getVaccinationCenterStore().getVaccinationCentersList().get(0), company.getVaccineTypeStore().getVaccineTypeList().get(0));
        company.getArrivalTimeStore().addToArrayList();
        company.getVaccineAdministrationStore().addToArrayList();
        */

        RecordDaily recordDaily = new RecordDaily();
        recordDaily.run();

        try {
            MainMenuUI menu = new MainMenuUI();

            menu.run();


        } catch (Exception e) {
            e.printStackTrace();
        }

        SaveReadCompany.saveCompany(company);

    }



}
