package app.ui.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * The type Vaccination statistics ui.
 */
public class VaccinationStatisticsUI extends Application implements Runnable {
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("VaccinationStatistics.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("Vaccination Statistics");
        stage.getIcons().add(new Image("images/DGS_logo.png"));
        stage.setScene(scene);
        stage.getScene().getStylesheets().add("Styles.css");
        stage.show();
    }

    @Override
    public void run() {
        try {
            start(new Stage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
