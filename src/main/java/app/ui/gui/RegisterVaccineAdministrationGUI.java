package app.ui.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.Serializable;

/**
 * The type Register vaccine administration gui.
 */
public class RegisterVaccineAdministrationGUI extends Application implements Serializable, Runnable{

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("RegisterVaccineAdministration.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("Record Vaccine Administration");
        stage.getIcons().add(new Image("images/DGS_logo.png"));
        stage.setScene(scene);
        stage.getScene().getStylesheets().add("Styles.css");
        stage.show();
        stage.setAlwaysOnTop(true);

    }

    @Override
    public void run() {
        try {
            start(new Stage());

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
