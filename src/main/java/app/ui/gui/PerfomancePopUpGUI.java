package app.ui.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.Serializable;

/**
 * The type Perfomance pop up gui.
 */
public class PerfomancePopUpGUI extends Application implements Runnable, Serializable {
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("PerformanceListPopUp.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("Performance Analysis");
        stage.getIcons().add(new Image("images/DGS_logo.png"));
        stage.setScene(scene);
        stage.getScene().getStylesheets().add("PerformanceStyle.css");
        stage.show();
    }

    @Override
    public void run() {
        try {
            start(new Stage());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
