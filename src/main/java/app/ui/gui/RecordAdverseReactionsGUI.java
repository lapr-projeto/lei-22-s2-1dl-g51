package app.ui.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.Serializable;

/**
 * The type Record adverse reactions gui.
 */
public class RecordAdverseReactionsGUI extends Application implements Runnable, Serializable {

    @Override
    public void start(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("RecordAdverseReactions.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("Record Adverse Reactions");
        stage.getIcons().add(new Image("images/DGS_logo.png"));
        stage.setScene(scene);
        stage.getScene().getStylesheets().add("Styles.css");
        stage.show();
        stage.setAlwaysOnTop(true);
    }

    @Override
    public void run() {
        try {
            start(new Stage());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
