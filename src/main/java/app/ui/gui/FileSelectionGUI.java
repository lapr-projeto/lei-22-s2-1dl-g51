package app.ui.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.Serializable;

/**
 * The type File selection gui.
 */
public class FileSelectionGUI extends Application implements Runnable, Serializable {
    @Override
    public void run() {
        try {
            start(new Stage());
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("FileSelection.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("Center Coordinator Menu");
        stage.getIcons().add(new Image("images/DGS_logo.png"));
        stage.setScene(scene);
        stage.getScene().getStylesheets().add("StylesFile.css");
        stage.show();
        stage.setAlwaysOnTop(true);
    }
}
