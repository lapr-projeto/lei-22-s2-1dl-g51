package app.ui.gui;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.RecordDaily;
import app.ui.console.utils.SaveReadCompany;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.Serializable;

/**
 * The type Login gui.
 */
public class LoginGUI extends Application implements Runnable, Serializable {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("Login.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("Login");
        stage.getIcons().add(new Image("images/DGS_logo.png"));
        stage.setScene(scene);
        stage.getScene().getStylesheets().add("Styles.css");
        stage.show();
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {

        Company company = App.getInstance().getCompany();
        RecordDaily recordDaily = new RecordDaily();
        recordDaily.run();
/*        company.getUserStore().addToArrayList();
        company.getVaccineTypeStore().addToArrayList();
        company.getVaccinationCenterStore().addToArrayList();
        company.getEmployeeStore().addToArrayList();

        company.getVaccinationScheduleStore().addToArrayList(company.getVaccinationCenterStore().getVaccinationCentersList().get(0), company.getVaccineTypeStore().getVaccineTypeList().get(0));
        company.getArrivalTimeStore().addToArrayList();
        company.getVaccineAdministrationStore().addToArrayList();*/

        launch(args);
        SaveReadCompany.saveCompany(company);
    }

    @Override
    public void run() {
        try {
            start(new Stage());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
