package app.ui.console;

import app.controller.App;
import app.controller.VaccinationScheduleController;
import app.domain.model.Company;
import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.domain.model.VaccinationSchedule;
import app.domain.model.VaccineType;
import app.domain.model.store.VaccinationCenterStore;
import app.domain.model.store.VaccinationScheduleStore;
import app.domain.model.store.VaccineTypeStore;
import app.ui.console.utils.Utils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Represents the UI for the VaccinationSchedule
 */
public class VaccinationScheduleUI implements Runnable, Serializable {

    /**
     * The Schedule.
     */
    protected VaccinationSchedule schedule;
    /**
     * The Vs controller.
     */
    protected VaccinationScheduleController vsController;
    /**
     * The Correct info.
     */
    protected boolean correctInfo = false;

    /**
     * Runs the UI for the VaccinationSchedule
     * Asks to be typed in the console the sns user number and the date and to be selected the VaccineType and VaccinationCenter
     */
    @Override
    public void run() {
        Company company = App.getInstance().getCompany();
        VaccinationCenterStore vaccinationCenterStore = company.getVaccinationCenterStore();
        VaccineTypeStore vaccineTypeStore = company.getVaccineTypeStore();
        VaccinationScheduleStore vaccinationScheduleStore = company.getVaccinationScheduleStore();

        vsController = new VaccinationScheduleController();
        VaccineType vaccineType = null;
        VaccinationCenter vaccinationCenter = null;
        String time = null;
        String snsUserNumber = Utils.getValidSNSNumber();
        if (vsController.checkUserRegistration(snsUserNumber)) {
            do {
                vaccineType = (VaccineType) Utils.showAndSelectOne(vaccineTypeStore.getVaccineTypeList(), "\n\nChoose one of the Vaccine Types");
            } while (vaccineType == null);

            List<VaccinationCenter> vaccinationCenterListByVaccineType = vaccinationCenterStore.getVaccinationCentersWithGivenVaccineType(vaccineType);

            do {
                vaccinationCenter = (VaccinationCenter) Utils.showAndSelectOne(vaccinationCenterListByVaccineType, "\n\nChoose one of the Vaccination Centers");
            } while (vaccinationCenter == null);
            Date date = Utils.getValidScheduleDate("Date: (dd-MM-yyyy)");

            do {
                time = (String) Utils.showAndSelectOne(vaccinationScheduleStore.checkAvailableHours(vaccinationCenter.getVaccinationCenterDTO(), Utils.convertToSimpleDataFormat(date)), "\nChoose one of the hours available: ");
            }while (time == null);

            schedule = vsController.createVaccinationSchedule(snsUserNumber, vaccinationCenter, date, time, vaccineType);

            if (schedule != null) {
                System.out.printf("\n%s\n", schedule);
                if (Utils.confirm("Confirm the data? (y/n)")) {
                    vsController.saveVaccinationSchedule(schedule);
                    correctInfo = true;
                }
            } else {
                System.out.println("Invalid Vaccination Schedule!");
            }
        } else {
            System.out.println("SNS User has not been registered in the System yet!");
        }
    }

}
