package app.ui.console;

import app.controller.App;
import app.controller.VaccinationScheduleController;
import app.domain.model.Company;
import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.ui.console.utils.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents the UI when a Receptionist logs in
 */
public class ReceptionistUI implements Runnable, Serializable {

    private VaccinationCenter vaccinationCenter;
    private Company company;

    /**
     * Creates a new UI for the Receptionist
     */
    public ReceptionistUI() {
        company = App.getInstance().getCompany();
    }

    /**
     * Runs the UI of the Receptionist
     */
    public void run() {

        do {
            vaccinationCenter = (VaccinationCenter) Utils.showAndSelectOne(company.getVaccinationCenterStore().getVaccinationCentersList(), "Choose the vaccination center where you are working at: ");
        }while (vaccinationCenter == null);


        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("Register new SNS User", new ShowTextUI("You have selected to register a new SNS User")));
        options.add(new MenuItem("Schedule a new vaccination", new ShowTextUI("You have selected to schedule a new vaccination")));
        options.add(new MenuItem("Register the arrival of a SNS user to take the vaccine", new ShowTextUI("You have selected to Register the arrival of a SNS user to take the vaccine")));


        int option = 0;

        do {
            option = Utils.showAndSelectIndex(options, "\n\nReceptionist Menu:");
            if ((option >= 0) && (option < options.size())) {
                options.get(option).run();
            }
            if (option == 0){
                RegisterSNSUserUI snsRegister = new RegisterSNSUserUI();
                snsRegister.run();
            }
            if (option == 1){
                VaccinationScheduleByReceptionistUI vs = new VaccinationScheduleByReceptionistUI();
                vs.run();
            }
            if (option == 2){
                ArrivalTimeUI at = new ArrivalTimeUI(vaccinationCenter);
                at.run();
            }


        } while (option != -1);

    }
}