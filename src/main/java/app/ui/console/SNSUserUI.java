package app.ui.console;


import app.ui.console.utils.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Sns user ui.
 */
public class SNSUserUI implements Runnable, Serializable {

    /**
     * Instantiates a new Sns user ui.
     */
    public SNSUserUI() {
    }

    public void run() {
        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("Schedule a Vaccine", new ShowTextUI("You have chosen to schedule a vaccine.")));
        int option = 0;
        do {
            option = Utils.showAndSelectIndex(options, "\n\nSNS User Menu:");
            if ((option >= 0) && (option < options.size())) {
                options.get(option).run();
                if (option == 0) {
                    VaccinationScheduleBySNSUserUI vsUI = new VaccinationScheduleBySNSUserUI();
                    vsUI.run();
                }
            }
        } while (option != -1);
    }
}
