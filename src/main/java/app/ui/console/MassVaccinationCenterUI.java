package app.ui.console;

import app.controller.App;
import app.controller.VaccinationCenterController.MassVaccinationCenterController;
import app.domain.model.Company;
import app.domain.model.VaccineType;
import app.ui.console.utils.Utils;
import javafx.stage.Stage;

import java.io.Serializable;

/**
 * The type Mass vaccination center ui.
 */
public class MassVaccinationCenterUI extends VaccinationCenterUI implements Runnable, Serializable {
    @Override
    public void run() {
        super.run();
        Company company = App.getInstance().getCompany();

        VaccineType vaccineTypes = (VaccineType) Utils.showAndSelectOne(company.getVaccineTypeStore().getVaccineTypeList(), "Choose one of the Vaccine Types:");

        MassVaccinationCenterController ctrl = new MassVaccinationCenterController();

        if (ctrl.createMVC(name, address, phoneNumber, email, openHours, closingHours, vaccinesSlotCap, slotDuration, vaccineTypes)) {
            if (Utils.confirm("Confirm the data? (y/n)")) {
                ctrl.saveMVC();
                System.out.print("\n\nRegistration Complete");
            }
        } else {
            System.out.println("Error");
        }

    }

    @Override
    public void start(Stage stage) throws Exception {

    }
}
