package app.ui.console;

import app.controller.SpecifyNewVaccineTypeController;
import app.domain.model.VaccineTypes;
import app.ui.console.utils.Utils;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * The type Specify new vaccine type ui.
 */
public class  SpecifyNewVaccineTypeUI implements Runnable, Serializable {
    private SpecifyNewVaccineTypeController specifyNewVaccineTypeController = new SpecifyNewVaccineTypeController();

    public void run() {
        List<VaccineTypes> vaccineTypes = Arrays.asList(VaccineTypes.values());

        VaccineTypes vaccineTypes1;
        do {
            vaccineTypes1 = (VaccineTypes) Utils.showAndSelectOne(vaccineTypes, "Choose one of the Vaccine Types: ");
        }while (vaccineTypes1 == null);

        String code = Utils.readLineFromConsole("Code:\n");

        String designation = Utils.readLineFromConsole("Designation:\n");

        String whoID = Utils.readLineFromConsole("WhoID:\n");


        if(specifyNewVaccineTypeController.createVaccineType(vaccineTypes1,code,designation,whoID)){
            if(Utils.confirm("Confirm the data? (y/n)")) {
                specifyNewVaccineTypeController.saveVaccineType();
                System.out.println("Vaccine Type registered successfully!");
            }
        }else{
            System.out.println("Couldn't regist a new Vaccine Type");
        }


    }

}
