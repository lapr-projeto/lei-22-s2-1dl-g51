package app.ui.console;

import app.controller.RegisterSNSUserController;
import app.domain.model.SNSUser;
import app.ui.console.utils.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Represents the UI when a Receptionist wants to create a new SNS User
 */
public class RegisterSNSUserUI implements Runnable, Serializable {

    /**
     * Creates a new UI for the registration of a new SNS User.
     */
    public RegisterSNSUserUI() {
    }

    /**
     * Runs the UI from the registration of a new SNS User.
     * Asks to be typed in the console the name, address, phone number, email, gender, citizen card number, birthdate and
     * sns user number for the creation of the new SNS User
     */
    public void run() {
                    String name = Utils.readLineFromConsole("Name:");
                    String address = Utils.readLineFromConsole("Address:");
                    String phoneNumber = Utils.getValidPhoneNumber();
                    String email = Utils.getValidEmail();
                    String gender = Utils.getValidGender();
                    String citizenCardNumber = Utils.getValidCitizenCardNumber();
                    Date birthDate = Utils.readDateFromConsole("BirthDate (dd-MM-yyyy): ");
                    String snsUserNumber = Utils.getValidSNSNumber();

                    RegisterSNSUserController snsRegister = new RegisterSNSUserController();

                    SNSUser user = snsRegister.createSNSUser(name, address, phoneNumber, email, citizenCardNumber, gender, birthDate, snsUserNumber);
                    System.out.printf("\n%s\n", user);
                    if(Utils.confirm("Confirm the data? (y/n)"))
                        snsRegister.saveSNSUser(user);
                }
            }




