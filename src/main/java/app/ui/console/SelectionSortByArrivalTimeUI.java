package app.ui.console;

import app.controller.VaccineAdminstrationController;
import app.domain.model.VaccineAdministration;
import app.ui.console.utils.Utils;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * The type Selection sort by arrival time ui.
 */
public class SelectionSortByArrivalTimeUI implements Runnable, Serializable {
    private ArrayList<VaccineAdministration> vaList;

    /**
     * The Va controller.
     */
    VaccineAdminstrationController vaController= new VaccineAdminstrationController();


    /**
     * Instantiates a new Selection sort by arrival time ui.
     *
     * @param vaList the va list
     */
    public SelectionSortByArrivalTimeUI(ArrayList<VaccineAdministration> vaList) {
        this.vaList=vaList;
        vaController.selectionSortByArrivalTime(vaList);
    }


    @Override
    public void run() {

        Utils.showList(vaList,"\nSelection Sort by Arrival time...");

    }
}
