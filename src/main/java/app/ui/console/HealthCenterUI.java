package app.ui.console;

import app.controller.App;
import app.controller.VaccinationCenterController.HealthCenterController;
import app.domain.model.Company;
import app.domain.model.VaccineType;
import app.ui.console.utils.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Health center ui.
 */
public class HealthCenterUI extends VaccinationCenterUI implements Runnable, Serializable {
    @Override
    public void run() {
        super.run();
        Company company = App.getInstance().getCompany();
        ArrayList<VaccineType> vaccineTypeList = new ArrayList<>();
        List <VaccineType> copyOfCompanyVaccineTypeList = new ArrayList<>(company.getVaccineTypeStore().getVaccineTypeList());

        do {
            VaccineType vaccineTypes = (VaccineType) Utils.showAndSelectOne(copyOfCompanyVaccineTypeList, "\nChoose one of the Vaccine Types:");
            vaccineTypeList.add(vaccineTypes);
            copyOfCompanyVaccineTypeList.remove(vaccineTypes);
        }while (Utils.confirm("Add another vaccine type? (y/n)"));

        HealthCenterController ctrl=new HealthCenterController();

        if (ctrl.createHC(name, address, phoneNumber, email, openHours, closingHours, vaccinesSlotCap, slotDuration,vaccineTypeList)){
            if(Utils.confirm("Confirm the data? (y/n)")) {
                ctrl.saveHC();
                System.out.print("\n\nRegistration Complete");
            }
        } else {
            System.out.println("Error");
        }

    }
}
