package app.ui.console;

//import app.ui.console.NurseUI.RegisterVaccineAdministrationUI;
import app.ui.console.utils.Utils;
import app.ui.gui.PerformanceAnalysisUI;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Center coordinator ui.
 */
public class CenterCoordinatorUI implements Runnable, Serializable {
    @Override
    public void run() {
        List<MenuItem> options = new ArrayList<MenuItem>();

        options.add(new MenuItem("Import new Vaccination Center Data",new LoadCSVFileUI()));

        int option = 0;
        do
        {
            option = Utils.showAndSelectIndex(options, "\n\nCenter coordinator Menu");

            if ( (option >= 0) && (option < options.size()))
            {
                options.get(option).run();
                if (option == 0){
                    PerformanceAnalysisUI paUI = new PerformanceAnalysisUI();
                    paUI.run();
                }
            }
        }
        while (option != -1 );
    }
}
