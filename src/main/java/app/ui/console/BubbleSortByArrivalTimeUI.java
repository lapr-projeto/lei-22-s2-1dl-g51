package app.ui.console;

import app.controller.VaccineAdminstrationController;
import app.domain.model.VaccineAdministration;
import app.ui.console.utils.Utils;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * The type Bubble sort by arrival time ui.
 */
public class BubbleSortByArrivalTimeUI implements Runnable, Serializable {

    private ArrayList<VaccineAdministration> vaList;

    /**
     * The Va controller.
     */
    VaccineAdminstrationController vaController= new VaccineAdminstrationController();

    /**
     * Instantiates a new Bubble sort by arrival time ui.
     *
     * @param vaList the va list
     */
    public BubbleSortByArrivalTimeUI(ArrayList<VaccineAdministration> vaList) {
        this.vaList=vaList;
        vaController.bubbleSortByArrivalTime(vaList);
    }


    @Override
    public void run() {

        Utils.showList(vaList,"\nBubble Sort by arrival time...");
    }
}
