package app.ui.console.NurseUI;

import app.controller.ArrivalTimeController;
import app.controller.VaccinationCenterController.NurseController;
import app.domain.model.ArrivalTime;
import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.ui.console.MenuItem;
import app.ui.console.utils.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Users waiting ui.
 */
public class UsersWaitingUI implements Runnable, Serializable {

    private NurseController NurseCtrl= new NurseController();

    private ArrivalTimeController arrivalTimeCtrl= new ArrivalTimeController();

    private VaccinationCenter vc1;

    private List<VaccinationCenter> vc = NurseCtrl.getVcStore().getVaccinationCentersList();

    /**
     * Instantiates a new Users waiting ui.
     *
     * @param vc1 the vc 1
     */
    public UsersWaitingUI(VaccinationCenter vc1) {
        this.vc1 = vc1;
    }

    @Override
    public void run() {
        List<MenuItem> options = new ArrayList<MenuItem>();


        ArrayList<ArrivalTime> at = arrivalTimeCtrl.arrivalTimeByVC(vc1);

        if(!at.isEmpty()){
            Utils.showList(at,"\nUsers in the waiting room of "+ vc1.getVaccinationCenterDTO().getName());
        }
        else{
            System.out.println("\nThere are no users in the waiting room of "+ vc1.getVaccinationCenterDTO().getName());
        }

}

}

