package app.ui.console.NurseUI;

import app.controller.VaccinationCenterController.NurseController;
import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.ui.console.MenuItem;
import app.ui.console.utils.Utils;
import app.ui.gui.RegisterVaccineAdministrationGUI;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Nurse ui.
 */
public class NurseUI implements Runnable, Serializable {

    /**
     * The constant vaccinationCenter.
     */
    protected static VaccinationCenter vaccinationCenter;
    /**
     * The Ctrl.
     */
    protected NurseController ctrl=new NurseController();

    /**
     * Instantiates a new Nurse ui.
     */
    public NurseUI() {}


    @Override
    public void run() {

        do {
            vaccinationCenter = (VaccinationCenter) Utils.showAndSelectOne(ctrl.getVcStore().getVaccinationCentersList(), "Choose the vaccination center where you are working at: ");
        }while (vaccinationCenter == null);
        List<MenuItem> options = new ArrayList<MenuItem>();

        options.add(new MenuItem("Show patients in waiting room.",new UsersWaitingUI(vaccinationCenter)));
        options.add(new MenuItem("Record vaccine administration.",new RegisterVaccineAdministrationGUI()));
        int option = 0;
        do
        {
            option = Utils.showAndSelectIndex(options, "\n\nNurse Menu:");

            if ( (option >= 0) && (option < options.size()))
            {
                options.get(option).run();
            }
        }
        while (option != -1 );
    }

    /**
     * Gets vaccination center.
     *
     * @return the vaccination center
     */
    public static VaccinationCenter getVaccinationCenter() {
        return vaccinationCenter;
    }
}

