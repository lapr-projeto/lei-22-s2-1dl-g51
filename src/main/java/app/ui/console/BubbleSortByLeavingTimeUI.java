package app.ui.console;

import app.controller.VaccineAdminstrationController;
import app.domain.model.VaccineAdministration;
import app.ui.console.utils.Utils;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * The type Bubble sort by leaving time ui.
 */
public class BubbleSortByLeavingTimeUI implements Runnable, Serializable {

    /**
     * The Valist.
     */
    ArrayList<VaccineAdministration> valist;
    /**
     * The Ctrl.
     */
    VaccineAdminstrationController ctrl= new VaccineAdminstrationController();

    /**
     * Instantiates a new Bubble sort by leaving time ui.
     *
     * @param vaList the va list
     */
    public BubbleSortByLeavingTimeUI(ArrayList<VaccineAdministration> vaList) {
        valist=vaList;
        ctrl.bubbleSortByLeavingTime(valist);
    }

    @Override
    public void run() {
        Utils.showList(valist,"\nBubble Sort by leaving time...");
    }
}
