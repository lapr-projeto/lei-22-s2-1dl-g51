package app.ui.console;

import app.controller.LoadCSVFileController;

import java.io.File;
import java.io.Serializable;

/**
 * The type Create users ui.
 */
public class CreateUsersUI implements Runnable, Serializable {
    private File file;

    /**
     * Instantiates a new Create users ui.
     *
     * @param file the file
     */
    public CreateUsersUI (File file){
        this.file = file;
    }
    @Override
    public void run() {
        LoadCSVFileController controller = new LoadCSVFileController();
        controller.createSNSUsers(file);
    }
}
