package app.ui.console.utils;

import pt.isep.lei.esoft.auth.domain.model.Email;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public abstract class Utils implements Serializable {

    /**
     * Method to read a line from the console.
     *
     * @param prompt
     * @return the line that has been read
     */
    static public String readLineFromConsole(String prompt) {
        try {
            System.out.println("\n" + prompt);

            InputStreamReader converter = new InputStreamReader(System.in);
            BufferedReader in = new BufferedReader(converter);

            return in.readLine();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Method to read an integer from the console.
     *
     * @param prompt
     * @return the integer that has been read.
     */
    static public int readIntegerFromConsole(String prompt) {
        do {
            try {
                String input = readLineFromConsole(prompt);

                int value = Integer.parseInt(input);

                return value;
            } catch (NumberFormatException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while (true);
    }

    /**
     * Method to read a double from the console.
     *
     * @param prompt
     * @return the double that has been read.
     */
    static public double readDoubleFromConsole(String prompt) {
        do {
            try {
                String input = readLineFromConsole(prompt);

                double value = Double.parseDouble(input);

                return value;
            } catch (NumberFormatException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while (true);
    }

    /**
     * Method to read a date from the console.
     *
     * @param prompt
     * @return the date that has been read.
     */
    static public Date readDateFromConsole(String prompt) {
        do {
            try {
                String strDate = readLineFromConsole(prompt);

                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

                Date date = df.parse(strDate);

                return date;
            } catch (ParseException ex) {
                System.out.println("Invalid Format! Date Format (dd-MM-yyyy)");
            }
        } while (true);
    }

    /**
     * Method to confirm anything. The user must type a "y" or a "n"
     *
     * @param message
     * @return true if the letter Y has been typed, and false if the letter N has been typed
     */
    static public boolean confirm(String message) {
        String input;
        do {
            input = Utils.readLineFromConsole("\n" + message + "\n");
        } while (!input.equalsIgnoreCase("y") && !input.equalsIgnoreCase("n"));

        return input.equalsIgnoreCase("y");
    }

    /**
     * Shows and selects an object from a list
     *
     * @param list
     * @param header
     * @return the selected object
     */
    static public Object showAndSelectOne(List list, String header) {
        showList(list, header);
        return selectsObject(list);
    }

    /**
     * Shows and selects the index of an object from a list
     *
     * @param list
     * @param header
     * @return the index of the selected object
     */
    static public int showAndSelectIndex(List list, String header) {
        showList(list, header);
        return selectsIndex(list);
    }

    /**
     * Prints a list
     *
     * @param list
     * @param header
     */
    static public void showList(List list, String header) {
        System.out.println(header);

        int index = 0;
        for (Object o : list) {
            index++;

            System.out.println("\n"+index + ". " + o.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancel");
    }

    /**
     * Selects an object from a list
     *
     * @param list
     * @return the selected object
     */
    static public Object selectsObject(List list) {
        String input;
        Integer value;
        do {
            input = Utils.readLineFromConsole("Type your option: ");
            value = Integer.valueOf(input);
        } while (value < 0 || value > list.size());

        if (value == 0) {
            return null;
        } else {
            return list.get(value - 1);
        }
    }

    /**
     * Selects the index of an object from a list
     *
     * @param list
     * @return the index of the selected object
     */
    static public int selectsIndex(List list) {
        String input;
        Integer value = null;
        do {
            input = Utils.readLineFromConsole("Type your option: ");
            try {
                value = Integer.valueOf(input);
            } catch (NumberFormatException ex) {
                System.out.println("Invalid option");
            }

        } while (value == null || value < 0 || value > list.size());

        return value - 1;
    }


    /**
     * Method to validate if the phone number has a correct length and starts with a 2 or a 9
     *
     * @param phoneNumber
     */
    public static void validatePhoneNumber(String phoneNumber) {
        if (phoneNumber.length() != 9 || !phoneNumber.startsWith("9") && !phoneNumber.startsWith("2")) {
            throw new NumberFormatException("Invalid Phone Number");
        }
        try {
            Integer.parseInt(phoneNumber);
        } catch (NumberFormatException ignored) {
            throw new NumberFormatException("Invalid Phone Number");
        }
    }

    /**
     * Method that asks to be typed a phone number and validates it.
     *
     * @return a valid phone number
     */
    public static String getValidPhoneNumber() {
        boolean validPhoneNumber;
        String phoneNumber = "";
        do {
            validPhoneNumber = true;
            try {
                phoneNumber = Utils.readLineFromConsole("Phone Number:");
                validatePhoneNumber(phoneNumber);
            } catch (IllegalArgumentException ex) {
                System.out.println(ex.getMessage());
                validPhoneNumber = false;
            }
        } while (!validPhoneNumber);
        return phoneNumber;
    }

    /**
     * Method that asks to be typed an email and validates it.
     *
     * @return a valid email
     */
    public static String getValidEmail() {
        String email = "";
        boolean validEmail;
        do {
            validEmail = true;
            try {
                email = Utils.readLineFromConsole("Email:");
                new Email(email);
            } catch (IllegalArgumentException ex) {
                System.out.println(ex.getMessage());
                validEmail = false;
            }
        } while (!validEmail);
        return email;
    }

    /**
     * Method that asks to be chosen a gender or to be typed if the user wants it
     *
     * @return a valid gender
     */
    public static String getValidGender() {
        boolean validGender;
        int value = 0;
        do {
            validGender = true;
            try {
                value = readIntegerFromConsole(String.format("%s\n   %s\n   %s\n   %s\n   %s\nType your option:", "Gender", "1. Male", "2. Female", "3. Not Listed", "4. Prefer Not To Answer"));
            } catch (NumberFormatException ex) {
                System.out.println("Invalid Option");
                validGender = false;
            }
        } while (!validGender);

        switch (value) {
            case 1:
                return "Male";
            case 2:
                return "Female";
            case 3:
                return readLineFromConsole("Type your gender: ");
            default:
                return "";
        }
    }

    /**
     * Method that asks to be typed a portuguese citizen card number and validates it.
     *
     * @return a valid citizen card number
     */
    public static String getValidCitizenCardNumber() {
        String citizenCardNumber = "";
        boolean validCC;
        do {
            validCC = true;
            try {
                citizenCardNumber = Utils.readLineFromConsole("Citizen Card Number:");
                validatePortugueseCitizenCard(citizenCardNumber);
            } catch (IllegalArgumentException ex) {
                System.out.println(ex.getMessage());
                validCC = false;
            }
        } while (!validCC);
        return citizenCardNumber;
    }

    /**
     * Method that asks to be typed a sns user number and validates it.
     *
     * @return a valid sns user number
     */
    public static String getValidSNSNumber() {
        String snsNumber = "";
        boolean validSNS;
        do {
            validSNS = true;
            try {
                snsNumber = Utils.readLineFromConsole("SNS User Number: ");
                validateSNSNumber(snsNumber);
            } catch (IllegalArgumentException ex) {
                System.out.println(ex.getMessage());
                validSNS = false;
            }
        } while (!validSNS);
        return snsNumber;
    }

    /**
     * Method to validate if the citizen card number has a correct length.
     *
     * @param citizenCardNumber
     */
    public static void validatePortugueseCitizenCard(String citizenCardNumber) {
        if (citizenCardNumber.length() < 7 || citizenCardNumber.length() > 12)
            throw new IllegalArgumentException("Invalid Length!");
    }

    /**
     * Method to validate if the snsNumber has a correct length.
     *
     * @param snsNumber
     */
    public static void validateSNSNumber(String snsNumber) {
        if (snsNumber.length() != 9)
            throw new IllegalArgumentException("Invalid Length!");
    }

    /**
     * Method that received an object of the Date class and parses it to the "dd-MM-yyyy" pattern
     *
     * @param date
     * @return a String of the date with the wanted pattern
     */
    public static String convertToSimpleDataFormat(Date date) {
        if (date != null) {
            String pattern = "dd-MM-yyyy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            return simpleDateFormat.format(date);
        }
        return "";
    }

    public static String convertFromDateToHours(Date date) {
        String pattern = "HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(date);
    }

    public static String getValidHour(String prompt) {
        boolean validHour;
        String hour = "";
        Hour hours = null;
        do {
            validHour = true;
            try {
                hour = Utils.readLineFromConsole(prompt);
                hours = new Hour(hour);
            } catch (IllegalArgumentException ex) {
                System.out.println(ex.getMessage());
                validHour = false;
            }
        } while (!validHour);
        return hours.toString();
    }

    public static Date getValidScheduleDate(String prompt) {
        boolean validDate;
        Date date = null;
        do {
            validDate = true;
            date = Utils.readDateFromConsole(prompt);

            LocalDate date1 = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            LocalDate currentDate = LocalDate.now();

            if (!date1.isAfter(currentDate.minusDays(1))) {
                validDate = false;
                System.out.println("Invalid Date!");
            }
        } while (!validDate);
        return date;
    }
    public static Integer[] toConvertInteger(int[] ids) {

        Integer[] newArray = new Integer[ids.length];
        for (int i = 0; i < ids.length; i++) {
            newArray[i] = Integer.valueOf(ids[i]);
        }
        return newArray;
    }

    public static int[] toint(Integer[] WrapperArray) {

        int[] newArray = new int[WrapperArray.length];
        for (int i = 0; i < WrapperArray.length; i++) {
            newArray[i] = WrapperArray[i].intValue();
        }
        return newArray;
    }

}
