package app.ui.console.utils;

import app.domain.model.Company;

import java.io.*;

/**
 * The type Save read company.
 */
public abstract class SaveReadCompany implements Serializable {

    /**
     * Save company.
     *
     * @param company the company
     */
    public static void saveCompany(Company company){
        try {
           ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("Company.bin"));
            outputStream.writeObject(company);
            outputStream.close();
        }catch (IOException ex){
            ex.printStackTrace();
        }
    }

    /**
     * Read company company.
     *
     * @return the company
     */
    public static Company readCompany(){
        Company company = null;
        try{
            ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("Company.bin"));
            company = (Company) inputStream.readObject();
            inputStream.close();
        }catch (IOException | ClassNotFoundException ex){
            ex.printStackTrace();
        }
        return company;
    }
}
