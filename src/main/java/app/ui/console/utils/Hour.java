package app.ui.console.utils;

import java.io.Serializable;

/**
 * The type Hour.
 */
public class Hour implements Serializable {
    private int hour;
    private int minutes;

    /**
     * Instantiates a new Hour.
     *
     * @param hours the hours
     */
    public Hour(String hours){
        this.parse(hours);
    }

    /**
     * Parse.
     *
     * @param hours the hours
     */
    public void parse(String hours){
        try{
            this.hour = Integer.parseInt(hours.split(":")[0]);
            this.minutes = Integer.parseInt(hours.split(":")[1]);
            }
        catch (NumberFormatException ex){
            throw new IllegalArgumentException("Invalid Hour! Please, insert only numbers in HH:MM format");
        }

        if(!((this.hour >= 0) && (this.hour <= 23) && (this.minutes >= 0) && (this.minutes <= 59)))
            throw new IllegalArgumentException("Invalid Hour!");
    }

    /**
     * Is after boolean.
     *
     * @param hour1 the hour 1
     * @param hour2 the hour 2
     * @return the boolean
     */
    public static boolean isAfter (String hour1, String hour2){
        Hour h1 = new Hour(hour1);
        Hour h2 = new Hour(hour2);

        if(hour1 == null || hour2 == null) {
            return false;
        }

        if(h1.hour > h2.hour) {
            return false;
        }

        if (h1.hour == h2.hour && h1.minutes > h2.minutes) {
            return false;
        }

        return true;
    }

    /**
     * Advance minutes hour.
     *
     * @param minutesAdded the minutes added
     * @return the hour
     */
    public Hour advanceMinutes(int minutesAdded) {
        this.minutes = minutes + minutesAdded;
        int minutes2 = minutes % 60;
        if (minutes > minutes2) {
            hour++;
            minutes = minutes2;
        }

        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hour hour1 = (Hour) o;
        return hour == hour1.hour && minutes == hour1.minutes;
    }


    @Override
    public String toString() {
        return String.format("%02d:%02d", hour, minutes);
    }
}
