package app.ui.console;

import app.controller.App;
import app.controller.ListOfEmployeesController;
import app.controller.RegisterEmployeeController;
import app.domain.model.SNSUser;
import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.domain.model.employees.CenterCoordinator;
import app.domain.model.employees.Nurse;
import app.domain.model.employees.Receptionist;
import app.ui.console.utils.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */

/**
 * Represents the UI when an Administrator wants to register a new Employee
 */

public class AdminUI implements Runnable, Serializable {
    private RegisterEmployeeController controller;


    /**
     * Creates a new UI for the registration of a new Employee.
     */
    public AdminUI()
    {
        controller = new RegisterEmployeeController();
    }

    /**
     * Runs the UI for the registration of a new Employee.
     * Asks to be typed in the console the name, address, phone number, email and citizen card number for the creation of the new SNS User
     */

    public void run()
    {
        List<MenuItem> options = new ArrayList<MenuItem>();
        List<MenuItem> subOptions = new ArrayList<MenuItem>();
        options.add(new MenuItem("Register a new Employee", new ShowTextUI("You have chosen to register a new Employee.")));
        options.add(new MenuItem("Register a new Health Care Center", new ShowTextUI("You have chosen to register a new Health Care Center")));
        options.add(new MenuItem("Register a new Mass Vaccination Center", new ShowTextUI("You have chosen to register a new Mass Vaccination Center")));
        options.add(new MenuItem("Get a list of Employees", new ShowTextUI("You have chosen to get a list of the Employees.")));
        options.add(new MenuItem("Specify a new vaccine", new ShowTextUI("You have chosen to specify a new vaccine.")));
        //options.add(new MenuItem("Enter nurse interface", new NurseUI()));
        options.add(new MenuItem("Load a set of users from a CSV file", new LoadCSVFileUI()));

        subOptions.add(new MenuItem("Register a new Nurse", new ShowTextUI("You have chosen to register a new Nurse")));
        subOptions.add(new MenuItem("Register a new Receptionist", new ShowTextUI("You have chosen to register a new Receptionist")));
        subOptions.add(new MenuItem("Register a new Center Coordinator", new ShowTextUI("You have chosen to register a new Center Coordinator")));

        int option = 0, option2 = 0;
        Nurse n1 = null;
        Receptionist r1 = null;
        CenterCoordinator c1 = null;
        SNSUser s1 = null;


        do
        {
            option = Utils.showAndSelectIndex(options, "\n\nAdmin Menu:");
            if ( (option >= 0) && (option < options.size())) {
                options.get(option).run();

                if (option == 0) {

                    option2 = Utils.showAndSelectIndex(subOptions, "\n\nChoose an option:");
                    if ((option2 >= 0) && (option2 < subOptions.size())) {
                        subOptions.get(option2).run();
                        if (option2 == 0) {
                            try {
                                String name = Utils.readLineFromConsole("Name:");
                                String address = Utils.readLineFromConsole("Address:");
                                String phoneNumber = Utils.getValidPhoneNumber();
                                String email = Utils.getValidEmail();
                                String citizenCardNumber = Utils.getValidCitizenCardNumber();
                                n1 = new Nurse(name, address, phoneNumber, email, citizenCardNumber);

                                controller.registerEmployee(n1);
                                controller.saveEmployee(n1);


                            } catch (IllegalArgumentException ex) {
                                System.out.println(ex.getMessage());
                            }
                        } else if (option2 == 1) {
                            try {
                                String name = Utils.readLineFromConsole("Name:");
                                String address = Utils.readLineFromConsole("Address:");
                                String phoneNumber = Utils.getValidPhoneNumber();
                                String email = Utils.getValidEmail();
                                String citizenCardNumber = Utils.getValidCitizenCardNumber();

                                r1 = new Receptionist(name, address, phoneNumber, email, citizenCardNumber);
                                controller.registerEmployee(r1);
                                controller.saveEmployee(r1);

                            } catch (IllegalArgumentException ex) {
                                System.out.println(ex.getMessage());
                            }
                        } else if (option2 == 2) {
                            try {
                                String name = Utils.readLineFromConsole("Name:");
                                String address = Utils.readLineFromConsole("Address:");
                                String phoneNumber = Utils.getValidPhoneNumber();
                                String email = Utils.getValidEmail();
                                String citizenCardNumber = Utils.getValidCitizenCardNumber();
                                VaccinationCenter vaccinationCenter = (VaccinationCenter) Utils.showAndSelectOne(App.getInstance().getCompany().getVaccinationCenterStore().getVaccinationCentersList(),"");

                                c1 = new CenterCoordinator(name, address, phoneNumber, email, citizenCardNumber, vaccinationCenter);

                                controller.registerEmployee(c1);
                                controller.saveEmployee(c1);

                            } catch (IllegalArgumentException ex) {
                                System.out.println(ex.getMessage());
                            }
                        }
                    }
                }
                else if(option == 1){
                    HealthCenterUI healthCenterUI = new HealthCenterUI();
                    healthCenterUI.run();
                }
                else if(option == 2){
                    MassVaccinationCenterUI massVaccinationCenterUI = new MassVaccinationCenterUI();
                    massVaccinationCenterUI.run();
                }

                else if(option == 3){
                    ListOfEmployeesController employeeList = new ListOfEmployeesController();
                    int option3 = Utils.showAndSelectIndex(employeeList.getRolesList(), "\n\nChoose an option:");
                    switch (option3){
                        case 0:
                            Utils.showList(employeeList.getEmployeeListByRole("NURSE"),"");
                            break;
                        case 1:
                            Utils.showList(employeeList.getEmployeeListByRole("RECEPTIONIST"),"");
                            break;
                        case 2:
                            Utils.showList(employeeList.getEmployeeListByRole("CENTER_COORDINATOR"),"");
                            break;
                        default:
                            System.out.println("Invalid Option");
                    }
                }
                else if(option == 4){
                    SpecifyNewVaccineTypeUI newVaccine = new SpecifyNewVaccineTypeUI();
                    newVaccine.run();
            }
            }
            } while (option != -1 );






    }

}
