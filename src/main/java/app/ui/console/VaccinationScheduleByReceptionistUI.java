package app.ui.console;

import app.ui.console.utils.Utils;


/**
 * The type Vaccination schedule by receptionist ui.
 */
public class VaccinationScheduleByReceptionistUI extends VaccinationScheduleUI{

    public void run() {
        super.run();
        if(correctInfo) {
            if (Utils.confirm("Do you want to send a SMS with vaccination schedule information? (y/n)")) {
                vsController.setVaccinationSchedule(schedule);
                vsController.sendSMS(schedule.getScheduleDTO().getSnsUserNumber());
            }
        }
    }
}
