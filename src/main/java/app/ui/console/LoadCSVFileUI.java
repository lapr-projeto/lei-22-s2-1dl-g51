package app.ui.console;

import app.controller.VaccineAdminstrationController;
import app.domain.model.VaccineAdministration;
import app.ui.console.utils.Utils;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Load csv file ui.
 */
public class LoadCSVFileUI implements Runnable, Serializable {

    /**
     * The Va ctrl.
     */
    VaccineAdminstrationController vaCtrl = new VaccineAdminstrationController();

    @Override
    public void run() {


        File file = null;
        do {
            String filePath = Utils.readLineFromConsole("Insert the file path:");
            file = new File(filePath);
            if (!file.exists())
                System.out.println("File not founded!");
        } while (!file.exists());


        ArrayList<VaccineAdministration> vaList = vaCtrl.getVaListFromFile(file);
        vaCtrl.saveVaList(vaList);

        vaList = vaCtrl.getVaStore().getVaList();

        List<MenuItem> options = new ArrayList<MenuItem>();
        List<MenuItem> subOptions = new ArrayList<MenuItem>();
        options.add(new MenuItem("Import SNS user file.", new CreateUsersUI(file)));
        options.add(new MenuItem("Import Vaccine Administration file. ", new ShowTextUI("Adding new vaccine administrations ...")));

        subOptions.add(new MenuItem("Selection Sort by arrival time...", new SelectionSortByArrivalTimeUI(vaList)));
        subOptions.add(new MenuItem("Bubble Sort by arrival time...", new BubbleSortByArrivalTimeUI(vaList)));
        subOptions.add(new MenuItem("Selection Sort by leaving time...", new SelectionSortByLeavingTimeUI(vaList)));
        subOptions.add(new MenuItem("Bubble Sort by leaving time...", new BubbleSortByLeavingTimeUI(vaList)));

        int option = Utils.showAndSelectIndex(options, "\n\nin Menu:");
        int option2 = 0;

        if ((option >= 0) && (option < options.size()))
            options.get(option).run();

        if (option == 1)

            option2 = Utils.showAndSelectIndex(subOptions, "\n\nChoose an option:");
        if ((option2 >= 0) && (option2 < subOptions.size())) {
            subOptions.get(option2).run();


        }
    }
}
