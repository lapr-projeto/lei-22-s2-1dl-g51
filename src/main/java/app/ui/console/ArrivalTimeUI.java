package app.ui.console;

import app.controller.App;
import app.controller.ArrivalTimeController;
import app.domain.model.ArrivalTime;
import app.domain.model.Company;
import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.domain.model.VaccinationSchedule;
import app.domain.model.store.ArrivalTimeStore;
import app.ui.console.utils.Utils;

import java.io.Serializable;


/**
 * The type Arrival time ui.
 */
public class ArrivalTimeUI implements Runnable, Serializable {

    /**
     * The Vc.
     */
    VaccinationCenter vc;

    /**
     * Instantiates a new Arrival time ui.
     *
     * @param vc the vc
     */
    public ArrivalTimeUI(VaccinationCenter vc) {
        this.vc = vc;
    }

    @Override
    public void run() {
        Company company = App.getInstance().getCompany();
        ArrivalTimeController controller = new ArrivalTimeController();

        String snsNum = Utils.getValidSNSNumber();

        VaccinationSchedule vs2 = controller.verifyIfScheduleExists(snsNum);

        ArrivalTimeStore arrivalTimeStore = company.getArrivalTimeStore();
            if (vs2 != null){
            ArrivalTime at1 = new ArrivalTime(snsNum,vc,vs2.getScheduleDTO().getDate(),vs2.getScheduleDTO().getTime());
            System.out.println(at1);
            if(Utils.confirm("Confirm the data? (y/n)"))
                arrivalTimeStore.getArrivaltimes().add(at1);
            else{
                System.out.println("Arrival time not registered!");
            }
            }else{
                System.out.println("There's no vaccination schedule for this SNS User today!");
            }

}
}
