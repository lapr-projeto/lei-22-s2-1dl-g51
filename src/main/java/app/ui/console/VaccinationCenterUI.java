package app.ui.console;


import app.ui.console.utils.Hour;
import app.ui.console.utils.Utils;
import javafx.application.Application;
import javafx.stage.Stage;

import java.io.Serializable;


/**
 * The type Vaccination center ui.
 */
public class VaccinationCenterUI extends Application implements Runnable, Serializable {

    /**
     * The Name.
     */
    protected String name, /**
     * The Address.
     */
    address, /**
     * The Phone number.
     */
    phoneNumber, /**
     * The Email.
     */
    email, /**
     * The Open hours.
     */
    openHours, /**
     * The Closing hours.
     */
    closingHours, /**
     * The Vaccines slot cap.
     */
    vaccinesSlotCap, /**
     * The Slot duration.
     */
    slotDuration;
        @Override
        public void run() {
            name = Utils.readLineFromConsole("Name:");
            address = Utils.readLineFromConsole("Address:");
            phoneNumber = Utils.getValidPhoneNumber();
            email = Utils.getValidEmail();
            openHours = "";
            closingHours = "";
            boolean validTimeTable = false;
            do {
                openHours = Utils.getValidHour("Opening hours:");
                closingHours = Utils.getValidHour("Closing hours:");
                validTimeTable = Hour.isAfter(openHours, closingHours);
                if (!validTimeTable) {
                    System.out.println("Invalid Timetable!");
                }
            }while (!validTimeTable);

            vaccinesSlotCap = Utils.readLineFromConsole("Maximum number of vaccines that can be given per slot:");
            slotDuration = Utils.readLineFromConsole("Slot duration in minutes:");
        }

    @Override
    public void start(Stage stage) throws Exception {

    }
}
