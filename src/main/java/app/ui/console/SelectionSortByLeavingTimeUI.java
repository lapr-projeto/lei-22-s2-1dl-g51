package app.ui.console;

import app.controller.VaccineAdminstrationController;
import app.domain.model.VaccineAdministration;
import app.ui.console.utils.Utils;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * The type Selection sort by leaving time ui.
 */
public class SelectionSortByLeavingTimeUI implements Runnable, Serializable {

    /**
     * The Va list.
     */
    ArrayList<VaccineAdministration> vaList;
    /**
     * The Ctrl.
     */
    VaccineAdminstrationController ctrl= new VaccineAdminstrationController();

    /**
     * Instantiates a new Selection sort by leaving time ui.
     *
     * @param vaList the va list
     */
    public SelectionSortByLeavingTimeUI(ArrayList<VaccineAdministration> vaList) {
        this.vaList=vaList;
        ctrl.selectionSortByLeavingTime(vaList);
    }

    @Override
    public void run() {

        Utils.showList(vaList,"\nSelection Sort by leaving time...");
    }
}
