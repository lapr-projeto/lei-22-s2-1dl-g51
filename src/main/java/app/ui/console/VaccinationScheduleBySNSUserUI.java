package app.ui.console;

import app.ui.console.utils.Utils;

import java.io.Serializable;


/**
 * The type Vaccination schedule by sns user ui.
 */
public class VaccinationScheduleBySNSUserUI extends VaccinationScheduleUI implements Serializable {

    public void run() {
        super.run();
        if(correctInfo) {
            if (Utils.confirm("Do you want to receive a SMS with vaccination schedule information? (y/n)")) {
                vsController.setVaccinationSchedule(schedule);
                vsController.sendSMS(schedule.getScheduleDTO().getSnsUserNumber());
            }
        }
    }
}
