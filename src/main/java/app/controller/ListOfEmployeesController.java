package app.controller;

import app.domain.model.Company;
import app.domain.model.EmployeesList;
import app.domain.model.RolesList;
import app.domain.model.employees.Employee;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListOfEmployeesController implements Serializable {

    private Company company;
    public ListOfEmployeesController (){}

    /**
     * method to get a list of roles
     * @return list of roles
     */
    public List<String> getRolesList(){
        RolesList list = new RolesList();
        return list.getUserRolesList();
    }

    /**
     * method to get a list of employees
     * @param role
     * @return list of employees
     */
    public List<Employee> getEmployeeListByRole(String role){
        EmployeesList list = new EmployeesList();
        List<Employee> list2 = list.getEmployeesList();
        List<Employee> listByRole = new ArrayList<>();
        for (Employee e: list2) {
            if(e.getRoleID().equalsIgnoreCase(role)){
                listByRole.add(e);
            }
        }
        if(listByRole.isEmpty())
            System.out.printf("\nList of %s is empty!", role);
        return listByRole;
    }

}