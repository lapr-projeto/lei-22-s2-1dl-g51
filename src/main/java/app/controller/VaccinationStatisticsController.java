package app.controller;

import app.domain.model.employees.CenterCoordinator;
import app.domain.model.store.VaccinationStatisticsStore;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.time.format.DateTimeFormatter;

/**
 * The type Vaccination statistics controller.
 */
public class VaccinationStatisticsController implements Serializable {

    @javafx.fxml.FXML
    private Button buttonStatistics;

    @javafx.fxml.FXML
    private DatePicker initialDate;

    @javafx.fxml.FXML
    private Button back;

    @javafx.fxml.FXML
    private Button export;
    @FXML
    private TextArea textBox;

    private static CenterCoordinator cc;
    @FXML
    private Label lblVaccinationCenter;


    /**
     * Initialize.
     */
    public void initialize(){
        lblVaccinationCenter.setText(cc.getVaccinationCenter().getVaccinationCenterDTO().getName());
    }

    /**
     * Sets cc.
     *
     * @param cc the cc
     */
    public static void setCc(CenterCoordinator cc) {
        VaccinationStatisticsController.cc = cc;
    }

    /**
     * Export statistics.
     *
     * @param actionEvent the action event
     */
    @javafx.fxml.FXML
    public void exportStatistics(ActionEvent actionEvent){
        DirectoryChooser dc = new DirectoryChooser();
        File selectedDirectory = dc.showDialog(new Stage());
        File file = new File(selectedDirectory.getAbsoluteFile()+"/Vaccination_Statistics_"+initialDate.getValue().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"))+".csv");
        try {
            PrintWriter writer = new PrintWriter(file);
            writer.print(String.format("%s\n%s",lblVaccinationCenter.getText(),textBox.getText()));
            writer.close();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("Statistics File saved in "+ file.getAbsolutePath());
            alert.showAndWait();
        }catch (IOException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("File not found!");
            alert.showAndWait();
        }

    }

    /**
     * Show statistics.
     *
     * @param actionEvent the action event
     */
    @FXML
    public void showStatistics(ActionEvent actionEvent) {
        if(initialDate.getValue() != null) {
            String date = initialDate.getValue().format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
            int countVaccinated = VaccinationStatisticsStore.getNumberOfVaccinated(cc, date);
            StringBuilder sb = new StringBuilder();
            sb.append(date);
            sb.append(" --> ");
            sb.append(countVaccinated);
            sb.append(" user(s) fully vaccinated");
            textBox.setText(sb.toString());
        }
        else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Please, insert a date!");
            alert.showAndWait();
        }
    }


    /**
     * Back.
     *
     * @param actionEvent the action event
     */
    @FXML
    public void back(ActionEvent actionEvent) {
        Stage stage = (Stage) ((Node) (actionEvent).getSource()).getScene().getWindow();
        stage.close();
    }
}