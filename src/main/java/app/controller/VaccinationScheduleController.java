package app.controller;

import app.domain.model.Company;
import app.domain.model.SNSUser;
import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.domain.model.VaccinationSchedule;
import app.domain.model.VaccineType;
import app.domain.model.store.SNSUserStore;
import app.domain.model.store.VaccinationScheduleStore;
import app.ui.console.utils.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.Date;

/*+
Represents the controller for the vaccination schedule
 */
public class VaccinationScheduleController implements SMSNotificationSender, Serializable {

    private Company company;
    private VaccinationScheduleStore vaccinationScheduleStore;
    private SNSUserStore snsUserStore;
    private VaccinationSchedule vs;

    /**
     * Creates a new object of the VaccinationScheduleController class and assigns a vaccination Schedule store to validate and keep his data.
     */
    public VaccinationScheduleController() {
        this.company = App.getInstance().getCompany();
        vaccinationScheduleStore = company.getVaccinationScheduleStore();
    }

    /**
     * Check's if this snsUserNumber has been already registered in the system
     *
     * @param snsUserNumber snsUserNumber
     * @return true, if the user is already registered in the system, or false otherwise
     */
    public boolean checkUserRegistration(String snsUserNumber) {
        snsUserStore = company.getUserStore();
        return snsUserStore.checkUserRegistration(snsUserNumber);
    }

    /**
     * Creates a new Vaccination Schedule if the store validates his data.
     *
     * @param snsUserNumber     snsUserNumber
     * @param vaccinationCenter vaccinationCenter
     * @param date              date
     * @param time              time
     * @param vaccineType       vaccineType
     * @return the vaccination schedule if all the date is valid
     */
    public VaccinationSchedule createVaccinationSchedule(String snsUserNumber, VaccinationCenter vaccinationCenter, Date date, String time, VaccineType vaccineType) {
        VaccinationSchedule vaccinationSchedule = new VaccinationSchedule(snsUserNumber, vaccinationCenter, date, time, vaccineType);
        if (vaccinationScheduleStore.validateVaccineSchedule(vaccinationSchedule))
            return vaccinationSchedule;
        return null;
    }

    /**
     * Saves the created vaccination schedule if his data is confirmed in the UI.
     *
     * @param vs vaccination schedule
     */
    public void saveVaccinationSchedule(VaccinationSchedule vs) {
        if (vaccinationScheduleStore.validateVaccineSchedule(vs)) {
            if (vaccinationScheduleStore.addVaccinationSchedule(vs)) {
                System.out.println("Vaccine Schedule created successfully!");
            }
        }
    }

    /**
     * Simulates the sending of an SMS for the SNSUser with schedule appointment
     */
    public void setVaccinationSchedule(VaccinationSchedule vs) {
        this.vs = vs;
    }

    @Override
    public void sendSMS(String snsNumber) {
        String userNumber = vs.getScheduleDTO().getSnsUserNumber();
        SNSUser snsUser = company.getUserStore().getSNSUserWithAGivenSNSUserNumber(userNumber);
        String phoneNumber = snsUser.getSNSUserDTO().getPhoneNumber();
        PrintWriter printWriter = null;

        try {
            printWriter = new PrintWriter(new File("SMSto" + phoneNumber + ".txt"));
            printWriter.print(String.format("Vaccination Schedule\n  Name: %s\n  SNS User Number: %s\n  Vaccination Center: %s\n"
                            + "  Vaccine:%s\n  Date:%s\n  Hour:%s\nRegards,\n%s", snsUser.getSNSUserDTO().getName(), userNumber,
                    vs.getScheduleDTO().getVaccinationCenter().getVaccinationCenterDTO().getName(), vs.getScheduleDTO().getVaccineType().getVaccineTypeDTO().getType().name(), Utils.convertToSimpleDataFormat(vs.getScheduleDTO().getDate()), vs.getScheduleDTO().getTime(), company.getDesignation()));
            System.out.println("SMS has been sent successfully!");
        } catch (FileNotFoundException ex) {
            System.out.println("File Not Found");
        } finally {
            printWriter.close();
        }
    }
}

