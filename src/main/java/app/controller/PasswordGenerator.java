package app.controller;

import java.io.Serializable;
import java.security.SecureRandom;
import java.util.*;

/**
 * The type Password generator.
 */
public class PasswordGenerator implements Serializable {

    /**
     * The constant upper.
     */
    public static final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /**
     * The constant lower.
     */
    public static final String lower = upper.toLowerCase(Locale.ROOT);

    /**
     * The constant digits.
     */
    public static final String digits = "0123456789";

    /**
     * The constant alphanum.
     */
    public static final String alphanum = upper + lower + digits;

        private final Random random;

        private char[] symbols;

        private final char[] buf;

    /**
     * Instantiates a new Password generator.
     *
     * @param length  the length
     * @param random  the random
     * @param symbols the symbols
     */
    public PasswordGenerator(int length, Random random, String symbols) {
            if (length < 1) throw new IllegalArgumentException();
            if (symbols.length() < 2) throw new IllegalArgumentException();
            this.random = Objects.requireNonNull(random);
            this.symbols = symbols.toCharArray();
            this.buf = new char[length];
        }

    /**
     * Create an alphanumeric string generator.
     *
     * @param length the length
     * @param random the random
     */
    public PasswordGenerator(int length, Random random) {
            this(length, random, alphanum);
        }

    /**
     * Create an alphanumeric strings from a secure generator.
     *
     * @param length the length
     */
    public PasswordGenerator(int length) {
            this(length, new SecureRandom());
        }

    /**
     * Generate a random string.
     *
     * @return the string
     */
    public String passwordGenerator() {
        for (int idx = 0; idx < buf.length; ++idx) {
            if (idx < 3){
                buf[idx] = symbols[random.nextInt(upper.length())];
            }
            if (idx >= 3 && idx < 5) {
                symbols = digits.toCharArray();
                buf[idx] = symbols[random.nextInt(digits.length())];
            }
            else if(idx >= 5) {
                symbols = lower.toCharArray();
                buf[idx] = symbols[random.nextInt(lower.length())];
            }
        }
        return shuffle(new String(buf));
    }

    /**
     * Shuffle string.
     *
     * @param s the s
     * @return the string
     */
    public static String shuffle(String s) {
        List<String> letters = Arrays.asList(s.split(""));
        Collections.shuffle(letters);
        StringBuilder t = new StringBuilder(s.length());
        for (String k : letters) {
            t.append(k);
        }
        return t.toString();
    }
        
}