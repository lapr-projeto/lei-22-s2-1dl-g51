package app.controller;

import app.domain.model.Company;
import app.domain.model.VaccineAdministration;
import app.domain.model.store.VaccineAdministrationStore;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * The type Vaccine adminstration controller.
 */
public class VaccineAdminstrationController implements Serializable {

    private Company company;
    private VaccineAdministrationStore vaStore;


    /**
     * Instantiates a new Vaccine adminstration controller.
     */
    public VaccineAdminstrationController(){
        company=App.getInstance().getCompany();
        vaStore=company.getVaccineAdministrationStore();
    }

    /**
     * Get va list from file array list.
     *
     * @param file the file
     * @return the array list
     */
    public ArrayList<VaccineAdministration> getVaListFromFile(File file){
     return  vaStore.getVaListFromFile(file);
    }

    /**
     * Selection sort by arrival time.
     *
     * @param vaList the va list
     */
    public void selectionSortByArrivalTime(ArrayList<VaccineAdministration> vaList){
        vaStore.selectionSortByArrivalTime(vaList,true);
    }

    /**
     * Bubble sort by arrival time.
     *
     * @param vaList the va list
     */
    public void bubbleSortByArrivalTime(ArrayList<VaccineAdministration> vaList){
        vaStore.bubbleSortByArrivalTime(vaList,true);
    }

    /**
     * Selection sort by leaving time.
     *
     * @param vaList the va list
     */
    public void selectionSortByLeavingTime(ArrayList<VaccineAdministration> vaList){
        vaStore.selectionSortByLeavingTime(vaList,true);
    }

    /**
     * Bubble sort by leaving time.
     *
     * @param vaList the va list
     */
    public void bubbleSortByLeavingTime(ArrayList<VaccineAdministration> vaList){
        vaStore.bubbleSortByLeavingTime(vaList,true);
    }

    /**
     * Gets va store.
     *
     * @return the va store
     */
    public VaccineAdministrationStore getVaStore() {
        return vaStore;
    }

    /**
     * Save va list.
     *
     * @param vaList the va list
     */
    public void saveVaList(ArrayList<VaccineAdministration> vaList) {
        vaStore.getVaList().addAll(vaList);
    }
}
