package app.controller.VaccinationCenterController;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.VaccinationCenter.HealthCenter;
import app.domain.model.VaccineType;
import app.domain.model.store.HealthCenterStore;

import java.io.Serializable;
import java.util.ArrayList;

public class HealthCenterController implements Serializable {
    private Company company;
    private HealthCenter hc;
    private HealthCenterStore store;

    public HealthCenterController (){
        this.company= App.getInstance().getCompany();
        this.store=company.getHcStore();
    }

    /**
     * Creates a Health Center
     * @return New Health Center
     */
    public boolean createHC(String name, String address, String phoneNumber, String email, String openHours, String closingHours, String vacinesSlotCap, String slotDuration, ArrayList<VaccineType> vaccineTypes){
        hc = new HealthCenter(name,address,phoneNumber,email,openHours,closingHours,vacinesSlotCap,slotDuration,vaccineTypes);
        System.out.println(hc);
        return store.validateHC(hc);
    }

    /**
     * Saves previous health center on the HC store
     * @return
     */
    public boolean saveHC(){return store.saveHC(hc);}

}
