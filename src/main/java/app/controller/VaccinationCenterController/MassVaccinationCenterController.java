package app.controller.VaccinationCenterController;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.VaccinationCenter.MassVaccinationCenter;
import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.domain.model.VaccineType;
import app.domain.model.store.MassVacinationCenterStore;

import java.io.Serializable;

/**
 * The type Mass vaccination center controller.
 */
public class MassVaccinationCenterController implements Serializable {
    /**
     * The Company.
     */
    public Company company;
    /**
     * The Mvc.
     */
    public MassVaccinationCenter mvc;
    /**
     * The Vc.
     */
    public VaccinationCenter vc;
    /**
     * The Ctrl.
     */
    public VaccinationCenterController ctrl;

    private MassVacinationCenterStore mvcStore;

    /**
     * Instantiates a new Mass vaccination center controller.
     */
    public MassVaccinationCenterController(){
        company=App.getInstance().getCompany();
        mvcStore = company.getMvcStore();

    }

    /**
     * Instantiates a new Mass vaccination center controller.
     *
     * @param company the company
     */
    public MassVaccinationCenterController(Company company){
        this.company=company;
        this.mvc=null;
        this.vc= ctrl.getVc();
    }

    /**
     * Create mvc boolean.
     *
     * @param name           the name
     * @param address        the address
     * @param phoneNumber    the phone number
     * @param email          the email
     * @param openHours      the open hours
     * @param closingHours   the closing hours
     * @param vacinesSlotCap the vacines slot cap
     * @param slotDuration   the slot duration
     * @param vaccineType    the vaccine type
     * @return the boolean
     */
    public boolean createMVC(String name, String address, String phoneNumber, String email, String openHours,
                             String closingHours, String vacinesSlotCap, String slotDuration, VaccineType vaccineType){
        this.mvc = mvcStore.createMVC(name, address, phoneNumber, email, openHours, closingHours, vacinesSlotCap, slotDuration,vaccineType);
        System.out.println(mvc);
        return mvcStore.validateMVC(mvc);
    }

    /**
     * Save mvc boolean.
     *
     * @return the boolean
     */
    public boolean saveMVC(){return this.mvcStore.saveMVC(this.mvc);}

    /**
     * Gets vc.
     *
     * @return the vc
     */
    public VaccinationCenter getVc() {
        return this.vc;
    }
}

