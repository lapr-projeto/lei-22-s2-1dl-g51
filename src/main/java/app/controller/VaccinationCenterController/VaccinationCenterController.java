package app.controller.VaccinationCenterController;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.domain.model.store.VaccinationCenterStore;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Represents the controller for the registration of new vaccination center
 */
public class VaccinationCenterController implements Serializable {

        private Company company;
        private VaccinationCenter vc;
        private VaccinationCenterStore vcStore;

    /**
     * Creates a new object of the VaccinationCenterController class and assigns a store to validate and keep his data.
     */
    public VaccinationCenterController() {
            this.company=App.getInstance().getCompany();
            vcStore = company.getVaccinationCenterStore();
        }


    /**
     * Instantiates a new Vaccination center controller.
     *
     * @param company the company
     */
    public VaccinationCenterController(Company company) {
            this.company = company;
            this.vc = null;
        }

    /**
     * Create vaccination center boolean.
     *
     * @param name           the name
     * @param address        the address
     * @param phoneNumber    the phone number
     * @param email          the email
     * @param openHours      the open hours
     * @param closingHours   the closing hours
     * @param vacinesSlotCap the vacines slot cap
     * @param slotDuration   the slot duration
     * @return the boolean
     */
    public boolean createVaccinationCenter(String name, String address, String phoneNumber, String email, String openHours, String closingHours, String vacinesSlotCap, String slotDuration) {
            this.vc = vcStore.createVaccinationCenter(name, address, phoneNumber, email, openHours, closingHours, vacinesSlotCap, slotDuration);
            System.out.println(vc);
            return vcStore.validateVaccinationCenter(vc);
        }

    /**
     * Save vaccination center boolean.
     *
     * @return the boolean
     */
    public boolean saveVaccinationCenter() {
            return vcStore.saveVaccinationCenter(vc);
        }


    /**
     * Gets vc.
     *
     * @return the vc
     */
    public VaccinationCenter getVc() {
        return vc;
    }

    /**
     * Get v clist array list.
     *
     * @return the array list
     */
    public ArrayList<VaccinationCenter> getVClist(){
            return (ArrayList<VaccinationCenter>) vcStore.getVaccinationCentersList();
    }


}
