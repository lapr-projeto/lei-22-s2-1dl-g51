package app.controller.VaccinationCenterController;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.store.HealthCenterStore;
import app.domain.model.store.MassVacinationCenterStore;
import app.domain.model.store.VaccinationCenterStore;

import java.io.Serializable;

/**
 * The type Nurse controller.
 */
public class NurseController implements Serializable {
    private VaccinationCenterStore vcStore;
    private MassVacinationCenterStore mvcStore;
    private HealthCenterStore hcStore;


    private Company company;
    private NurseController ctrl;

    /**
     * Instantiates a new Nurse controller.
     */
    public NurseController(){
        this.company= App.getInstance().getCompany();
        this.vcStore=company.getVcStore();
        this.mvcStore=company.getMvcStore();
        this.hcStore=company.getHcStore();
    }

    /**
     * Gets vc store.
     *
     * @return the vc store
     */
    public VaccinationCenterStore getVcStore() {
        return vcStore;
    }

    /**
     * Gets mvc store.
     *
     * @return the mvc store
     */
    public MassVacinationCenterStore getMvcStore() {
        return mvcStore;
    }

    /**
     * Gets hc store.
     *
     * @return the hc store
     */
    public HealthCenterStore getHcStore() {
        return hcStore;
    }
}
