package app.controller;

import app.domain.model.Company;
import app.domain.model.VaccineType;
import app.domain.model.VaccineTypes;
import app.domain.model.store.VaccineTypeStore;

import java.io.Serializable;

/**
 * The type Specify new vaccine type controller.
 */
public class SpecifyNewVaccineTypeController implements Serializable {
    private Company company;
    private VaccineType vt;
    private VaccineTypeStore vaccineTypeStore;

    /**
     * Instantiates a new Specify new vaccine type controller.
     */
    public SpecifyNewVaccineTypeController () {
        this(App.getInstance().getCompany());
        vaccineTypeStore = this.company.getVaccineTypeStore();
    }

    /**
     * Instantiates a new Specify new vaccine type controller.
     *
     * @param company the company
     */
    public SpecifyNewVaccineTypeController(Company company) {
        this.company = company;
        this.vt = null;
    }

    /**
     * Create vaccine type boolean.
     *
     * @param vaccineTypes the vaccine types
     * @param code         the code
     * @param designation  the designation
     * @param whoId        the who id
     * @return the boolean
     */
    public boolean createVaccineType(VaccineTypes vaccineTypes, String code, String designation, String whoId) {
        this.vt = company.getVaccineTypeStore().createVaccineType(vaccineTypes, code, designation, whoId);
        System.out.println(vt);
        return company.getVaccineTypeStore().validateVaccineType(vt);
    }

    /**
     * Save vaccine type.
     */
//método para guardar o tipo de vacina criado
    public void saveVaccineType() {
        vaccineTypeStore.saveVaccineType(vt);
    }
}

