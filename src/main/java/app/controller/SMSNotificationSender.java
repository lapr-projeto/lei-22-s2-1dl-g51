package app.controller;

/**
 * The interface Sms notification sender.
 */
public interface SMSNotificationSender {
    /**
     * Send sms.
     *
     * @param snsNumber the sns number
     */
    void sendSMS(String snsNumber);
}
