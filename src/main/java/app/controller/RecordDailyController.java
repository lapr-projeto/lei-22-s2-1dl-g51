package app.controller;

import app.domain.model.RecordDaily;
import app.domain.model.VaccineAdministration;

import java.io.Serializable;

/**
 * The type Main.
 */
public class RecordDailyController implements Serializable {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        RecordDaily recordDaily = new RecordDaily();
        recordDaily.writeAdministrationCountCSV();
    }

    /**
     * Gets number of administrations.
     *
     * @param data              the data
     * @param vaccinationCenter the vaccination center
     * @return the number of administrations
     */
    public static int getNumberOfAdministrations(String data, String vaccinationCenter) {
        int count = 0;
        for (VaccineAdministration va : App.getInstance().getCompany().getVaccineAdministrationStore().getVaccineAdministrationsList()
        ) {
            String data2 = va.getVaccineAdministrationDTO().getNurseAdministrationDateTime().substring(0,10);
            if (data.equalsIgnoreCase(data2) && vaccinationCenter.equalsIgnoreCase(va.getVaccineAdministrationDTO().getVaccinationCenter())) {
                count++;

            }
        }
        return count;

    }
}