package app.controller;

import app.domain.model.ArrivalTime;
import app.domain.model.Company;
import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.domain.model.VaccinationSchedule;
import app.domain.model.store.ArrivalTimeStore;
import app.domain.model.store.VaccinationScheduleStore;
import app.ui.console.utils.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Represents the controller for the Arrival Time of the SNS User
 */

    public class ArrivalTimeController implements Serializable {

    private Company company;

    private VaccinationScheduleStore vaccinationScheduleStore;

    private ArrivalTimeStore arrivalTimeStore;

    /**
     * Request the Arrival Time of an SNS user class assigns a store to validate and keep his data.
     */

    public ArrivalTimeController() {
        company = App.getInstance().getCompany();
        vaccinationScheduleStore = company.getVaccinationScheduleStore();
        arrivalTimeStore= company.getArrivalTimeStore();
    }

    /**
     * This method checks if the SNS Number that was entered matches any appointment scheduled.
     * Also change the date format and return only the day.
     * @param snsNum
     * @return If the number entered matches the number of an appointment booked that day then it returns the schedule information otherwise it returns null.
     */


    public VaccinationSchedule verifyIfScheduleExists(String snsNum){

        List<VaccinationSchedule> vs = vaccinationScheduleStore.getVaccinationScheduleList();
        Date data = new Date();
        String data1 = Utils.convertToSimpleDataFormat(data);


    for (VaccinationSchedule vs1:vs)
    {
        String scheduleday = Utils.convertToSimpleDataFormat(vs1.getScheduleDTO().getDate());
        if (snsNum.equalsIgnoreCase(vs1.getScheduleDTO().getSnsUserNumber()) && scheduleday.equalsIgnoreCase(data1))
                return vs1;


    } return null;
}

    public ArrayList<ArrivalTime> arrivalTimeByVC(VaccinationCenter vc){
        return arrivalTimeStore.getArrivalTimeByVC(vc);
    }

}
