package app.controller;

import app.domain.model.Company;
import app.domain.shared.Constants;
import app.ui.console.utils.SaveReadCompany;
import pt.isep.lei.esoft.auth.AuthFacade;
import pt.isep.lei.esoft.auth.UserSession;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;

/**
 * The type App.
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class App implements Serializable {

    private Company company;
    private AuthFacade authFacade;

    private App(){

        Properties props = getProperties();
//        this.company = new Company(props.getProperty(Constants.PARAMS_COMPANY_DESIGNATION));
//        this.authFacade = company.getAuthFacade();

        //With Data Persistence
        this.company = SaveReadCompany.readCompany();
        this.authFacade = new AuthFacade();
        company.setAuthFacade(authFacade);

        bootstrap();
    }

    /**
     * Gets company.
     *
     * @return the company
     */
    public Company getCompany()
    {
        return this.company;
    }

    /**
     * Gets current user session.
     *
     * @return the current user session
     */
    public UserSession getCurrentUserSession()
    {
        return this.authFacade.getCurrentUserSession();
    }

    /**
     * Do login boolean.
     *
     * @param email the email
     * @param pwd   the pwd
     * @return the boolean
     */
    public boolean doLogin(String email, String pwd)
    {
        return this.authFacade.doLogin(email,pwd).isLoggedIn();
    }

    /**
     * Do logout.
     */
    public void doLogout()
    {
        this.authFacade.doLogout();
    }

    /**
     * Gets properties.
     *
     * @return the properties
     */
    public Properties getProperties()
    {
        Properties props = new Properties();

        // Add default properties and values
        props.setProperty(Constants.PARAMS_COMPANY_DESIGNATION, "DGS/SNS");


        // Read configured values
        try
        {
            InputStream in = new FileInputStream(Constants.PARAMS_FILENAME);
            props.load(in);
            in.close();
        }
        catch(IOException ex)
        {

        }
        return props;
    }


    private void bootstrap()
    {
        this.authFacade.addUserRole(Constants.ROLE_ADMIN,Constants.ROLE_ADMIN);
        this.authFacade.addUserRole(Constants.ROLE_NURSE,Constants.ROLE_NURSE);
        this.authFacade.addUserRole(Constants.ROLE_RECEPTIONIST,Constants.ROLE_RECEPTIONIST);
        this.authFacade.addUserRole(Constants.ROLE_CENTER_COORDINATOR,Constants.ROLE_CENTER_COORDINATOR);
        this.authFacade.addUserRole(Constants.ROLE_SNS_USER,Constants.ROLE_SNS_USER);

        this.authFacade.addUserWithRole("Main Administrator", "admin@lei.sem2.pt", "123456",Constants.ROLE_ADMIN);
        this.authFacade.addUserWithRole("test", "test@arroz.pt", "arroz",Constants.ROLE_RECEPTIONIST);
        this.authFacade.addUserWithRole("User","sigala@arroz.pt","arroz",Constants.ROLE_SNS_USER);
        this.authFacade.addUserWithRole("Berta","nurse@arroz.pt","arroz",Constants.ROLE_NURSE);
        this.authFacade.addUserWithRole("José","cc@arroz.pt","arroz",Constants.ROLE_CENTER_COORDINATOR);

    }

    // Extracted from https://www.javaworld.com/article/2073352/core-java/core-java-simply-singleton.html?page=2
    private static App singleton = null;

    /**
     * Get instance app.
     *
     * @return the app
     */
    public static App getInstance(){
        if(singleton == null)
        {
            synchronized(App.class)
            {
                singleton = new App();
            }
        }
        return singleton;
    }

}
