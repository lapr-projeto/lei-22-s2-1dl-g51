package app.controller.gui;

import app.controller.AuthController;
import app.domain.shared.Constants;
import app.ui.console.MenuItem;
import app.ui.gui.CenterCoordinatorGUI;
import app.ui.gui.NurseGUI;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import pt.isep.lei.esoft.auth.mappers.dto.UserRoleDTO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * The type Login controller.
 */
public class LoginController implements Serializable {
    private AuthController ctrl;

    @javafx.fxml.FXML
    private Button btnLogin;
    @javafx.fxml.FXML
    private TextField txtEmail;
    @javafx.fxml.FXML
    private PasswordField passField;
    @javafx.fxml.FXML
    private Button btnQuit;
    private static int maxAttempts = 3;

    /**
     * Instantiates a new Login controller.
     */
    public LoginController() {
        ctrl = new AuthController();
    }

    /**
     * Initialize.
     */
    public void initialize() {
        txtEmail.setFocusTraversable(true);
        passField.setFocusTraversable(true);
        btnLogin.setFocusTraversable(true);
    }

    /**
     * Do login.
     *
     * @param actionEvent the action event
     */
    @javafx.fxml.FXML
    public void doLogin(ActionEvent actionEvent) {
        if (tryLogin(txtEmail.getText(), passField.getText())) {
            Stage stage = (Stage) ((Node) (actionEvent).getSource()).getScene().getWindow();
            stage.close();
        }
        else{
            txtEmail.clear();
            passField.clear();
        }
    }

    private void logout() {
        Platform.exit();
    }

    /**
     * Do login boolean.
     *
     * @param id  the id
     * @param pwd the pwd
     * @return the boolean
     */
    public boolean doLogin(String id, String pwd) {
        maxAttempts--;
        boolean success = ctrl.doLogin(id, pwd);
        if (!success && maxAttempts >= 0) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText("Invalid UserId and/or Password. \n You have " + maxAttempts + " more attempt(s).");
            alert.showAndWait();
        }
        return success;
    }

    private void redirectToRoleUI(List<MenuItem> rolesUI, UserRoleDTO role) {
        boolean found = false;
        Iterator<MenuItem> it = rolesUI.iterator();
        while (it.hasNext() && !found) {
            MenuItem item = it.next();
            found = item.hasDescription(role.getDescription());
            if (found)
                item.run();
        }
        if (!found) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("There is no UI for users with role '" + role.getDescription() + "'");
            alert.showAndWait();
        }
    }

    /**
     * Try login boolean.
     *
     * @param email the email
     * @param pwd   the pwd
     * @return the boolean
     */
    public boolean tryLogin(String email, String pwd) {
        if (doLogin(email, pwd)) {
            List<UserRoleDTO> roles = this.ctrl.getUserRoles();
            UserRoleDTO role = roles.get(0);
            List<MenuItem> rolesUI = getMenuItemForRoles();
            this.redirectToRoleUI(rolesUI, role);
            return true;
        }
        if (maxAttempts == 0) {
            this.logout();
        }
        return false;
    }

    private List<MenuItem> getMenuItemForRoles() {
        List<MenuItem> rolesUI = new ArrayList<>();
        rolesUI.add(new MenuItem(Constants.ROLE_NURSE, new NurseGUI()));
        rolesUI.add(new MenuItem(Constants.ROLE_CENTER_COORDINATOR, new CenterCoordinatorGUI()));
        // To complete with other user roles and related RoleUI

        //
        return rolesUI;
    }


    @javafx.fxml.FXML
    public void btnQuitAction(ActionEvent actionEvent) {
        Platform.exit();
    }

}