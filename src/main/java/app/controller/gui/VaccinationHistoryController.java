package app.controller.gui;

import app.controller.App;
import app.domain.model.VaccineAdministration;
import app.domain.model.dto.VaccineAdministrationDTO;
import javafx.collections.FXCollections;
import javafx.scene.control.ListView;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * The type Vaccination history controller.
 */
public class VaccinationHistoryController implements Serializable {
    @javafx.fxml.FXML
    private ListView listHistory;

    private static String snsUserNumber;

    /**
     * Initialize.
     */
    public void initialize(){
        listHistory.setItems(FXCollections.observableArrayList(getVaccineHistory(snsUserNumber)));
    }

    /**
     * Get vaccine history array list.
     *
     * @param snsUserNumber the sns user number
     * @return the array list
     */
    public ArrayList<String> getVaccineHistory(String snsUserNumber){
        ArrayList<String> historyList = new ArrayList<>();
        for (VaccineAdministration va : App.getInstance().getCompany().getVaccineAdministrationStore().getVaccineAdministrationsList()) {
            VaccineAdministrationDTO vaDto = va.getVaccineAdministrationDTO();
            if(vaDto.getSnsNumber().equalsIgnoreCase(snsUserNumber)){
                historyList.add(String.format("Vaccine Name: %s\nDose: %s\nLot Number: %s\nAdministration Date Time: %s\n",
                        vaDto.getVaccineName(),vaDto.getDose(),vaDto.getLotNumber(),vaDto.getNurseAdministrationDateTime()));
            }
        }
        if (historyList.size() == 0) {
            //throw new IllegalArgumentException("No Vaccination History!");
            historyList.add("No Vaccination History!");
        }
        return historyList;
    }

    /**
     * Sets sns user number.
     *
     * @param snsUserNumber the sns user number
     */
    public static void setSnsUserNumber(String snsUserNumber) {
        VaccinationHistoryController.snsUserNumber = snsUserNumber;
    }
}
