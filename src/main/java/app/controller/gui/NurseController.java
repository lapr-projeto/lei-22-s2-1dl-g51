package app.controller.gui;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.domain.model.store.VaccineAdministrationStore;
import app.ui.gui.LoginGUI;
import app.ui.gui.RecordAdverseReactionsGUI;
import app.ui.gui.RegisterVaccineAdministrationGUI;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import pt.isep.lei.esoft.auth.AuthFacade;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Nurse controller.
 */
public class NurseController implements Serializable {

    @javafx.fxml.FXML
    private ComboBox cmbFunction;
    @javafx.fxml.FXML
    private Button btnLogout;
    @javafx.fxml.FXML
    private ComboBox cmbVaccinCenter;
    private static String vaccCenterName;
    @javafx.fxml.FXML
    private Button btnGo;
    @javafx.fxml.FXML
    private Label lblHello;


    /**
     * Initialize.
     */
    public void initialize(){
        AuthFacade authFacade = App.getInstance().getCompany().getAuthFacade();
        Email email = authFacade.getCurrentUserSession().getUserId();
        String name = authFacade.getUser(email.toString()).get().getName();
        lblHello.setText("Hello, "+ name);
        List<String> functions = new ArrayList<>();
        functions.add("Record Vaccine Administration");
        functions.add("Record Adverse Reactions of a SNS User");
        ObservableList<String> list = FXCollections.observableList(functions);
        cmbFunction.setItems(list);
        Company company = App.getInstance().getCompany();
        ObservableList<String> vaccCenterlist = FXCollections.observableArrayList(getVaccinationCenterName(company.getVaccinationCenterStore().getVaccinationCentersList()));
        cmbVaccinCenter.setItems(vaccCenterlist);
    }

    /**
     * Go function.
     *
     * @param actionEvent the action event
     */
    @javafx.fxml.FXML
    public void goFunction(ActionEvent actionEvent) {
        Stage stage = (Stage)((Node)(actionEvent).getSource()).getScene().getWindow();
        stage.setAlwaysOnTop(false);
        if(cmbFunction.getValue() == null || cmbVaccinCenter.getValue() == null){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Please, choose a Vaccination Center and a task!");
            alert.showAndWait();
        }
        else {
            vaccCenterName = cmbVaccinCenter.getValue().toString();
            if (cmbFunction.getValue().toString().equalsIgnoreCase("Record Vaccine Administration")) {
                VaccineAdministrationStore vaccineAdministrationStore = App.getInstance().getCompany().getVaccineAdministrationStore();
                VaccinationCenter vaccinationCenter = vaccineAdministrationStore.getVaccinationCenterByName(NurseController.getVaccinationCenterName());
                ObservableList<String> list = FXCollections.observableList(vaccineAdministrationStore.addSNSUserToChoiceBox(vaccinationCenter));
                if (list.isEmpty()) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setContentText("There's no users waiting to take a vaccine!");
                    alert.showAndWait();
                } else {
                    RegisterVaccineAdministrationGUI re = new RegisterVaccineAdministrationGUI();
                    re.run();
                }
            }
            if (cmbFunction.getValue().toString().equalsIgnoreCase("Record Adverse Reactions of a SNS User")) {
                new RecordAdverseReactionsGUI().run();
            }
        }
    }

    /**
     * Do logout.
     *
     * @param actionEvent the action event
     * @throws Exception the exception
     */
    @javafx.fxml.FXML
    public void doLogout(ActionEvent actionEvent) throws Exception {
        Stage stage = (Stage)((Node)(actionEvent).getSource()).getScene().getWindow();
        stage.close();
        LoginGUI loginGUI = new LoginGUI();
        loginGUI.start(new Stage());
    }

    /**
     * Get vaccination center name array list.
     *
     * @param vc the vc
     * @return the array list
     */
    public ArrayList<String> getVaccinationCenterName(List<VaccinationCenter> vc){
        ArrayList<String> vaccNameList = new ArrayList<>();
        for (VaccinationCenter vacc:vc) {
            vaccNameList.add(vacc.getVaccinationCenterDTO().getName());
        }
        return vaccNameList;
    }

    /**
     * Get vaccination center name string.
     *
     * @return the string
     */
    public static String getVaccinationCenterName(){
       return vaccCenterName;
    }
}
