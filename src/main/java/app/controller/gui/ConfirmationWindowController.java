package app.controller.gui;

import app.controller.App;
import app.domain.model.VaccineAdministration;
import app.domain.model.store.ArrivalTimeStore;
import app.domain.model.store.ScheduleAndAdminstrationStore;
import app.domain.model.store.VaccinationScheduleStore;
import app.domain.model.store.VaccineAdministrationStore;
import app.ui.console.utils.SaveReadCompany;
import app.ui.gui.RegisterVaccineAdministrationGUI;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * The type Confirmation window controller.
 */
public class ConfirmationWindowController implements Serializable {
    @FXML
    private TextArea txtData;
    @FXML
    private Button btnConfirm;
    @FXML
    private Button btnExit;
    private static VaccineAdministration va;
    private static Stage previousStage;


    /**
     * Initialize.
     */
    public void initialize(){
        txtData.setText(va.toString());
    }

    /**
     * Sets va.
     *
     * @param va the va
     */
    public static void setVa(VaccineAdministration va) {
        ConfirmationWindowController.va = va;
    }

    /**
     * Sets previous stage.
     *
     * @param previousStage the previous stage
     */
    public static void setPreviousStage(Stage previousStage) {
        ConfirmationWindowController.previousStage = previousStage;
    }

    /**
     * Back.
     *
     * @param actionEvent the action event
     */
    @FXML
    public void back(ActionEvent actionEvent) {
        Stage stage = (Stage) btnExit.getScene().getWindow();
        stage.close();
    }

    /**
     * Save vaccination administration.
     *
     * @param actionEvent the action event
     * @throws ParseException the parse exception
     */
    @FXML
    public void saveVaccinationAdministration(ActionEvent actionEvent) throws ParseException {
        VaccineAdministrationStore store = App.getInstance().getCompany().getVaccineAdministrationStore();
        VaccinationScheduleStore vsStore = App.getInstance().getCompany().getVaccinationScheduleStore();
        ArrivalTimeStore arrivalTimeStore = App.getInstance().getCompany().getArrivalTimeStore();
        if(store.addVaccinationAdministration(va)) {
            arrivalTimeStore.removeFromWaitingListWhenVaccineAdministrated(va.getVaccineAdministrationDTO());
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setContentText("Vaccine Administration saved successfully!");
            alert.showAndWait();

            List<ScheduleAndAdminstrationStore> listStore = new ArrayList<>();
            listStore.add(vsStore);
            listStore.add(store);
            Timer timer = new Timer();

            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    for (ScheduleAndAdminstrationStore obj: listStore) {
                        obj.sendSMS(va.getVaccineAdministrationDTO().getSnsNumber());
                    }
//                    vsStore.sendSMS(va.getVaccineAdministrationDTO().getSnsNumber());
//                    store.sendSMS(va.getVaccineAdministrationDTO().getSnsNumber());
                }
            };

                Date leavingDate = new SimpleDateFormat("MM/dd/yyyy HH:mm").parse(va.getVaccineAdministrationDTO().getLeavingTime());

                timer.schedule(timerTask,leavingDate);

        }
        Stage stage = (Stage) btnConfirm.getScene().getWindow();
        stage.close();
        SaveReadCompany.saveCompany(App.getInstance().getCompany());
        previousStage.close();
        new RegisterVaccineAdministrationGUI().run();
    }




    }

