package app.controller.gui;

import app.controller.App;
import app.domain.model.*;
import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.domain.model.dto.VaccinationScheduleDTO;
import app.domain.model.store.VaccineAdministrationStore;
import app.ui.console.utils.Utils;
import app.ui.gui.ShowUserDataGUI;
import app.ui.gui.VaccinationConfirmationWindowGUI;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * The type Register vaccine administration controller.
 */
public class RegisterVaccineAdministrationController implements Serializable {
    @FXML
    private TextField txtLot;
    @FXML
    private Label lblVaccine;
    @FXML
    private Label lblArrivalTime;
    @FXML
    private Label lblScheduleTime;
    @FXML
    private Label lblSnsUser;
    @FXML
    private Label lblLot;
    @FXML
    private Label lblDose;
    @FXML
    private Button btnConfirm;
    @FXML
    private Button btnExit;
    @FXML
    private ComboBox<String> cmbSNSUser;
    @FXML
    private ComboBox<String> cmbVaccine;
    @FXML
    private ComboBox<String> cmbDose;
    @FXML
    private Button btnAdverseReactions;

    private Company company;
    private VaccinationCenter vaccinationCenter;
    private ArrivalTime arrivalTime;
    private VaccinationSchedule vs;
    private VaccineAdministrationStore vaccineAdministrationStore;
    private static VaccineAdministration va;
    private String vaccinationCenterName = NurseController.getVaccinationCenterName();


    /**
     * Initialize.
     */
    public void initialize() {
        this.company = App.getInstance().getCompany();
        this.vaccineAdministrationStore = company.getVaccineAdministrationStore();
        btnAdverseReactions.setVisible(false);
        lblScheduleTime.setVisible(false);
        lblArrivalTime.setVisible(false);

        vaccinationCenter = vaccineAdministrationStore.getVaccinationCenterByName(vaccinationCenterName);
        ObservableList<String> list = FXCollections.observableList(vaccineAdministrationStore.addSNSUserToChoiceBox(vaccinationCenter));
        cmbSNSUser.setItems(list);
        txtLot.setFocusTraversable(false);
    }

    /**
     * Fill vaccination options.
     *
     * @param actionEvent the action event
     */
    @FXML
    public void fillVaccinationOptions(ActionEvent actionEvent) {
        btnAdverseReactions.setVisible(true);
        lblScheduleTime.setVisible(true);
        lblArrivalTime.setVisible(true);
        cmbSNSUser.setStyle("-fx-border-color: #029d02");
        if(cmbSNSUser.getValue() != null) {
            String snsUserNumber = cmbSNSUser.getValue().substring(cmbSNSUser.getValue().length() - 9);
            ShowUserDataController.setSnsUserNumber(snsUserNumber);
            arrivalTime = vaccineAdministrationStore.getArrivalTimeBySNSNumber(snsUserNumber, vaccinationCenter);
            String arrivedTime = String.format("User has arrived at %s, %s", Utils.convertFromDateToHours(arrivalTime.getArrivalTimeDTO().getArrivalTime()), Utils.convertToSimpleDataFormat(arrivalTime.getArrivalTimeDTO().getArrivalTime()));
            String scheduledTime = String.format("Vaccination Scheduled for %s, %s", arrivalTime.getArrivalTimeDTO().getScheduleHour(), Utils.convertToSimpleDataFormat(arrivalTime.getArrivalTimeDTO().getScheduleDate()));
            lblArrivalTime.setText(arrivedTime);
            lblScheduleTime.setText(scheduledTime);
            ObservableList<String> list = FXCollections.observableArrayList("First", "Second", "Third", "Fourth");
            cmbDose.setItems(list);
            vs = vaccineAdministrationStore.getVaccinationScheduleBySNSNumber(snsUserNumber, Utils.convertToSimpleDataFormat(arrivalTime.getArrivalTimeDTO().getScheduleDate()));
            VaccineType vt = vs.getScheduleDTO().getVaccineType();
            cmbVaccine.setItems(FXCollections.observableArrayList(vaccineAdministrationStore.fillVaccineBox(vt)));
        }

    }

    /**
     * Exit.
     *
     * @param actionEvent the action event
     */
    @FXML
    public void exit(ActionEvent actionEvent) {
        Stage stage = (Stage) btnExit.getScene().getWindow();
        stage.close();
    }

    /**
     * Create record vaccine administration.
     *
     * @param actionEvent the action event
     */
    @FXML
    public void createRecordVaccineAdministration(ActionEvent actionEvent){
        Stage stage = (Stage) ((Node) (actionEvent).getSource()).getScene().getWindow();
        stage.setAlwaysOnTop(false);
        if(vs != null) {
            VaccinationScheduleDTO vsDTO = vs.getScheduleDTO();
            String snsUserNumber = vsDTO.getSnsUserNumber();
            String vaccineName = vsDTO.getVaccineType().getVaccineTypeDTO().getDesignation();

            SimpleDateFormat formatOnlyDate = new SimpleDateFormat("MM/dd/yyyy");
            String scheduledTime = String.format("%s %s", formatOnlyDate.format(vsDTO.getDate()), vsDTO.getTime());
            SimpleDateFormat formatDateHours = new SimpleDateFormat("MM/dd/yyyy HH:mm");
            String arrivedTime = formatDateHours.format(arrivalTime.getArrivalTimeDTO().getArrivalTime());
            Calendar date = Calendar.getInstance();

            int recoveryPeriod = Integer.parseInt(App.getInstance().getProperties().getProperty("RECOVERY_PERIOD"));
            va = vaccineAdministrationStore.createVaccineAdministration(snsUserNumber, vaccineName, cmbDose.getValue(), txtLot.getText(), scheduledTime, arrivedTime, formatDateHours.format(new Date()), formatDateHours.format(new Date(date.getTimeInMillis() + ((long) recoveryPeriod * 60 * 1000))));

            ConfirmationWindowController.setVa(va);
        }

        boolean confirm = vaccineAdministrationStore.validateVaccineAdministration(va) && checkValidLotNumber(txtLot.getText());
        if (confirm) {
            ConfirmationWindowController.setPreviousStage(stage);
            new VaccinationConfirmationWindowGUI().run();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("All the requirements should be fulfilled");
            alert.showAndWait();
        }
    }

    private void validateLotNumber(String lotNumber) {
        if (!lotNumber.matches("(([A-Z]|\\d){5}-\\d{2})")) {
            throw new IllegalArgumentException("Incorrect Lot Number Format!");
        }
    }

    private boolean checkValidLotNumber(String lotNumber){
        try {
            validateLotNumber(lotNumber);
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;
    }

    /**
     * Check validated lot.
     *
     * @param event the event
     */
    @FXML
    public void checkValidatedLot(Event event) {
        try {
            validateLotNumber(txtLot.getText());
            txtLot.setStyle("-fx-border-color: #029d02");
        } catch (IllegalArgumentException e) {
            txtLot.setStyle("-fx-border-color: #c42525");
        }
    }

    /**
     * Turn dose green.
     *
     * @param actionEvent the action event
     */
    @FXML
    public void turnDoseGreen(ActionEvent actionEvent) {
        cmbDose.setStyle("-fx-border-color: #029d02");
    }

    /**
     * Turn vaccine green.
     *
     * @param actionEvent the action event
     */
    @FXML
    public void turnVaccineGreen(ActionEvent actionEvent) {
        cmbVaccine.setStyle("-fx-border-color: #029d02");
    }

    /**
     * Show user data.
     *
     * @param actionEvent the action event
     */
    @FXML
    public void showUserData(ActionEvent actionEvent) {
        new ShowUserDataGUI().run();
    }

}
