package app.controller.gui;

import app.controller.App;
import app.domain.model.AdverseReactions;
import app.domain.model.SNSUser;
import app.domain.model.dto.SNSUserDTO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class ShowUserDataController implements Serializable {
    @FXML
    private Label txtPhoneNumber;
    @FXML
    private Label txtBirthDate;
    @FXML
    private Label txtName;
    @FXML
    private Label txtSnsUserNumber;
    @FXML
    private Label txtAddress;
    @FXML
    private Label txtEmail;
    @FXML
    private Label txtGender;
    @FXML
    private Label txtAge;
    @FXML
    private Label txtCitizen;
    @FXML
    private Label txtAdverseReac;
    @FXML
    private Button btnHistory;

    private static String snsUserNumber;

    public void initialize() {
        //App.getInstance().getCompany().getUserStore().addToArrayList();
        SNSUser user = App.getInstance().getCompany().getSnsUserStore().getSNSUserWithAGivenSNSUserNumber(snsUserNumber);
        //App.getInstance().getCompany().getAdverseReactionsStore().addToAdverseReactionsList(new AdverseReactions("999888777","Fever"));
        userDataToString(user.getSNSUserDTO());
    }

    public static void setSnsUserNumber(String snsUserNumber) {
        ShowUserDataController.snsUserNumber = snsUserNumber;
    }

    public void userDataToString(SNSUserDTO dto) {
        AdverseReactions userAdverseReactions = App.getInstance().getCompany().getAdverseReactionsStore().getAdverseReactionsBySnsNumber(dto.getSnsUserNumber());
        String adverseReactions = "";
        if (userAdverseReactions == null || userAdverseReactions.getAdverseReactionsDTO().getAdverseReactions().length() == 0) {
            adverseReactions = "No Adverse Reactions registered in the system";
        } else {
            adverseReactions = userAdverseReactions.getAdverseReactionsDTO().getAdverseReactions();
        }

        txtName.setText(dto.getName());
        txtAddress.setText(dto.getAddress());
        txtPhoneNumber.setText(dto.getPhoneNumber());
        txtEmail.setText(dto.getEmail());
        txtCitizen.setText(dto.getCitizenCardNumber());
        txtGender.setText(dto.getGender());
        txtBirthDate.setText(dto.getBirthDate());
        txtAge.setText(String.valueOf(Period.between(LocalDate.parse(dto.getBirthDate(), DateTimeFormatter.ofPattern("dd-MM-yyyy")), LocalDate.now()).getYears()));
        txtSnsUserNumber.setText(dto.getSnsUserNumber());
        txtAdverseReac.setText(adverseReactions);

    }

    @FXML
    public void openHistory(ActionEvent actionEvent) throws IOException {
        VaccinationHistoryController.setSnsUserNumber(snsUserNumber);
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("VaccinationHistory.fxml"));
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setTitle("Vaccination History");
        stage.getIcons().add(new Image("images/DGS_logo.png"));
        stage.setScene(scene);
        stage.show();
        stage.setAlwaysOnTop(true);
    }
}
