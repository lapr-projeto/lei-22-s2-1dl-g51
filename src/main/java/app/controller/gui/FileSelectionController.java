package app.controller.gui;

import app.domain.model.VaccineAdministration;
import app.domain.model.store.VaccineAdministrationStore;
import app.ui.gui.CenterCoordinatorGUI;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FileSelectionController implements Serializable {

    @javafx.fxml.FXML
    private Button btnBack;
    @javafx.fxml.FXML
    private Button btnChooseFile;
    @javafx.fxml.FXML
    private Button btnAnalyseFile;
    @javafx.fxml.FXML
    private ComboBox cmbFunction;
    @javafx.fxml.FXML
    private ListView list;
    @javafx.fxml.FXML
    private ToggleButton ascendantButton;
    @javafx.fxml.FXML
    private ToggleButton descendantButton;
    @javafx.fxml.FXML
    private TextField txtFileName;

    private ArrayList<VaccineAdministration> vaList;
    @Deprecated
    public void initialize(){
        //table.setVisible(false);
        list.setVisible(false);
        ToggleGroup group = new ToggleGroup();
        ascendantButton.setToggleGroup(group);
        descendantButton.setToggleGroup(group);

        List<String> functions = new ArrayList<>();
        functions.add("Selection Sort by arrival time");
        functions.add("Bubble Sort by arrival time");
        functions.add("Selection Sort by leaving time");
        functions.add("Bubble Sort by leaving time");
        ObservableList<String> list = FXCollections.observableList(functions);
        cmbFunction.setItems(list);
    }

    /**
     * Open file.
     *
     * @param actionEvent the action event
     */
    @javafx.fxml.FXML
    public void openFile(ActionEvent actionEvent) {


        Stage stage = (Stage) ((Node)(actionEvent).getSource()).getScene().getWindow();
        final FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("CSV Files", "*.csv")
        );
        File file = fileChooser.showOpenDialog (stage);
        txtFileName.setVisible(true);
        txtFileName.setText(file.getName());
        txtFileName.setStyle("-fx-border-color: green");



        vaList = VaccineAdministrationStore.getVaListFromFile(file);


    }

    @javafx.fxml.FXML
    public void backAction(ActionEvent actionEvent) {
        //table.setVisible(false);
        list.setVisible(false);
        btnChooseFile.setVisible(true);

        Stage stage = (Stage) btnBack.getScene().getWindow();
        stage.close();
        new CenterCoordinatorGUI().run();

    }


    @javafx.fxml.FXML
    public void analyseBtnAction(ActionEvent actionEvent) {


        btnChooseFile.setVisible(false);

        boolean order= descendantButton.isSelected()?false:true;
        order = ascendantButton.isSelected()?true:false;

        if(cmbFunction.getValue().toString().equalsIgnoreCase("Selection Sort by arrival time")) {
            long startTime = System.currentTimeMillis();

            VaccineAdministrationStore.selectionSortByArrivalTime(vaList,order);
            long endTime = System.currentTimeMillis();
            System.out.println("That took " + (endTime - startTime) + " milliseconds");

        }
        else if(cmbFunction.getValue().toString().equalsIgnoreCase("Bubble Sort by arrival time")) {
            long startTime = System.currentTimeMillis();

            VaccineAdministrationStore.bubbleSortByArrivalTime(vaList,order);
            long endTime = System.currentTimeMillis();
            System.out.println("That took " + (endTime - startTime) + " milliseconds");

        }
        else if(cmbFunction.getValue().toString().equalsIgnoreCase("Selection Sort by leaving time")) {
            long startTime = System.currentTimeMillis();

            VaccineAdministrationStore.selectionSortByLeavingTime(vaList,order);
            long endTime = System.currentTimeMillis();
            System.out.println("That took " + (endTime - startTime) + " milliseconds");

        }
        else if (cmbFunction.getValue().toString().equalsIgnoreCase("Bubble Sort by arrival time")){
            long startTime = System.currentTimeMillis();

            VaccineAdministrationStore.bubbleSortByLeavingTime(vaList,order);
            long endTime = System.currentTimeMillis();
            System.out.println("That took " + (endTime - startTime) + " milliseconds");
        }

        //System.out.println(cmbFunction.getValue());
        list.setItems(FXCollections.observableArrayList(vaList));
        list.setVisible(true);


    }
}
