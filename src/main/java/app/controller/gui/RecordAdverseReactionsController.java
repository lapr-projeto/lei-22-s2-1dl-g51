package app.controller.gui;

import app.controller.App;
import app.domain.model.SNSUser;
import app.ui.console.utils.SaveReadCompany;
import app.ui.gui.ShowUserDataGUI;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * The type Record adverse reactions controller.
 */
public class RecordAdverseReactionsController implements Serializable {
    @javafx.fxml.FXML
    private TextField search;
    @javafx.fxml.FXML
    private ListView list;
    @javafx.fxml.FXML
    private Button btnBack;
    @javafx.fxml.FXML
    private Button btnSave;
    @javafx.fxml.FXML
    private Button btnUserData;
    @javafx.fxml.FXML
    private TextArea txtAdverseReactions;


    /**
     * Initialize.
     */
    public void initialize(){
        btnSave.setVisible(false);
        btnUserData.setVisible(false);
        txtAdverseReactions.setStyle("-fx-border-width: 1 1 1 1; -fx-border-radius: 2%");
        search.setFocusTraversable(false);
        list.setVisible(false);
        txtAdverseReactions.setVisible(false);

       /* LoadCSVFileController ctrl = new LoadCSVFileController();
        ctrl.createSNSUsers(new File("CSVFilesWithUsers/SNSUserDataFromGaoFuNationalCenterDoPortoVaccinationCenter.csv"));
        */
        ArrayList<String> snsNumbers = new ArrayList<>();
        for (SNSUser sns : App.getInstance().getCompany().getUserStore().getSnsUserList()) {
            snsNumbers.add(sns.getSNSUserDTO().getSnsUserNumber());
        }

        ObservableList<String> list1 = FXCollections.observableArrayList(snsNumbers);

        FilteredList<String> filteredData = new FilteredList<>(list1, s -> true);
        list.setItems(filteredData);
        search.textProperty().addListener(obs->{
            String filter = search.getText();
            if(filter == null || filter.length() == 0) {
                filteredData.setPredicate(s -> true);
            }
            else {
                filteredData.setPredicate(s -> s.startsWith(filter));
            }
        });


    }

    /**
     * Show list.
     *
     * @param event the event
     */
    @javafx.fxml.FXML
    public void showList(Event event) {
        list.setVisible(true);
        txtAdverseReactions.setVisible(false);
        btnSave.setVisible(false);
        btnUserData.setVisible(false);
    }

    /**
     * Gets sns number.
     *
     * @param event the event
     */
    @javafx.fxml.FXML
    public void getSNSNumber(Event event) {
        search.setText(String.valueOf(list.getSelectionModel().getSelectedItem()));
        list.setVisible(false);
        txtAdverseReactions.setVisible(true);
        btnSave.setVisible(true);
        btnUserData.setVisible(true);
    }

    /**
     * Save.
     *
     * @param actionEvent the action event
     */
    @javafx.fxml.FXML
    public void save(ActionEvent actionEvent) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        stage.setAlwaysOnTop(false);
        App.getInstance().getCompany().getAdverseReactionsStore().setAdverseReactions(search.getText(),txtAdverseReactions.getText());
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setContentText("Adverse Reactions Saved Successfully!");
        alert.showAndWait();
        txtAdverseReactions.setVisible(false);
        btnSave.setVisible(false);
        btnUserData.setVisible(false);
        search.clear();
        SaveReadCompany.saveCompany(App.getInstance().getCompany());
    }

    /**
     * Back.
     *
     * @param actionEvent the action event
     */
    @javafx.fxml.FXML
    public void back(ActionEvent actionEvent) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        stage.close();
    }

    /**
     * Show user data.
     *
     * @param actionEvent the action event
     */
    @javafx.fxml.FXML
    public void showUserData(ActionEvent actionEvent) {
        ShowUserDataController.setSnsUserNumber(search.getText());
        new ShowUserDataGUI().run();
    }
}
