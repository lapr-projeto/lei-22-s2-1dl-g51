package app.controller.gui;

import app.controller.App;
import app.domain.model.PerformanceData;
import app.domain.model.store.PerformanceAnalysisStore;
import app.ui.console.utils.Utils;
import app.ui.gui.CenterCoordinatorGUI;
import app.ui.gui.PerfomancePopUpGUI;
import com.isep.mdis.Sum;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.InvalidObjectException;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

/**
 * The type Performance analysis controller.
 */
public class PerformanceAnalysisController implements Serializable {

    @javafx.fxml.FXML
    private Label lblFileName;
    @javafx.fxml.FXML
    private DatePicker dateChooser;
    @javafx.fxml.FXML
    private TextField lblTime;
    @javafx.fxml.FXML
    private Button btnBack;

    private static List<Integer> performance;

    private static List<Integer> contList;

    private static int maxSumContList;
    @javafx.fxml.FXML
    private ComboBox cmbAlgorithm;
    @javafx.fxml.FXML
    private Button btnChooseFile;
    @javafx.fxml.FXML
    private Button btnAnalyseFile;

    /**
     * Initialize.
     */
    public void initialize() {
        ObservableList<String> list = FXCollections.observableArrayList("Brute-Force Algorithm", "Benchmark Algorithm");
        cmbAlgorithm.setItems(list);
        lblFileName.getStyleClass().add("labelBlack");
    }

    /**
     * Open file.
     *
     * @param actionEvent the action event
     */
    @javafx.fxml.FXML
    public void openFile(ActionEvent actionEvent) {

        Stage stage = (Stage) ((Node) (actionEvent).getSource()).getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("CSV Files", "*.csv")
        );
        File file = fileChooser.showOpenDialog(stage);
        lblFileName.setText(file.getName());
        lblFileName.getStyleClass().add("labelGreen");

        try {
            PerformanceAnalysisStore.validateFile(file);
            PerformanceAnalysisStore.dataList(file);
        } catch (InvalidObjectException e) {
            lblFileName.getStyleClass().add("labelRed");
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(e.getMessage());
            alert.showAndWait();
        }

    }


    /**
     * Analyse btn action.
     *
     * @param actionEvent the action event
     */
    @javafx.fxml.FXML
    public void analyseBtnAction(ActionEvent actionEvent) {


        if (dateChooser.getValue() != null && lblTime != null && cmbAlgorithm.getValue() != null && lblTime.getText().length() != 0) {

            int inputInterval = Integer.parseInt(lblTime.getText());
            LocalDate inputDate = dateChooser.getValue();
           // inputDate.format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
            List<PerformanceData> data = PerformanceAnalysisStore.getPerformanceList();


                try {
                    performance = App.getInstance().getCompany().getPerformanceAnalysisStore().analyse(inputInterval, inputDate, data);
                    if (cmbAlgorithm.getValue().toString().equalsIgnoreCase("Brute-Force Algorithm")) {
                        contList = PerformanceAnalysisStore.contiguousSublist(performance);
                    }
                    if (cmbAlgorithm.getValue().toString().equalsIgnoreCase("Benchmark Algorithm")) {
                        PerformanceAnalysisStore.contiguousSublist(performance);
                        Integer[] array = Arrays.copyOfRange(performance.toArray(), 0, performance.size(), Integer[].class);
                        int[] primitiveArray = Utils.toint(array);
                        int[] benchMarkArray = Sum.Max(primitiveArray);

                        Integer[] arraySum = Utils.toConvertInteger(benchMarkArray);
                        contList = Arrays.asList(arraySum);
                    }
                    maxSumContList = PerformanceAnalysisStore.maxSumContiguousSublist(contList);
                    new PerfomancePopUpGUI().run();


                } catch (InvalidObjectException e) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setContentText(e.getMessage());
                    alert.showAndWait();
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Please fill all the requirements!");
                alert.showAndWait();
            }


    }

    /**
     * Gets performance.
     *
     * @return the performance
     */
    public static List<Integer> getPerformance() {
        return performance;
    }

    /**
     * Gets cont list.
     *
     * @return the cont list
     */
    public static List<Integer> getContList() {
        return contList;
    }

    /**
     * Gets max sum cont list.
     *
     * @return the max sum cont list
     */
    public static int getMaxSumContList() {
        return maxSumContList;
    }

    /**
     * Back action.
     *
     * @param actionEvent the action event
     */
    @javafx.fxml.FXML
    public void backAction(ActionEvent actionEvent) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        stage.close();
        new CenterCoordinatorGUI().run();
    }

    /**
     * Gets analyse date.
     *
     * @param actionEvent the action event
     */
    @javafx.fxml.FXML public void getAnalyseDate(ActionEvent actionEvent) {
    }}
