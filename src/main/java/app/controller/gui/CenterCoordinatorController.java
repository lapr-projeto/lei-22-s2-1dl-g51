package app.controller.gui;

import app.controller.App;
import app.controller.VaccinationStatisticsController;
import app.domain.model.employees.CenterCoordinator;
import app.ui.console.utils.SaveReadCompany;
import app.ui.gui.FileSelectionGUI;
import app.ui.gui.LoginGUI;
import app.ui.gui.PerformanceAnalysisUI;
import app.ui.gui.VaccinationStatisticsUI;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import pt.isep.lei.esoft.auth.AuthFacade;
import pt.isep.lei.esoft.auth.domain.model.Email;

import javax.management.InstanceNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Center coordinator controller.
 */
public class CenterCoordinatorController implements Serializable {
    @javafx.fxml.FXML
    private ComboBox cmbFunction;
    @javafx.fxml.FXML
    private Button btnLogout;
    @javafx.fxml.FXML
    private Label lblHello;
    @javafx.fxml.FXML
    private Button btnGo;
    @javafx.fxml.FXML
    private Label lblVaccinationCenter;

    /**
     * Initialize.
     */
    public void initialize(){
        AuthFacade authFacade = App.getInstance().getCompany().getAuthFacade();
        Email email = authFacade.getCurrentUserSession().getUserId();
        String name = authFacade.getUser(email.toString()).get().getName();
        try {
            CenterCoordinator cc = App.getInstance().getCompany().getEmployeeStore().getCenterCoordinatorByEmail(email.toString());
            String vaccinationCenter = cc.getVaccinationCenter().getVaccinationCenterDTO().getName();
            lblVaccinationCenter.setText(vaccinationCenter);
            VaccinationStatisticsController.setCc(cc);

        } catch (InstanceNotFoundException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(e.getMessage());
            alert.showAndWait();
        }
        lblHello.setText("Hello, "+ name);
        List<String> functions = new ArrayList<>();
        functions.add("Check and Export Vaccination Statistics");
        functions.add("Analyse Performance of the Center");
        functions.add("Import Data from a Legacy System");
        ObservableList<String> list = FXCollections.observableList(functions);
        cmbFunction.setItems(list);
    }

    /**
     * Go function.
     *
     * @param actionEvent the action event
     */
    @javafx.fxml.FXML
    public void goFunction(ActionEvent actionEvent) {
        Stage stage = (Stage)((Node)(actionEvent).getSource()).getScene().getWindow();
        if(cmbFunction.getValue().toString().equalsIgnoreCase("Check and Export Vaccination Statistics")) {
            stage.setAlwaysOnTop(false);
            new VaccinationStatisticsUI().run();
        }
        else if(cmbFunction.getValue().toString().equalsIgnoreCase("Analyse Performance of the Center")) {
            stage.close();
            new PerformanceAnalysisUI().run();
        }
        else if(cmbFunction.getValue().toString().equalsIgnoreCase("Import Data from a Legacy System")) {
            stage.close();
            new FileSelectionGUI().run();
        }


    }

    /**
     * Do logout.
     *
     * @param actionEvent the action event
     */
    @javafx.fxml.FXML
    public void doLogout(ActionEvent actionEvent) {
        Stage stage = (Stage)((Node)(actionEvent).getSource()).getScene().getWindow();
        stage.close();
        SaveReadCompany.saveCompany(App.getInstance().getCompany());
        new LoginGUI().run();
    }
}
