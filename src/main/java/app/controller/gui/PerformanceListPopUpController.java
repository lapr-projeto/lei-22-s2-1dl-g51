package app.controller.gui;


import app.controller.App;
import app.domain.model.store.PerformanceAnalysisStore;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * The type Performance list pop up controller.
 */
public class PerformanceListPopUpController implements Initializable, Serializable {

    @javafx.fxml.FXML
    private Button btnBackPU;
    @javafx.fxml.FXML
    private ListView listPerformance;
    @javafx.fxml.FXML
    private Label lblPerformance;
    @javafx.fxml.FXML
    private Label lblTimeInterval;
    @javafx.fxml.FXML
    private ListView listMax;
    @javafx.fxml.FXML
    private Label lblSumMaxList;
    @javafx.fxml.FXML
    private Label lblMaxTimeInt;
    @javafx.fxml.FXML
    private Label lblSumMaxContList;
    @javafx.fxml.FXML
    private Label lblMaxContList;
    @javafx.fxml.FXML
    private Label lblMaxListInterval;

    /**
     * Back action.
     *
     * @param actionEvent the action event
     */
    @javafx.fxml.FXML
    public void backAction(ActionEvent actionEvent) {

        Stage stage = (Stage) btnBackPU.getScene().getWindow();
        stage.close();

    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        lblPerformance.setStyle("-fx-border-color: black");
        lblTimeInterval.setStyle("-fx-border-color: black");
        lblSumMaxContList.setStyle("-fx-border-color: black");
        lblMaxListInterval.setStyle("-fx-border-color: black");
        lblMaxContList.setStyle("-fx-border-color: black");

        ArrayList<String> tablePrint = new ArrayList<>();

        for (int i = 0; i < PerformanceAnalysisController.getPerformance().size(); i++) {
            tablePrint.add(String.format("%-20s %20s", App.getInstance().getCompany().getPerformanceAnalysisStore().getIntervalTime().get(i).toString(), PerformanceAnalysisController.getPerformance().get(i).toString()));
        }

        listPerformance.setItems(FXCollections.observableArrayList(tablePrint));

        listMax.setItems(FXCollections.observableArrayList(PerformanceAnalysisController.getContList()));

        lblSumMaxList.setText(String.valueOf(PerformanceAnalysisController.getMaxSumContList()));

        lblMaxTimeInt.setText(App.getInstance().getCompany().getPerformanceAnalysisStore().getIntervalTime().get(PerformanceAnalysisStore.getStart()).toString2() + " - " + App.getInstance().getCompany().getPerformanceAnalysisStore().getIntervalTime().get(PerformanceAnalysisStore.getEnd()+1).toString2());

    }


}


