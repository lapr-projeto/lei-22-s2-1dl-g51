package app.controller;

import app.domain.model.Company;
import app.domain.model.SNSUser;
import app.domain.model.store.SNSUserStore;

import java.io.Serializable;
import java.util.Date;

/*+
Represents the controller for the registration of new SNS User
 */
public class RegisterSNSUserController implements Serializable {
    private SNSUserStore store;

    private Company company;

    /**
     * Creates a new object of the RegisterSNSUserController class and assigns a SNSUserStore to validate and keep his data.
     */
    public RegisterSNSUserController() {
        this.company = App.getInstance().getCompany();
        store = company.getUserStore();
    }


    /**
     * Creates a new SNSUser if the store validates his data.
     *
     * @param name              SNSUser name
     * @param address           SNSUser address
     * @param phoneNumber       SNSUser phoneNumber
     * @param email             SNSUser email
     * @param citizenCardNumber SNSUser citizenCardNumber
     * @param gender            SNSUser gender
     * @param birthDate         SNSUser birthDate
     * @param snsUserNumber     SNSUser snsUserNumber
     * @return the SNSUser if all the data is valid
     */
    public SNSUser createSNSUser(String name, String address, String phoneNumber, String email, String citizenCardNumber, String gender, Date birthDate, String snsUserNumber) {
        SNSUser user = new SNSUser(name, address, phoneNumber, email, citizenCardNumber, gender, birthDate, snsUserNumber);
        if (store.validateSNSUser(user))
            return user;
        return null;
    }

    /**
     * Saves the created SNSUser if his data is confirmed in the UI.
     *
     * @param user - SNS User
     */
    public void saveSNSUser(SNSUser user) {
        if (store.validateSNSUser(user)) {
            if (store.addSNSUser(user))
                System.out.print("\nSNS User added successfully!\n\n");
        }
    }




}

