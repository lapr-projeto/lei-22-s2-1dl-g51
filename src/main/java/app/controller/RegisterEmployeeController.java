package app.controller;

import app.domain.model.Company;
import app.domain.model.EmployeesList;
import app.domain.model.employees.Employee;
import app.domain.model.store.EmployeeStore;
import app.ui.console.utils.Utils;

import java.io.Serializable;

/*+
Represents the controller for the Administrator
 */

public class RegisterEmployeeController implements Serializable {

    private EmployeeStore store;

    private Company company;

/**
 * Creates a new object of the EmployeeStore class and assigns a store to validate and keep their data.
 */

    public RegisterEmployeeController(){
        company = App.getInstance().getCompany();
        store = company.getEmployeeStore();
    }

    /**
     * Registers a new Employee if the store validates their data.
     * @param employee
     */

    public void registerEmployee(Employee employee){
        if (store.validateEmployeeData(employee)){
            Employee employee1 = new Employee(employee.getName(),employee.getAddress(), employee.getPhoneNumber(), employee.getEmail(), employee.getCitizenCardNumber());
        }

    }

    /**
     * Saves the created Employee if their data is confirmed in the UI.
     * @param employee
     */


    public void saveEmployee(Employee employee){
        System.out.printf("\n%s\n", employee);
        if (Utils.confirm("Confirm the data? (y/n)")) {
            if (store.addEmployee(employee)) {
                System.out.print("\n" + employee.getClass().getSimpleName() + " added successfully!");
                EmployeesList.addToEmployeeList(employee);
            }
        } else {
            System.out.print("\nInvalid Option!");
        }
    }

}


