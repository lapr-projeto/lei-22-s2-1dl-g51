package app.controller;

import app.domain.model.SNSUser;
import app.domain.model.store.SNSUserStore;
import app.ui.console.utils.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.Scanner;

/**
 * The type Load csv file controller.
 */
public class LoadCSVFileController implements Serializable {
    /**
     * Verify if file has header boolean.
     *
     * @param file the file
     * @return the boolean
     */
    public static boolean verifyIfFileHasHeader(File file){
        try {
            Scanner readFile = new Scanner(file,"UTF-8");
            String firstLine = readFile.nextLine();
            if(firstLine.contains(";"))
                return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return false;

    }

    /**
     * Create sns users.
     *
     * @param file the file
     */
    public void createSNSUsers(File file) {
        RegisterSNSUserController controller = new RegisterSNSUserController();
        SNSUserStore store = App.getInstance().getCompany().getUserStore();
        int counterUsersLoaded = 0;
        boolean invalidFile = false;
        for (SNSUser user : SNSUserStore.getUserFromFile(file)) {
            SNSUser snsUser = controller.createSNSUser(user.getName(), user.getAddress(), user.getPhoneNumber(), user.getEmail(), user.getCitizenCardNumber(), user.getGender(), user.getBirthDate(), user.getSnsUserNumber());
            if (snsUser == null) {
                invalidFile = true;
                System.out.println("Invalid User data! The file has been discarded!");
            }
        }


        if (!invalidFile) {
            for (SNSUser user : SNSUserStore.getUserFromFile(file)) {
                SNSUser snsUser = controller.createSNSUser(user.getName(), user.getAddress(), user.getPhoneNumber(), user.getEmail(), user.getCitizenCardNumber(), user.getGender(), user.getBirthDate(), user.getSnsUserNumber());
                /*System.out.println(snsUser);
                if (Utils.confirm("Confirm the data? (y/n)")) {
                controller.saveSNSUser(snsUser);
                }*/
                if(!store.getSnsUserList().contains(snsUser)){
                    store.getSnsUserList().add(snsUser);
                    counterUsersLoaded++;
                }
            }
            if(counterUsersLoaded != 0) {
                System.out.printf("%d SNS Users have been loaded successfully!", counterUsersLoaded);
            }
            else {
                System.out.println("No users have been loaded!");
            }

        }
    }

    /**
     * Show vacine administration.
     *
     * @param file the file
     */
    public void showVacineAdministration(File file){
        VaccineAdminstrationController ctrl= new VaccineAdminstrationController();
        Utils.showList(ctrl.getVaStore().getVaList(),"\nSorted by arrival time");
    }

}
