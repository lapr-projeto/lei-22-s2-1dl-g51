package app.domain.shared;

import java.io.Serializable;

/**
 * The type Constants.
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public abstract class Constants implements Serializable {
    /**
     * The constant ROLE_ADMIN.
     */
    public static final String ROLE_ADMIN = "ADMINISTRATOR";
    /**
     * The constant ROLE_RECEPTIONIST.
     */
    public static final String ROLE_RECEPTIONIST = "RECEPTIONIST";
    /**
     * The constant ROLE_NURSE.
     */
    public static final String ROLE_NURSE = "NURSE";
    /**
     * The constant ROLE_CENTER_COORDINATOR.
     */
    public static final String ROLE_CENTER_COORDINATOR = "CENTER_COORDINATOR";
    /**
     * The constant ROLE_SNS_USER.
     */
    public static final String ROLE_SNS_USER= "SNS_USER";
    /**
     * The constant PARAMS_FILENAME.
     */
    public static final String PARAMS_FILENAME = "src/main/resources/Config/config.properties";
    /**
     * The constant PARAMS_COMPANY_DESIGNATION.
     */
    public static final String PARAMS_COMPANY_DESIGNATION = "Company.Designation";
}