package app.domain.model.dto;

import app.domain.model.VaccinationCenter.VaccinationCenter;

import java.io.Serializable;
import java.util.Date;

/**
 * The type Arrival time dto.
 */
public class ArrivalTimeDTO implements Serializable {
    private String snsNum;
    private Date arrivalTime;
    private VaccinationCenter vc;
    private Date scheduleDate;
    private String scheduleHour;

    /**
     * Instantiates a new Arrival time dto.
     *
     * @param snsNum       the sns num
     * @param vc           the vc
     * @param scheduleDate the schedule date
     * @param scheduleHour the schedule hour
     */
    public ArrivalTimeDTO(String snsNum, VaccinationCenter vc,Date scheduleDate, String scheduleHour) {
        this.snsNum = snsNum;
        this.arrivalTime = new Date();
        this.vc = vc;
        this.scheduleDate = scheduleDate;
        this.scheduleHour = scheduleHour;
    }

    /**
     * Gets sns num.
     *
     * @return the sns num
     */
    public String getSnsNum() {
        return snsNum;
    }

    /**
     * Gets vc.
     *
     * @return the vc
     */
    public VaccinationCenter getVc() {
        return vc;
    }

    /**
     * Gets arrival time.
     *
     * @return the arrival time
     */
    public Date getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Gets schedule date.
     *
     * @return the schedule date
     */
    public Date getScheduleDate() {
        return scheduleDate;
    }

    /**
     * Gets schedule hour.
     *
     * @return the schedule hour
     */
    public String getScheduleHour() {
        return scheduleHour;
    }
}
