package app.domain.model.dto;

import app.domain.model.VaccineTypes;

import java.io.Serializable;

/**
 * The type Vaccine type dto.
 */
public class VaccineTypeDTO implements Serializable {

    private VaccineTypes type;
    private String code;
    private String designation;
    private String whoId;

    /**
     * Instantiates a new Vaccine type dto.
     *
     * @param type        the type
     * @param code        the code
     * @param designation the designation
     * @param whoId       the who id
     */
    public VaccineTypeDTO(VaccineTypes type, String code, String designation, String whoId) {
        this.code = code;
        this.designation = designation;
        this.whoId = whoId;
        this.type = type;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets designation.
     *
     * @return the designation
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * Gets who id.
     *
     * @return the who id
     */
    public String getWhoId() {
        return whoId;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public VaccineTypes getType() {
        return type;
    }
}
