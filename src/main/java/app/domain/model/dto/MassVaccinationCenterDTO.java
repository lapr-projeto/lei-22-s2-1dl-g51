package app.domain.model.dto;

import app.domain.model.VaccineType;

import java.io.Serializable;

/**
 * The type Mass vaccination center dto.
 */
public class MassVaccinationCenterDTO extends VaccinationCenterDTO implements Serializable {
    private VaccineType vaccineType;

    /**
     * Instantiates a new Mass vaccination center dto.
     *
     * @param name            the name
     * @param address         the address
     * @param phoneNumber     the phone number
     * @param email           the email
     * @param openHours       the open hours
     * @param closingHours    the closing hours
     * @param vaccinesSlotCap the vaccines slot cap
     * @param slotDuration    the slot duration
     * @param vaccineType     the vaccine type
     */
    public MassVaccinationCenterDTO(String name, String address, String phoneNumber, String email, String openHours,
                                    String closingHours, String vaccinesSlotCap, String slotDuration, VaccineType vaccineType) {
        super(name, address, phoneNumber, email, openHours, closingHours, vaccinesSlotCap, slotDuration);
        this.vaccineType = vaccineType;
    }

    /**
     * Gets vaccine type.
     *
     * @return the vaccine type
     */
    public VaccineType getVaccineType() {
        return vaccineType;
    }
}
