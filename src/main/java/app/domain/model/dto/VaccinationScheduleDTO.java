package app.domain.model.dto;

import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.domain.model.VaccineType;

import java.io.Serializable;
import java.util.Date;

/**
 * Represents a DTO for the Vaccination Schedule
 */
public class VaccinationScheduleDTO implements Serializable {

    private String snsUserNumber;

    private VaccinationCenter vaccinationCenter;

    private Date date;

    private String time;

    private VaccineType vaccineType;

    /**
     * Instantiates a new Vaccination schedule dto.
     *
     * @param snsUserNumber     the sns user number
     * @param vaccinationCenter the vaccination center
     * @param date              the date
     * @param time              the time
     * @param vaccineType       the vaccine type
     */
    public VaccinationScheduleDTO(String snsUserNumber, VaccinationCenter vaccinationCenter, Date date, String time, VaccineType vaccineType) {
        this.snsUserNumber = snsUserNumber;
        this.vaccinationCenter = vaccinationCenter;
        this.date = date;
        this.time = time;
        this.vaccineType = vaccineType;
    }

    /**
     * Gets sns user number.
     *
     * @return the sns user number
     */
    public String getSnsUserNumber() {
        return snsUserNumber;
    }

    /**
     * Gets vaccination center.
     *
     * @return the vaccination center
     */
    public VaccinationCenter getVaccinationCenter() {
        return vaccinationCenter;
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Gets time.
     *
     * @return the time
     */
    public String getTime() {
        return time;
    }

    /**
     * Gets vaccine type.
     *
     * @return the vaccine type
     */
    public VaccineType getVaccineType() {
        return vaccineType;
    }
}
