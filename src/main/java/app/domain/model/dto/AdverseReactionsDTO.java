package app.domain.model.dto;

import java.io.Serializable;

/**
 * The type Adverse reactions dto.
 */
public class AdverseReactionsDTO implements Serializable {

    private String snsUserNumber;

    private String adverseReactions;

    /**
     * Instantiates a new Adverse reactions dto.
     *
     * @param snsUserNumber    the sns user number
     * @param adverseReactions the adverse reactions
     */
    public AdverseReactionsDTO(String snsUserNumber, String adverseReactions) {
        this.snsUserNumber = snsUserNumber;
        this.adverseReactions = adverseReactions;
    }

    /**
     * Gets sns user number.
     *
     * @return the sns user number
     */
    public String getSnsUserNumber() {
        return snsUserNumber;
    }

    /**
     * Gets adverse reactions.
     *
     * @return the adverse reactions
     */
    public String getAdverseReactions() {
        return adverseReactions;
    }

    /**
     * Sets adverse reactions.
     *
     * @param adverseReactions the adverse reactions
     */
    public void setAdverseReactions(String adverseReactions) {
        this.adverseReactions = adverseReactions;
    }
}
