package app.domain.model.dto;

import app.domain.model.VaccineType;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * The type Health center dto.
 */
public class HealthCenterDTO extends VaccinationCenterDTO implements Serializable {
    private ArrayList<VaccineType> vaccineTypes;

    /**
     * Instantiates a new Health center dto.
     *
     * @param name            the name
     * @param address         the address
     * @param phoneNumber     the phone number
     * @param email           the email
     * @param openHours       the open hours
     * @param closingHours    the closing hours
     * @param vaccinesSlotCap the vaccines slot cap
     * @param slotDuration    the slot duration
     * @param vaccineTypes    the vaccine types
     */
    public HealthCenterDTO(String name, String address, String phoneNumber, String email, String openHours, String closingHours, String vaccinesSlotCap, String slotDuration, ArrayList<VaccineType> vaccineTypes) {
        super(name, address, phoneNumber, email, openHours, closingHours, vaccinesSlotCap, slotDuration);
        this.vaccineTypes = vaccineTypes;
    }

    /**
     * Gets vaccine types.
     *
     * @return the vaccine types
     */
    public ArrayList<VaccineType> getVaccineTypes() {
        return vaccineTypes;
    }
}
