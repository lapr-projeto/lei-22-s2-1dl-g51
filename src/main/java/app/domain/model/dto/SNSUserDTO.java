package app.domain.model.dto;

import app.ui.console.utils.Utils;

import java.io.Serializable;
import java.util.Date;

/**
 * Represents a DTO for the SNSUser
 */
public class SNSUserDTO implements Serializable {

    private String name;

    private String address;

    private String phoneNumber;

    private String email;

    private String citizenCardNumber;

    private String gender;

    private Date birthDate;

    private String snsUserNumber;

    /**
     * Instantiates a new Sns user dto.
     *
     * @param name              the name
     * @param address           the address
     * @param phoneNumber       the phone number
     * @param email             the email
     * @param citizenCardNumber the citizen card number
     * @param gender            the gender
     * @param birthDate         the birth date
     * @param snsUserNumber     the sns user number
     */
    public SNSUserDTO(String name, String address, String phoneNumber, String email, String citizenCardNumber, String gender, Date birthDate, String snsUserNumber) {
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.citizenCardNumber = citizenCardNumber;
        this.gender = gender;
        this.birthDate = birthDate;
        this.snsUserNumber = snsUserNumber;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Gets phone number.
     *
     * @return the phone number
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Gets citizen card number.
     *
     * @return the citizen card number
     */
    public String getCitizenCardNumber() {
        return citizenCardNumber;
    }

    /**
     * Gets the gender of the SNSUser
     *
     * @return this SNSUser's gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * Gets the birthdate of the SNSUser
     *
     * @return this SNSUser's birthdate
     */
    public String getBirthDate() {
        return Utils.convertToSimpleDataFormat(birthDate);
    }

    /**
     * Gets the sns user number of the SNSUser
     *
     * @return this SNSUser's sns user number
     */
    public String getSnsUserNumber() {
        return snsUserNumber;
    }
}
