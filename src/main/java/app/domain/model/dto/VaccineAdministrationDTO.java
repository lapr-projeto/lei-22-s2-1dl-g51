package app.domain.model.dto;


import app.controller.App;

import java.io.Serializable;

/**
 * The type Vaccine administration dto.
 */
public class VaccineAdministrationDTO implements Serializable {
    private String snsNumber;
    private String vaccineName;
    private String dose;
    private String lotNumber;
    private String scheduleDateTime;
    private String arrivalTime;
    private String leavingTime;
    private String nurseAdministrationDateTime;
    private String vaccinationCenter;

    /**
     * Instantiates a new Vaccine administration dto.
     *
     * @param snsNumber                   the sns number
     * @param vaccineName                 the vaccine name
     * @param dose                        the dose
     * @param lotNumber                   the lot number
     * @param scheduleDateTime            the schedule date time
     * @param arrivalTime                 the arrival time
     * @param nurseAdministrationDateTime the nurse administration date time
     * @param leavingTime                 the leaving time
     */
    public VaccineAdministrationDTO(String snsNumber, String vaccineName, String dose, String lotNumber, String scheduleDateTime, String arrivalTime,String nurseAdministrationDateTime, String leavingTime) {
        this.snsNumber = snsNumber;
        this.vaccineName = vaccineName;
        this.dose = dose;
        this.lotNumber = lotNumber;
        this.scheduleDateTime = scheduleDateTime;
        this.arrivalTime = arrivalTime;
        this.nurseAdministrationDateTime =nurseAdministrationDateTime;
        this.leavingTime = leavingTime;

            if(snsNumber != null)
                this.vaccinationCenter = App.getInstance().getCompany().getVaccinationScheduleStore().getVaccinationCenterBySNSNumber(snsNumber);

    }

    /**
     * Gets sns number.
     *
     * @return the sns number
     */
    public String getSnsNumber() {
        return snsNumber;
    }

    /**
     * Gets vaccine name.
     *
     * @return the vaccine name
     */
    public String getVaccineName() {
        return vaccineName;
    }

    /**
     * Gets dose.
     *
     * @return the dose
     */
    public String getDose() {
        return dose;
    }

    /**
     * Gets lot number.
     *
     * @return the lot number
     */
    public String getLotNumber() {
        return lotNumber;
    }

    /**
     * Gets schedule date time.
     *
     * @return the schedule date time
     */
    public String getScheduleDateTime() {
        return scheduleDateTime;
    }

    /**
     * Gets arrival time.
     *
     * @return the arrival time
     */
    public String getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Gets leaving time.
     *
     * @return the leaving time
     */
    public String getLeavingTime() {
        return leavingTime;
    }

    /**
     * Gets nurse administration date time.
     *
     * @return the nurse administration date time
     */
    public String getNurseAdministrationDateTime() {
        return nurseAdministrationDateTime;
    }

    /**
     * Gets vaccination center.
     *
     * @return the vaccination center
     */
    public String getVaccinationCenter() {
        return vaccinationCenter;
    }
}


