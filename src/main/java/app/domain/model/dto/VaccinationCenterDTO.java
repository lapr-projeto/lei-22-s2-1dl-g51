package app.domain.model.dto;

import java.io.Serializable;

/**
 * The type Vaccination center dto.
 */
public class VaccinationCenterDTO implements Serializable {
    private String name;
    private String address;
    private String phoneNumber;
    private String email;
    private String openHours;
    private String closingHours;
    private String vaccinesSlotCap;
    private String slotDuration;

    /**
     * Instantiates a new Vaccination center dto.
     *
     * @param name            the name
     * @param address         the address
     * @param phoneNumber     the phone number
     * @param email           the email
     * @param openHours       the open hours
     * @param closingHours    the closing hours
     * @param vaccinesSlotCap the vaccines slot cap
     * @param slotDuration    the slot duration
     */
    public VaccinationCenterDTO(String name, String address, String phoneNumber, String email, String openHours, String closingHours, String vaccinesSlotCap, String slotDuration) {
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.openHours = openHours;
        this.closingHours = closingHours;
        this.vaccinesSlotCap = vaccinesSlotCap;
        this.slotDuration = slotDuration;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Gets phone number.
     *
     * @return the phone number
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Gets open hours.
     *
     * @return the open hours
     */
    public String getOpenHours() {
        return openHours;
    }

    /**
     * Gets closing hours.
     *
     * @return the closing hours
     */
    public String getClosingHours() {
        return closingHours;
    }

    /**
     * Gets vaccines slot cap.
     *
     * @return the vaccines slot cap
     */
    public String getVaccinesSlotCap() {
        return vaccinesSlotCap;
    }

    /**
     * Gets slot duration.
     *
     * @return the slot duration
     */
    public String getSlotDuration() {
        return slotDuration;
    }


}
