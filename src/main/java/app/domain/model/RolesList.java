package app.domain.model;


import java.io.Serializable;
import java.util.List;

/**
 * The type Roles list.
 */
public class RolesList implements Serializable {

    private List<String> userRolesList;

    /**
     * Instantiates a new Roles list.
     */
    public RolesList(){

    }

    /**
     * method to get a list of roles
     *
     * @return list of roles
     */
    public List<String> getUserRolesList() {
        userRolesList = Company.getUserRole();
        return userRolesList;
    }
}
