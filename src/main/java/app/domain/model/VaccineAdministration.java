package app.domain.model;

import app.domain.model.dto.VaccineAdministrationDTO;

import java.io.Serializable;
import java.util.Objects;

/**
 * The type Vaccine administration.
 */
public class VaccineAdministration implements Serializable {
    private String snsNumber;
    private String vaccineName;
    private String dose;
    private String lotNumber;
    private String scheduleDateTime;
    private String arrivalTime;
    private String nurseAdministrationDateTime;
    private String leavingTime;
    private VaccineAdministrationDTO vaccineAdministrationDTO;


    /**
     * Instantiates a new Vaccine administration.
     *
     * @param snsNumber                   the sns number
     * @param vaccineName                 the vaccine name
     * @param dose                        the dose
     * @param lotNumber                   the lot number
     * @param scheduleDateTime            the schedule date time
     * @param arrivalTime                 the arrival time
     * @param nurseAdministrationDateTime the nurse administration date time
     * @param leavingTime                 the leaving time
     */
    public VaccineAdministration(String snsNumber, String vaccineName, String dose, String lotNumber, String scheduleDateTime, String arrivalTime,String nurseAdministrationDateTime, String leavingTime) {
        this.snsNumber = snsNumber;
        this.vaccineName = vaccineName;
        this.dose = dose;
        this.lotNumber = lotNumber;
        this.scheduleDateTime = scheduleDateTime;
        this.nurseAdministrationDateTime =nurseAdministrationDateTime;
        this.arrivalTime = arrivalTime;
        this.leavingTime = leavingTime;

        this.vaccineAdministrationDTO = new VaccineAdministrationDTO(snsNumber, vaccineName, dose, lotNumber, scheduleDateTime, arrivalTime,nurseAdministrationDateTime, leavingTime);
    }

    /**
     * Gets vaccine administration dto.
     *
     * @return the vaccine administration dto
     */
    public VaccineAdministrationDTO getVaccineAdministrationDTO() {
        return vaccineAdministrationDTO;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VaccineAdministration that = (VaccineAdministration) o;
        return Objects.equals(snsNumber, that.snsNumber) && Objects.equals(vaccineName, that.vaccineName) && Objects.equals(dose, that.dose) && Objects.equals(lotNumber, that.lotNumber) && Objects.equals(scheduleDateTime, that.scheduleDateTime) && Objects.equals(arrivalTime, that.arrivalTime) && Objects.equals(nurseAdministrationDateTime, that.nurseAdministrationDateTime) && Objects.equals(leavingTime, that.leavingTime);
    }

    @Override
    public String toString() {
        return String.format(" SNS User Number: %s\n Vaccine Name: %s" +
                "\n Dose: %s\n Lot Number: %s\n Scheduled Date Time: %s\n Arrival Date Time: %s" +
                "\n Nurse Administration Date Time: %s\n Leaving Date Time: %s"
                , snsNumber, vaccineName, dose, lotNumber, scheduleDateTime, arrivalTime, nurseAdministrationDateTime, leavingTime);
    }
}
