package app.domain.model.employees;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.Person;
import pt.isep.lei.esoft.auth.AuthFacade;

import java.io.Serializable;


/**
 * Represents an Employee enrolled in the system.
 */

public class Employee extends Person implements Serializable {

    /**
     * The Employees company.
     */

    private Company company;

    /**
     * The Employees ID.
     */

    private int id;

    /**
     * The Employees role ID.
     */

    private String roleID;

    /**
     * The Employee ID generator.
     */

    private static int teamIDCounter = 0;

    /**
     * Creates a new Employee with the given name, address, phoneNumber, email and citizenCardNumber
     * @param name
     * @param address
     * @param phoneNumber
     * @param email
     * @param citizenCardNumber
     */

    public Employee(String name, String address, String phoneNumber, String email, String citizenCardNumber) {
        super(name, address, phoneNumber, email, citizenCardNumber);
        this.id = ++teamIDCounter;
        App app = App.getInstance();
        this.company = app.getCompany();
        AuthFacade authFacade = this.company.getAuthFacade();
    }

    /**
     * Gets the Employee's ID
     * @return this Employee's ID
     */

    public int getId() {
        return id;
    }

    /**
     * Changes the Employee's role ID
     * @param roleID
     */

    public void setRoleID(String roleID) {
        this.roleID = roleID;
    }

    /**
     * Gets the Employee's role ID
     * @return this Employee's role ID
     */

    public String getRoleID() {
        return roleID;
    }


    public String toString() {
        return String.format("ID: %04d\n%s", id,super.toString());
    }




}
