package app.domain.model.employees;

import app.domain.shared.Constants;

import java.io.Serializable;

/**
 * Represents a Receptionist enrolled in the system.
 */

public class Receptionist extends Employee implements Serializable {

    private final static String roleID = Constants.ROLE_RECEPTIONIST;

    /**
     * Creates a new Receptionist with the given name, address, phoneNumber, email and citizenCardNumber, and sets its roleID.
     * @param name
     * @param address
     * @param phoneNumber
     * @param email
     * @param citizenCardNumber
     */


    public Receptionist(String name, String address, String phoneNumber, String email, String citizenCardNumber) {
        super(name, address, phoneNumber, email, citizenCardNumber);
        setRoleID(roleID);
    }

    public String toString(){
        return String.format("%s\n%s",this.getClass().getSimpleName(),super.toString());
    }
}
