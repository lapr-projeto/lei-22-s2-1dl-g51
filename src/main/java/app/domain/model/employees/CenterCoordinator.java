package app.domain.model.employees;

import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.domain.shared.Constants;

import java.io.Serializable;

/**
 * Represents a Center Coordinator enrolled in the system.
 */

public class CenterCoordinator extends Employee implements Serializable {
    private final static String roleID = Constants.ROLE_CENTER_COORDINATOR;

    private VaccinationCenter vaccinationCenter;

/**
 * Creates a new Center Coordinator with the given name, address, phoneNumber, email and citizenCardNumber, and sets its roleID.
 * @param name
 * @param address
 * @param phoneNumber
 * @param email
 * @param citizenCardNumber
 */

    public CenterCoordinator(String name, String address, String phoneNumber, String email, String citizenCardNumber, VaccinationCenter vaccinationCenter) {
        super(name, address, phoneNumber, email, citizenCardNumber);
        this.vaccinationCenter = vaccinationCenter;
        setRoleID(roleID);
    }

    public VaccinationCenter getVaccinationCenter() {
        return vaccinationCenter;
    }

    public String toString(){
        return String.format("Center Coordinator\n%s",super.toString());
    }
}
