package app.domain.model;

import app.domain.model.employees.Employee;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * represents list of employees
 */
public class EmployeesList implements Serializable {

    private static List<Employee> employeesList = new ArrayList<>();;

    public EmployeesList(){
    }

    /**
     * method to add employees to the list
     * @param employee
     */
    public static void addToEmployeeList(Employee employee){
        employeesList.add(employee);
    }

    /**
     * method to get employees list
     * @return list of employees
     */
    public List<Employee> getEmployeesList(){
        return employeesList;
    }
}
