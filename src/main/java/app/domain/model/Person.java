package app.domain.model;

import java.io.Serializable;

/**
 * Represents a person enrolled in the system.
 * Each person has a role.
 */
public abstract class Person implements Serializable {

    /**
     * The name of the person
     */
    private String name;

    /**
     * The address of the person
     */
    private String address;

    /**
     * The phone number of the person
     */
    private String phoneNumber;

    /**
     * The email of the person
     */
    private String email;

    /**
     * The citizen card number of the person
     */
    private String citizenCardNumber;

    /**
     * Creates a new person with the given name, address, phoneNumber, email and citizenCardNumber
     * @param name
     * @param address
     * @param phoneNumber
     * @param email
     * @param citizenCardNumber
     */
    public Person(String name, String address, String phoneNumber, String email, String citizenCardNumber) {
            this.name = name;
            this.address = address;
            this.phoneNumber = phoneNumber;
            this.email = email;
            this.citizenCardNumber = citizenCardNumber;
    }

    /**
     * Gets the name of this person
     * @return this person's name
     */
    public String getName() {
        return name;
    }

    /**
     * Changes the name of this Person
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the address of this person
     * @return this person's address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Changes the address of this Person
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Gets the phone number of this person
     * @return this person's phone number
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Changes the phoneNumber of this Person
     * @param phoneNumber
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Gets the email of this person
     * @return this person's email
     */
    public String getEmail() { return email.toString(); }

    /**
     * Changes the email of this Person
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the citizen card number of this person
     * @return this person's citizen card number
     */
    public String getCitizenCardNumber() {
        return citizenCardNumber;
    }


    public String toString() {
        return String.format("Name: %s\nAddress: %s\nPhone Number: %s\nE-Mail: %s\nCitizen Card Number: %s\n",
                name,address,phoneNumber,email,citizenCardNumber);
    }

}
