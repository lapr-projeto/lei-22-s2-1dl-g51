package app.domain.model;

import app.domain.model.dto.VaccineTypeDTO;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Objects;

/**
 * The type Vaccine type.
 */
public class VaccineType implements Serializable {

    private VaccineTypes type;
    private String code;
    private String designation;
    private String whoId;



    //SPRINT C
    private VaccineTypeDTO vaccineDTO;

    /**
     * Instantiates a new Vaccine type.
     *
     * @param type        the type
     * @param code        the code
     * @param designation the designation
     * @param whoId       the who id
     */
    public VaccineType (VaccineTypes type, String code, String designation, String whoId) {
        if (checkCodeRules(code) && checkDesignationRules(designation)){
            this.type = type;
            this.code = code;
            this.designation = designation;
            this.whoId = whoId;

            //SPRINT C
            this.vaccineDTO = new VaccineTypeDTO(type,code,designation,whoId);
        }
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets designation.
     *
     * @return the designation
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * Gets who id.
     *
     * @return the who id
     */
    public String getWhoId() {
        return whoId;
    }

    @Override
    public String toString() {
        return String.format("\n=========VACCINE TYPE=========\nType: %s\nCode: %s\nDesignation: %s\nWHO ID: %s\n==============================",type.toString(),code,designation,whoId);
    }

    /**
     * To simple string string.
     *
     * @return the string
     */
    public String toSimpleString() {
        return String.format("\n  Type: %s\n  Code: %s\n  Designation: %s\n  WHO ID: %s",type.toString(),code,designation,whoId);
    }

    //método para verificar o codigo da vacina
    private boolean checkCodeRules(String code) {
        if (StringUtils.isBlank(code))
            throw new IllegalArgumentException("Code cannot be blank.");
        if ( (code.length() < 4) || (code.length() > 8))
            throw new IllegalArgumentException("Code must have 4 to 8 chars.");
        return true;
    }

    private boolean checkDesignationRules(String designation) {
        if (StringUtils.isBlank(designation))
            throw new IllegalArgumentException("Designation cannot be blank.");
        if ((designation.length() < 2) || (designation.length() > 40))
            throw new IllegalArgumentException("Designation must have 2 to 40 chars.");
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VaccineType that = (VaccineType) o;
        return Objects.equals(code, that.code) && Objects.equals(designation, that.designation) && Objects.equals(whoId, that.whoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, designation, whoId);
    }

    /**
     * Gets vaccine type dto.
     *
     * @return the vaccine type dto
     */
//SPRINT C
    public VaccineTypeDTO getVaccineTypeDTO() {
        return vaccineDTO;
    }
}
