package app.domain.model;

import java.io.Serializable;

/**
 * The enum Vaccine types.
 */
public enum VaccineTypes implements Serializable {
    /**
     * The Covid 19.
     */
    Covid_19 {
        @Override
        public String toString() {
            return "Covid 19";
        }
    },
    /**
     * Dengue vaccine types.
     */
    Dengue,
    /**
     * The Hepatitis a.
     */
    Hepatitis_A {
                @Override
                public String toString() {
                    return "Hepatitis A";
                }
            },
    /**
     * Flu vaccine types.
     */
    Flu,
    /**
     * Polio vaccine types.
     */
    Polio,
    /**
     * Rabies vaccine types.
     */
    Rabies,
    /**
     * Measles vaccine types.
     */
    Measles,
    /**
     * Mumps vaccine types.
     */
    Mumps,
    /**
     * Rubella vaccine types.
     */
    Rubella,
    /**
     * Rotavirus vaccine types.
     */
    Rotavirus,
    /**
     * Smallpox vaccine types.
     */
    Smallpox,
    /**
     * Chickenpox vaccine types.
     */
    Chickenpox,
    /**
     * The Yellow fever.
     */
    Yellow_fever{
        @Override
        public String toString() {
            return "Yellow Fever";
        }
    },
    /**
     * Hib vaccine types.
     */
    Hib,
    /**
     * The Hepatitis b.
     */
    Hepatitis_B{
        @Override
        public String toString() {
            return "Hepatitis B";
        }
    },
    /**
     * Hpv vaccine types.
     */
    HPV,
    /**
     * The Whooping cough.
     */
    Whooping_cough{
        @Override
        public String toString() {
            return "Whooping Cough";
        }
    },
    /**
     * The Pneumococcal disease.
     */
    Pneumococcal_disease{
        @Override
        public String toString() {
            return "Pneumococcal disease";
        }
    },
    /**
     * The Meningococcal disease.
     */
    Meningococcal_disease {
        @Override
        public String toString() {
            return "Meningococcal disease";
        }
    },
    /**
     * Shingles vaccine types.
     */
    Shingles,
    /**
     * Diphtheria vaccine types.
     */
    Diphtheria,
    /**
     * Tetanus vaccine types.
     */
    Tetanus
}
