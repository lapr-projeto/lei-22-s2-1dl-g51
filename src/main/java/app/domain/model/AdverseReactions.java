package app.domain.model;

import app.domain.model.dto.AdverseReactionsDTO;

import java.io.Serializable;

/**
 * The type Adverse reactions.
 */
public class AdverseReactions implements Serializable {

    private String snsUserNumber;
    private String adverseReactions;
    private AdverseReactionsDTO adverseReactionsDTO;

    /**
     * Instantiates a new Adverse reactions.
     *
     * @param snsUserNumber    the sns user number
     * @param adverseReactions the adverse reactions
     */
    public AdverseReactions(String snsUserNumber, String adverseReactions) {
        this.snsUserNumber = snsUserNumber;
        this.adverseReactions = adverseReactions;
        this.adverseReactionsDTO = new AdverseReactionsDTO(snsUserNumber,adverseReactions);
    }

    /**
     * Gets adverse reactions dto.
     *
     * @return the adverse reactions dto
     */
    public AdverseReactionsDTO getAdverseReactionsDTO() {
        return adverseReactionsDTO;
    }

    /**
     * Sets sns user number.
     *
     * @param snsUserNumber the sns user number
     */
    public void setSnsUserNumber(String snsUserNumber) {
        this.snsUserNumber = snsUserNumber;
    }

    /**
     * Sets adverse reactions.
     *
     * @param adverseReactions the adverse reactions
     */
    public void setAdverseReactions(String adverseReactions) {
        this.adverseReactions = adverseReactions;
        this.adverseReactionsDTO.setAdverseReactions(adverseReactions);
    }
}
