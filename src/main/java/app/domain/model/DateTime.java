package app.domain.model;

import java.io.Serializable;

/**
 * The type Date time.
 */
public class DateTime implements Serializable {
    private int day;
    private int month;
    private int year;
    private int hour;
    private int minute;

    /**
     * Instantiates a new Date time.
     *
     * @param day    the day
     * @param month  the month
     * @param year   the year
     * @param hour   the hour
     * @param minute the minute
     */
    public DateTime(int day, int month, int year, int hour, int minute) {
        this.day = day;
        this.month = month;
        this.year = year;
        this.hour = hour;
        this.minute = minute;
    }

    /**
     * Gets day.
     *
     * @return the day
     */
    public int getDay() {
        return day;
    }

    /**
     * Gets month.
     *
     * @return the month
     */
    public int getMonth() {
        return month;
    }

    /**
     * Gets year.
     *
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * Gets hour.
     *
     * @return the hour
     */
    public int getHour() {
        return hour;
    }

    /**
     * Gets minute.
     *
     * @return the minute
     */
    public int getMinute() {
        return minute;
    }
}
