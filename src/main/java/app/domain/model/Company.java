package app.domain.model;

import app.domain.model.store.*;
import app.domain.shared.Constants;
import org.apache.commons.lang3.StringUtils;
import pt.isep.lei.esoft.auth.AuthFacade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Company.
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Company implements Serializable {
    private String designation;
    private transient AuthFacade authFacade;
    private SNSUserStore snsUserStore = new SNSUserStore();
    private VaccineTypeStore vaccineTypeStore = new VaccineTypeStore();
    private EmployeeStore employeeStore = new EmployeeStore();
    private VaccinationScheduleStore vaccinationScheduleStore = new VaccinationScheduleStore();
    private MassVacinationCenterStore mvcStore = new MassVacinationCenterStore();
    private HealthCenterStore hcStore = new HealthCenterStore();
    private VaccinationCenterStore vcStore = new VaccinationCenterStore();
    private ArrivalTimeStore arrivalTimeStore = new ArrivalTimeStore();
    private VaccineAdministrationStore vaStore = new VaccineAdministrationStore();
    private AdverseReactionsStore adverseReactionsStore = new AdverseReactionsStore();
    private VaccinationStatisticsStore vaccinationStatisticsStore = new VaccinationStatisticsStore();

    private PerformanceAnalysisStore performanceAnalysisStore = new PerformanceAnalysisStore();


    /**
     * Gets vaccination statistics store.
     *
     * @return the vaccination statistics store
     */
    public VaccinationStatisticsStore getVaccinationStatisticsStore() {
        return vaccinationStatisticsStore;
    }

    /**
     * Instantiates a new Company.
     *
     * @param designation the designation
     */
    public Company(String designation) {
        if (StringUtils.isBlank(designation))
            throw new IllegalArgumentException("Designation cannot be blank.");
        this.designation = designation;
        this.authFacade = new AuthFacade();
    }

    /**
     * Gets designation.
     *
     * @return Company designation
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * Gets auth facade.
     *
     * @return the auth facade
     */
    public AuthFacade getAuthFacade() {
        return authFacade;
    }

    /**
     * Sets auth facade.
     *
     * @param authFacade the auth facade
     */
    public void setAuthFacade(AuthFacade authFacade) {
        this.authFacade = authFacade;
    }

    /**
     * method to get a user store
     *
     * @return userStore user store
     */
    public SNSUserStore getUserStore() {
        return snsUserStore;
    }

    /**
     * method to get a vaccination center store
     *
     * @return vaccination center store
     */
    public VaccinationCenterStore getVaccinationCenterStore() {
        return vcStore;
    }

    /**
     * method to get vaccine type store
     *
     * @return vaccine type store
     */
    public VaccineTypeStore getVaccineTypeStore() {
        return vaccineTypeStore;
    }

    /**
     * method to get employee store
     *
     * @return employee store
     */
    public EmployeeStore getEmployeeStore() {
        return employeeStore;
    }

    /**
     * method to get vaccination schedule store
     *
     * @return vacination schedule
     */
    public VaccinationScheduleStore getVaccinationScheduleStore() {
        return vaccinationScheduleStore;
    }

    /**
     * method to get vaccination center store
     *
     * @return mass vacination center store
     */
    public MassVacinationCenterStore getMvcStore() {
        return mvcStore;
    }

    /**
     * method to get health center store
     *
     * @return hc store
     */
    public HealthCenterStore getHcStore() {
        return hcStore;
    }

    /**
     * method to get vacination center store
     *
     * @return vacination center store
     */
    public VaccinationCenterStore getVcStore() {
        return vcStore;
    }

    /**
     * Gets arrival time store.
     *
     * @return the arrival time store
     */
    public ArrivalTimeStore getArrivalTimeStore() {
        return arrivalTimeStore;
    }

    /**
     * Gets sns user store.
     *
     * @return the sns user store
     */
    public SNSUserStore getSnsUserStore() {
        return snsUserStore;
    }

    /**
     * Gets vaccine administration store.
     *
     * @return the vaccine administration store
     */
    public VaccineAdministrationStore getVaccineAdministrationStore() {
        return vaStore;
    }

    /**
     * Gets adverse reactions store.
     *
     * @return the adverse reactions store
     */
    public AdverseReactionsStore getAdverseReactionsStore() {
        return adverseReactionsStore;
    }

    /**
     * creates list of employee's roles
     *
     * @return list of roles
     */
    public static List<String> getUserRole() {
        List<String> rolesList = new ArrayList<>();

        rolesList.add(Constants.ROLE_NURSE);
        rolesList.add(Constants.ROLE_RECEPTIONIST);
        rolesList.add(Constants.ROLE_CENTER_COORDINATOR);

        return rolesList;
    }

    /**
     * Gets performance analysis store.
     *
     * @return the performance analysis store
     */
    public PerformanceAnalysisStore getPerformanceAnalysisStore() {
        return performanceAnalysisStore;
    }
}

