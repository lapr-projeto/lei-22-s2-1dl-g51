package app.domain.model;

import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.domain.model.dto.VaccinationScheduleDTO;
import app.ui.console.utils.Utils;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * The type Vaccination schedule.
 */
public class VaccinationSchedule implements Serializable {
    private String snsUserNumber;
    private VaccinationCenter vaccinationCenter;
    private Date date;
    private String time;
    private VaccineType vaccineType;
    private VaccinationScheduleDTO scheduleDTO;

    /**
     * Instantiates a new Vaccination schedule.
     *
     * @param snsUserNumber     the sns user number
     * @param vaccinationCenter the vaccination center
     * @param date              the date
     * @param time              the time
     * @param vaccineType       the vaccine type
     */
    public VaccinationSchedule(String snsUserNumber, VaccinationCenter vaccinationCenter, Date date, String time, VaccineType vaccineType) {
        this.snsUserNumber = snsUserNumber;
        this.vaccinationCenter = vaccinationCenter;
        this.date = date;
        this.time = time;
        this.vaccineType = vaccineType;
        this.scheduleDTO = new VaccinationScheduleDTO(snsUserNumber,vaccinationCenter,date,time,vaccineType);
    }

    /**
     * Gets schedule dto.
     *
     * @return the schedule dto
     */
    public VaccinationScheduleDTO getScheduleDTO() {
        return scheduleDTO;
    }

    /**
     * Sets sns user number.
     *
     * @param snsUserNumber the sns user number
     */
    public void setSnsUserNumber(String snsUserNumber) {
        this.snsUserNumber = snsUserNumber;
    }

    /**
     * Sets vaccination center.
     *
     * @param vaccinationCenter the vaccination center
     */
    public void setVaccinationCenter(VaccinationCenter vaccinationCenter) {
        this.vaccinationCenter = vaccinationCenter;
    }

    /**
     * Sets date.
     *
     * @param date the date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Sets time.
     *
     * @param time the time
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * Sets vaccine type.
     *
     * @param vaccineType the vaccine type
     */
    public void setVaccineType(VaccineType vaccineType) {
        this.vaccineType = vaccineType;
    }

    @Override
    public String toString() {
        return String.format("===========VACCINATION SCHEDULE==========\nSNS User Number: %s\nVaccination Center %s\nVaccine Type %s\nDate: %s\nTime: %s\n=========================================",snsUserNumber,
                vaccinationCenter.toSimpleString(),vaccineType.toSimpleString(), Utils.convertToSimpleDataFormat(date),time);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VaccinationSchedule schedule = (VaccinationSchedule) o;
        return Objects.equals(snsUserNumber, schedule.snsUserNumber) && Objects.equals(vaccineType, schedule.vaccineType);
    }

}
