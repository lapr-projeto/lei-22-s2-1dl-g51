package app.domain.model;

import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.domain.model.dto.ArrivalTimeDTO;
import app.ui.console.utils.Utils;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * The type Arrival time.
 */
public class ArrivalTime implements Serializable {
    private String snsNum;
    private Date arrivalTime;
    private VaccinationCenter vc;
    private ArrivalTimeDTO arrivalTimeDTO;
    private Date scheduleDate;

    private String scheduleHour;

    /**
     * Instantiates a new Arrival time.
     *
     * @param snsNum       the sns num
     * @param vc           the vc
     * @param scheduleDate the schedule date
     * @param scheduleHour the schedule hour
     */
    public ArrivalTime(String snsNum, VaccinationCenter vc, Date scheduleDate, String scheduleHour) {
        this.snsNum = snsNum;
        this.arrivalTime = new Date();
        this.scheduleDate = scheduleDate;
        this.vc = vc;
        this.scheduleHour = scheduleHour;

        this.arrivalTimeDTO=new ArrivalTimeDTO(snsNum,vc, scheduleDate,scheduleHour);

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ArrivalTime that = (ArrivalTime) o;
        return Objects.equals(snsNum, that.snsNum) && Objects.equals(arrivalTime, that.arrivalTime) && Objects.equals(vc, that.vc) && Objects.equals(scheduleDate, that.scheduleDate) && Objects.equals(scheduleHour, that.scheduleHour);
    }


    /**
     * Gets arrival time dto.
     *
     * @return the arrival time dto
     */
    public ArrivalTimeDTO getArrivalTimeDTO() {
        return arrivalTimeDTO;
    }

    @Override
    public String toString() {
        return String.format("\nSNS User Number: %s\nVaccination Center: %s\nVaccination Schedule for %s at %s\nArrived at %s of %s\n",snsNum,vc.getVaccinationCenterDTO().getName(), Utils.convertToSimpleDataFormat(scheduleDate),scheduleHour,Utils.convertFromDateToHours(arrivalTime),Utils.convertToSimpleDataFormat(arrivalTime));
    }
}


