package app.domain.model.store;
import app.controller.App;
import app.domain.model.VaccinationCenter.HealthCenter;
import app.domain.model.VaccinationCenter.MassVaccinationCenter;
import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.domain.model.VaccineType;
import app.domain.model.VaccineTypes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



public class VaccinationCenterStore implements Serializable {

    protected List<VaccinationCenter> vaccinationCentersList = new ArrayList<>();

    public VaccinationCenterStore(){
    }

    /**
     * Creates a vacination center
     * @param name
     * @param address
     * @param phoneNumber
     * @param email
     * @param openHours
     * @param closingHours
     * @param vaccinesSlotCap
     * @param slotDuration
     * @return new Vaccination Center
     */
    public VaccinationCenter createVaccinationCenter(String name, String address, String phoneNumber, String email, String openHours, String closingHours, String vaccinesSlotCap, String slotDuration) {
        return new VaccinationCenter(name, address, phoneNumber, email, openHours, closingHours, vaccinesSlotCap, slotDuration);
    }

    /**
     * Verifies if the vaccination center isn't null nor absent in Vaccination center list
     * @param vc
     * @return boolean
     */

    public boolean validateVaccinationCenter(VaccinationCenter vc){
        if (vc == null)
            return false;
        return ! this.vaccinationCentersList.contains(vc);
    }

    /**
     * Verifies if vaccination center is valid
     *Verifies if vaccination center is added in vaccination center list
     *
     * @param vc
     * @return boolean
     */

    public boolean saveVaccinationCenter (VaccinationCenter vc) {
        if (!validateVaccinationCenter(vc))
            return false;
        return this.vaccinationCentersList.add(vc);

    }

    /**
     * method to get vacination center list
     *
     * @return vacination center list
     */

    public List<VaccinationCenter> getVaccinationCentersList() {
        return vaccinationCentersList;
    }

    public List<VaccinationCenter> getVaccinationCentersWithGivenVaccineType(VaccineType vaccineType) {
        List<VaccinationCenter> vaccinationCenters = new ArrayList<>(vaccinationCentersList);

        for (VaccinationCenter vc : vaccinationCentersList) {
            if (vc.getClass().getSimpleName().equalsIgnoreCase("HealthCenter")) {
                HealthCenter hc = (HealthCenter) vc;
                boolean vaccineTypeAvailable = false;
                for (VaccineType vt: hc.getHealthCenterDTO().getVaccineTypes()) {
                    if(vt.getVaccineTypeDTO().getType().equals(vaccineType.getVaccineTypeDTO().getType()))
                        vaccineTypeAvailable = true;
                }
                if(!vaccineTypeAvailable){
                    vaccinationCenters.remove(vc);
                }
            } else if (vc.getClass().getSimpleName().equalsIgnoreCase("MassVaccinationCenter")) {
                MassVaccinationCenter mvc = (MassVaccinationCenter) vc;
                if(!mvc.getMassVaccinationCenterDTO().getVaccineType().getVaccineTypeDTO().getType().name().equalsIgnoreCase(vaccineType.getVaccineTypeDTO().getType().name()))
                    vaccinationCenters.remove(vc);
            }
        }
        return vaccinationCenters;
    }


    //apenas para testes
    public void addToArrayList(){
        VaccineType vc1 = new VaccineType(VaccineTypes.Covid_19,"3213","Pfizer","321313");
        VaccineType vc2 = new VaccineType(VaccineTypes.Covid_19,"3213","Moderna","321313");
        VaccineType vc3 = new VaccineType(VaccineTypes.Covid_19,"3213","Astra","321313");

        ArrayList<VaccineType> vaccineTypesList = new ArrayList<>();
        vaccineTypesList.add(vc1);
        vaccineTypesList.add(vc2);
        vaccineTypesList.add(vc3);
        vaccineTypesList.add(new VaccineType(VaccineTypes.Flu,"12312","Pfizer","123312"));

        ArrayList<VaccineType> vaccineTypesList2 = new ArrayList<>();
        vaccineTypesList2.add(vc3);
        vaccineTypesList2.add(new VaccineType(VaccineTypes.Hepatitis_A,"3213","Pfizer","321313"));
        vaccineTypesList2.add(new VaccineType(VaccineTypes.Chickenpox,"3213","Pfizer","321313"));
        vaccinationCentersList.add(new HealthCenter("Centro de Saúde de Alfandega da Fé", "Rua Eng. Manuel Cunha, 5350-009 Alfândega da Fé", "283451692",
                "covidFe@dgs.pt", "09:00", "13:00", "1", "25",vaccineTypesList));
        vaccinationCentersList.add(new HealthCenter("Centro de Saúde Chaves II", "Rua Fonte do Leite, 5400-261 Chaves", "274956128", "covidChaves@dgs.pt", "09:00", "12:30", "5", "5",vaccineTypesList2));
        vaccinationCentersList.add(new MassVaccinationCenter("CS Rebordosa", "Rua Cooperativa A CELER, 4585-846 Rebordosa", "245913278", "covidRebor@dgs.pt", "08:30", "19:00", "5", "5",new VaccineType(VaccineTypes.Hepatitis_A,"12312","Pfizer","123312")));
        vaccinationCentersList.add(new MassVaccinationCenter("CS Rebordosa2", "Rua Cooperativa A CELER, 4585-846 Rebordosa", "245913278", "covidRebor@dgs.pt", "00:00", "22:00", "5", "60",vc2));

    }
}
