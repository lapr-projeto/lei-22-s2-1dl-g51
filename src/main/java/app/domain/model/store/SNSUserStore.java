package app.domain.model.store;

import app.controller.App;
import app.controller.LoadCSVFileController;
import app.controller.PasswordGenerator;
import app.domain.model.SNSUser;
import app.domain.shared.Constants;
import app.ui.console.utils.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.Scanner;


/**
 * Represents a store for the SNSUser data
 */
public class SNSUserStore implements Serializable {

    private ArrayList<SNSUser> snsUserList = new ArrayList<>();

    /**
     * Creates a new store for the SNSUser data
     */
    public SNSUserStore() {
    }

    public ArrayList<SNSUser> getSnsUserList() {
        return snsUserList;
    }

    /**
     * Validates if the SNSUser object, and it's attributes are null
     *
     * @param user
     * @return false if, the SNSUser object or any of his attributes are null, and true otherwise
     */
    public boolean validateSNSUser(SNSUser user) {
        if (user.getName().length() == 0 || user.getAddress().length() == 0 || user.getPhoneNumber().length() == 0 || user.getEmail().length() == 0 || user.getCitizenCardNumber().length() == 0 || Utils.convertToSimpleDataFormat(user.getBirthDate()).length() == 0 || user.getSnsUserNumber().length() == 0)
            return false;
        else if (Objects.isNull(user) || user.getName() == null || user.getAddress() == null || user.getPhoneNumber() == null || user.getEmail() == null || user.getCitizenCardNumber() == null || user.getBirthDate() == null || user.getSnsUserNumber() == null || user.getGender() == null || !checkDuplicates(user)) {
            return false;
        }
        return true;
    }

    /**
     * Validates if the SNSUser's email is duplicated in the System
     *
     * @param user
     * @return false if, the email already exists in the System, and true otherwise.
     */
    public boolean checkDuplicates(SNSUser user) {
        String email = user.getEmail();
        boolean duplicatedUser = App.getInstance().getCompany().getAuthFacade().existsUser(email);
        if (duplicatedUser) {
            System.out.println("Duplicated User!");
            return false;
        }
        return true;
    }

    /**
     * Generates a random Password with a given length, with 3 capital letters and 2 numbers.
     *
     * @return a new password
     */
    public String generatePassword() {
        PasswordGenerator pwdGenerator = new PasswordGenerator(7);
        return pwdGenerator.passwordGenerator();
    }

    /**
     * Adds the SNSUser and gives him a login to enter the System.
     *
     * @param user
     * @return true if, there is no duplicated users and if the user has been added to the System successfully, and false otherwise
     */
    public boolean addSNSUser(SNSUser user) {
        String email = user.getEmail();
        String name = user.getName();
        String pwd = generatePassword();
        boolean addUser = App.getInstance().getCompany().getAuthFacade().addUserWithRole("SNS User " + name, email, pwd, Constants.ROLE_SNS_USER);
        if (addUser) {
            //App.getInstance().getCompany().getUserSerializables().add(new UsersLogins(name, email, pwd, Constants.ROLE_SNS_USER));
            snsUserList.add(user);
            System.out.printf("\nEmail: %s\nPassword: %s", email, pwd);
            return true;
        }
        return false;
    }

    /**
     * Check's if this snsUserNumber has been already registered in the system
     *
     * @param snsUserNumber snsUserNumber
     * @return true, if the user is already registered in the system, or false otherwise
     */
    public boolean checkUserRegistration(String snsUserNumber) {
        for (SNSUser snsUser : snsUserList) {
            if (snsUser.getSNSUserDTO().getSnsUserNumber().equalsIgnoreCase(snsUserNumber)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets the SNS User with the given SNSUserNumber
     *
     * @param snsUserNumber SNS User's snsUserNumber
     * @return the snsUser, if the SNSUserNumber is already registered in the system.
     */
    public SNSUser getSNSUserWithAGivenSNSUserNumber(String snsUserNumber) {
        for (SNSUser snsUser : snsUserList) {
            if (snsUser.getSNSUserDTO().getSnsUserNumber().equalsIgnoreCase(snsUserNumber)) {
                return snsUser;
            }
        }
        return null;
    }

    //apenas para testes
    public void addToArrayList() {
        this.snsUserList.add(new SNSUser("Nikolas Quintão Quirino", "Rua de Choupelo Santa Marinha 250 4404-509 Vila Nova de Gaia", "926123570", "nikolas@hotmail.com", "93847625", "Male", new Date(97, 3, 23), "160478539"));
        this.snsUserList.add(new SNSUser("Saúl Junqueira Zarco", "Rua La Couture 4200-361 Porto", "937190826", "saul@hotmail.com", "09276481", "Female", new Date(97, 3, 24), "173056892"));
        this.snsUserList.add(new SNSUser("Eliel Bandeira Fazendeiro", "Rua Grupo 10 de Maio 4200-315 Porto", "935319204", "eliel@gmail.com", "79812534", "Female", new Date(97, 3, 25), "123456789"));
        this.snsUserList.add(new SNSUser("Reginald Bryan", "Brick Lane. London", "913156567", "reginald@gmail.com", "32323421", "Male", new Date(97, 3, 26), "999888777"));
        this.snsUserList.add(new SNSUser("Ze Silva", "Rua de Choupelo Santa Marinha 250 4404-509 Vila Nova de Gaia", "926123570", "nikolas@hotmail.com", "93847625", "Male", new Date(97, 3, 23), "111111111"));
        this.snsUserList.add(new SNSUser("Antonio Pereira", "Rua La Couture 4200-361 Porto", "937190826", "saul@hotmail.com", "09276481", "Female", new Date(97, 3, 24), "222222222"));
        this.snsUserList.add(new SNSUser("Ana Barbosa", "Rua Grupo 10 de Maio 4200-315 Porto", "935319204", "eliel@gmail.com", "79812534", "Female", new Date(97, 3, 25), "333333333"));
        this.snsUserList.add(new SNSUser("Maria Antónia", "Brick Lane. London", "913156567", "reginald@gmail.com", "32323421", "Male", new Date(97, 3, 26), "444444444"));
    }


    public static ArrayList<SNSUser> getUserFromFile(File file) {
        ArrayList<SNSUser> snsUserlist = new ArrayList<>();
        Scanner readFile = null;
        String splitLine = "";
        if (LoadCSVFileController.verifyIfFileHasHeader(file)) {
            try {
                readFile = new Scanner(file,"UTF-8");
                readFile.nextLine();
                splitLine = ";";
            } catch (FileNotFoundException e) {
            }
        } else {
            try {
                readFile = new Scanner(file,"UTF-8");
                splitLine = ",";
            } catch (FileNotFoundException e) {
            }
        }
        while (readFile.hasNextLine()) {
            String[] snsUserInfo = readFile.nextLine().split(splitLine);

            //SprintD
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            //SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

            Date data = null;
            try {
                data = df.parse(snsUserInfo[2]);
            } catch (ParseException e) {
            }

            try {
                if(snsUserInfo[1].length() == 0)
                    snsUserInfo[1] = "NA";
                //SNSUser user = new SNSUser(snsUserInfo[0], snsUserInfo[1], snsUserInfo[2], snsUserInfo[3], snsUserInfo[4], snsUserInfo[5], data, snsUserInfo[7]);
                SNSUser user = new SNSUser(snsUserInfo[0], snsUserInfo[3], snsUserInfo[4], snsUserInfo[5], snsUserInfo[7], snsUserInfo[1], data, snsUserInfo[6]);
                snsUserlist.add(user);
            } catch (ArrayIndexOutOfBoundsException ex) {
            }

        }
        return snsUserlist;
    }
}


