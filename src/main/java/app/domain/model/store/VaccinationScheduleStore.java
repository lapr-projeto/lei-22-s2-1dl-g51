package app.domain.model.store;

import app.controller.App;
import app.controller.SMSNotificationSender;
import app.domain.model.*;
import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.domain.model.dto.VaccinationCenterDTO;
import app.domain.model.dto.VaccinationScheduleDTO;
import app.ui.console.utils.Hour;
import app.ui.console.utils.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.*;

/**
 * Represents a store for the VaccinationSchedule data
 */
public class VaccinationScheduleStore extends ScheduleAndAdminstrationStore implements Serializable, SMSNotificationSender {

    private ArrayList<VaccinationSchedule> vaccinationScheduleList = new ArrayList<>();

    /**
     * Represents a new store for the VaccinationSchedule data
     */
    public VaccinationScheduleStore() {
    }

    public ArrayList<VaccinationSchedule> getVaccinationScheduleList() {
        return vaccinationScheduleList;
    }

    /**
     * Validates if the VaccinationSchedule object and it's attributes are null
     *
     * @param vaccinationSchedule vaccinationSchedule
     * @return false if, the VaccinationSchedule object or any of his attributes are null and if the vaccineType/VaccinationCenter is not registered in the system, and true otherwise
     */
    public boolean validateVaccineSchedule(VaccinationSchedule vaccinationSchedule) {
        Company company = App.getInstance().getCompany();
        VaccinationScheduleDTO vs = vaccinationSchedule.getScheduleDTO();
        VaccineType vaccineType = vs.getVaccineType();
        VaccinationCenter vaccinationCenter = vs.getVaccinationCenter();
        VaccineTypeStore vaccineTypeStore = company.getVaccineTypeStore();
        VaccinationCenterStore vaccinationCenterStore = company.getVaccinationCenterStore();

        if (Objects.isNull(vaccinationSchedule) || vs.getSnsUserNumber() == null || vs.getVaccinationCenter() == null || vs.getDate() == null || vs.getTime() == null
                || vs.getVaccineType() == null || !vaccineTypeStore.getVaccineTypeList().contains(vaccineType) || !vaccinationCenterStore.getVaccinationCentersList().contains(vaccinationCenter)
                || checkDuplicates(vaccinationSchedule)) {
            return false;
        }
        return true;
    }

    /**
     * Validates if there's a duplicate vaccination schedule
     *
     * @param vs vaccinationSchedule
     * @return false if, the vaccination schedule already exists in the System, and true otherwise.
     */
    public boolean checkDuplicates(VaccinationSchedule vs) {
        if(vaccinationScheduleList.contains(vs)) {
            System.out.println("There is a duplicated schedule for this SNS User Number");
            return true;
        }
        return false;
    }

    /**
     * Adds the vaccination schedule on the system
     *
     * @param vs vaccinationSchedule
     * @return true if, there is no duplicated vaccination schedules (with same SNSUserNumber and VaccineType), and false otherwise
     */
    public boolean addVaccinationSchedule(VaccinationSchedule vs) {
        if (validateVaccineSchedule(vs)) {
            if (!checkDuplicates(vs)) {
                vaccinationScheduleList.add(vs);
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * Checks the number of slots left for schedule a vaccination
     *
     * @param hours hour of the schedule
     * @param date  date of the schedule
     * @param vc    vaccination center data
     * @return number of slots left for the given hour, date and vaccination center
     */
    public int counterSchedulesPerSlot(String hours, String date, VaccinationCenterDTO vc) {
        int vaccinesPerSlot = Integer.parseInt(vc.getVaccinesSlotCap());
        for (VaccinationSchedule vs : vaccinationScheduleList) {
            if (vs.getScheduleDTO().getTime().equals(hours) && Utils.convertToSimpleDataFormat(vs.getScheduleDTO().getDate()).equalsIgnoreCase(date)) {
                vaccinesPerSlot -= 1;
            }
        }
        return vaccinesPerSlot;
    }

    /**
     * Checks the available hours to schedule a vaccine
     *
     * @param vc   vaccination center data
     * @param date date of the schedule
     * @return a list with the available hours to schedule a vaccine
     */
    public List<String> checkAvailableHours(VaccinationCenterDTO vc, String date) {
        List<String> hourList = new ArrayList<>();
        Hour openHour = new Hour(vc.getOpenHours());
        Hour closingHour = new Hour(vc.getClosingHours());

        for (Hour hours = openHour; Hour.isAfter(hours.toString(), closingHour.toString()); hours.advanceMinutes(Integer.parseInt(vc.getSlotDuration()))) {
            int slotsLeft = counterSchedulesPerSlot(hours.toString(), date, vc);
            boolean hoursAvailable = checkAvailableHoursToday(hours, date);
            if (slotsLeft > 0 && hoursAvailable)
                hourList.add(hours.toString());
        }

        if (hourList.isEmpty()) {
            System.out.println(vc.getName() + " is closed at the moment! Reopens tomorrow at " + vc.getOpenHours());
        }

        return hourList;
    }

    /**
     * Checks if there are available hours to schedule a vaccine today
     *
     * @param openHour vaccination center opening hour
     * @param date     today's date
     * @return true, if there is available hours for the given date, and false otherwise
     */
    public boolean checkAvailableHoursToday(Hour openHour, String date) {
        Calendar calendar = GregorianCalendar.getInstance();

        Hour currentHour = new Hour(String.format("%02d:%02d", calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE)));
        if (Hour.isAfter(openHour.toString(), currentHour.toString()) && date.equalsIgnoreCase(Utils.convertToSimpleDataFormat(new Date()))) {
            return false;
        }
        return true;
    }


    /**
     * For tests only
     */
    public void addToArrayList(VaccinationCenter vs, VaccineType vt) {
        this.vaccinationScheduleList.add(new VaccinationSchedule("160478539", vs, new Date(), "15:00", vt));
        this.vaccinationScheduleList.add(new VaccinationSchedule("173056892", vs, new Date(), "15:00", new VaccineType(VaccineTypes.Hepatitis_A,"3131","Pfizer","31231")));
        this.vaccinationScheduleList.add(new VaccinationSchedule("123456789", vs, new Date(), "15:00", vt));
        this.vaccinationScheduleList.add(new VaccinationSchedule("999888777", vs, new Date(), "15:00", vt));
        this.vaccinationScheduleList.add(new VaccinationSchedule("111111111", App.getInstance().getCompany().getVaccinationCenterStore().vaccinationCentersList.get(1), new Date(), "15:00", vt));
        this.vaccinationScheduleList.add(new VaccinationSchedule("222222222", App.getInstance().getCompany().getVaccinationCenterStore().vaccinationCentersList.get(2), new Date(), "15:00", vt));
        this.vaccinationScheduleList.add(new VaccinationSchedule("333333333", App.getInstance().getCompany().getVaccinationCenterStore().vaccinationCentersList.get(2), new Date(), "15:00", vt));
        this.vaccinationScheduleList.add(new VaccinationSchedule("444444444", App.getInstance().getCompany().getVaccinationCenterStore().vaccinationCentersList.get(0), new Date(), "15:00", vt));
        this.vaccinationScheduleList.add(new VaccinationSchedule("555555555", App.getInstance().getCompany().getVaccinationCenterStore().getVaccinationCentersList().get(3), new Date(), "15:00", App.getInstance().getCompany().getVaccineTypeStore().getVaccineTypeList().get(0)));
        this.vaccinationScheduleList.add(new VaccinationSchedule("666666666", App.getInstance().getCompany().getVaccinationCenterStore().getVaccinationCentersList().get(3), new Date(), "15:00", App.getInstance().getCompany().getVaccineTypeStore().getVaccineTypeList().get(2)));
        this.vaccinationScheduleList.add(new VaccinationSchedule("777777777", App.getInstance().getCompany().getVaccinationCenterStore().getVaccinationCentersList().get(0), new Date(), "15:00", App.getInstance().getCompany().getVaccineTypeStore().getVaccineTypeList().get(1)));
        this.vaccinationScheduleList.add(new VaccinationSchedule("999999999", App.getInstance().getCompany().getVaccinationCenterStore().getVaccinationCentersList().get(0), new Date(), "15:00", App.getInstance().getCompany().getVaccineTypeStore().getVaccineTypeList().get(1)));
        this.vaccinationScheduleList.add(new VaccinationSchedule("111111119", App.getInstance().getCompany().getVaccinationCenterStore().vaccinationCentersList.get(1), new Date(), "15:00", vt));
        this.vaccinationScheduleList.add(new VaccinationSchedule("222222228", App.getInstance().getCompany().getVaccinationCenterStore().vaccinationCentersList.get(2), new Date(), "15:00", vt));
        this.vaccinationScheduleList.add(new VaccinationSchedule("333333337", App.getInstance().getCompany().getVaccinationCenterStore().vaccinationCentersList.get(2), new Date(), "15:00", vt));
        this.vaccinationScheduleList.add(new VaccinationSchedule("444444446", App.getInstance().getCompany().getVaccinationCenterStore().vaccinationCentersList.get(0), new Date(), "15:00", vt));
        this.vaccinationScheduleList.add(new VaccinationSchedule("555555554", App.getInstance().getCompany().getVaccinationCenterStore().getVaccinationCentersList().get(3), new Date(), "15:00", App.getInstance().getCompany().getVaccineTypeStore().getVaccineTypeList().get(0)));
        this.vaccinationScheduleList.add(new VaccinationSchedule("666666665", App.getInstance().getCompany().getVaccinationCenterStore().getVaccinationCentersList().get(3), new Date(), "15:00", App.getInstance().getCompany().getVaccineTypeStore().getVaccineTypeList().get(2)));
        this.vaccinationScheduleList.add(new VaccinationSchedule("777777773", App.getInstance().getCompany().getVaccinationCenterStore().getVaccinationCentersList().get(0), new Date(), "15:00", App.getInstance().getCompany().getVaccineTypeStore().getVaccineTypeList().get(1)));
        this.vaccinationScheduleList.add(new VaccinationSchedule("999999992", App.getInstance().getCompany().getVaccinationCenterStore().getVaccinationCentersList().get(0), new Date(), "15:00", App.getInstance().getCompany().getVaccineTypeStore().getVaccineTypeList().get(1)));

    }

    public String getVaccinationCenterBySNSNumber(String snsNumber){
        for (VaccinationSchedule va: vaccinationScheduleList) {
            if(snsNumber.equalsIgnoreCase(va.getScheduleDTO().getSnsUserNumber()))
                return va.getScheduleDTO().getVaccinationCenter().getVaccinationCenterDTO().getName();
        }
        return null;
    }

    @Override
    public void sendSMS(String snsNumber) {
        String userNumber = snsNumber;
        SNSUser snsUser = App.getInstance().getCompany().getUserStore().getSNSUserWithAGivenSNSUserNumber(userNumber);
        String phoneNumber = snsUser.getSNSUserDTO().getPhoneNumber();
        PrintWriter printWriter = null;

        try {
            printWriter = new PrintWriter(new File("ConfirmationSMSto" + phoneNumber + ".txt"));
            printWriter.print(("Please, go to the Recovery room!\nYou will receive a SMS to inform when you can leave the center!\n"));
            System.out.println("Confirmation SMS has been sent successfully!");
        } catch (FileNotFoundException ex) {
            System.out.println("File Not Found");
        } finally {
            printWriter.close();
        }
    }
}
