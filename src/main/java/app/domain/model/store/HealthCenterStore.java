package app.domain.model.store;

import app.domain.model.VaccinationCenter.HealthCenter;

import java.io.Serializable;

/**
 * The type Health center store.
 */
public class HealthCenterStore extends VaccinationCenterStore implements Serializable {

    /**
     * Instantiates a new Health center store.
     */
    public HealthCenterStore(){
    }


    /**
     * Verifies if the health center isn't null nor absent in health center list
     *
     * @param hc the hc
     * @return boolean boolean
     */
    public boolean validateHC(HealthCenter hc){
        if (hc == null)
            return false;
        return ! vaccinationCentersList.contains(hc);
    }

    /**
     * Verifies if vaccination center is valid
     * Verifies if vaccination center is added in vaccination center list
     *
     * @param hc the hc
     * @return boolean
     */
    public boolean saveHC (HealthCenter hc){
        if (!validateHC(hc))
            return false;
        return vaccinationCentersList.add(hc);
    }
}
