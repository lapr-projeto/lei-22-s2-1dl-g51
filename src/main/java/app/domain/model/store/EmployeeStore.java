package app.domain.model.store;


import app.controller.App;
import app.controller.PasswordGenerator;
import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.domain.model.employees.CenterCoordinator;
import app.domain.model.employees.Employee;
import app.domain.model.employees.Nurse;
import app.domain.model.employees.Receptionist;

import javax.management.InstanceNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Represents a store for the Employee data
 */
public class EmployeeStore implements Serializable {
    private List<Employee> employeeList = new ArrayList<>();

    /**
     * Creats a new store for the Employee data and assignes a company to it.
     */
    public EmployeeStore() {
    }


    /**
     * Validates if the Employee object and it's atributes are null
     *
     * @param employee the employee
     * @return false if the Employee object or any of its atributes are null, otherwise returns true
     */
    public boolean validateEmployeeData(Employee employee) {
        if (Objects.isNull(employee) || employee.getName() == null || employee.getAddress() == null || employee.getPhoneNumber() == null || employee.getEmail() == null || employee.getCitizenCardNumber() == null || employee.getRoleID() == null) {
            return false;
        }
        return true;
    }

    /**
     * Validates if the Employee's email is duplicated in the System
     *
     * @param employee the employee
     * @return false if the email already exists in the System, otherwise it returns true
     */
    public boolean checkDuplicates(Employee employee) {
        String email = employee.getEmail();
        boolean duplicatedUser = App.getInstance().getCompany().getAuthFacade().existsUser(email);
        if (duplicatedUser) {
            System.out.println("Duplicated User!");
            return false;
        }
        return true;
    }

    /**
     * Generates a random Password with a given lenght, with 3 capital letters and 2 numbers.
     *
     * @return a new password
     */
    public String generatePassword() {
        PasswordGenerator pwdGenerator = new PasswordGenerator(7);
        return pwdGenerator.passwordGenerator();
    }

    /**
     * Adds the Employee and gives him a login to enter the System.
     *
     * @param employee the employee
     * @return true if there is no duplicated users and if the user has been added to the System successfully, otherwise it returns false.
     */
    public boolean addEmployee(Employee employee) {
        if (checkDuplicates(employee)) {
            String email = employee.getEmail();
            String name = employee.getName();
            String roleID = employee.getRoleID();
            String pwd = generatePassword();
            if (App.getInstance().getCompany().getAuthFacade().addUserWithRole(this.getClass().getSimpleName() + name, email, pwd, roleID)) {
                employeeList.add(employee);
                //App.getInstance().getCompany().getUserSerializables().add(new UsersLogins(name,pwd,email,roleID));
                System.out.printf("\nEmail: %s\nPassword: %s", email, pwd);
                return true;
            }
        }
        return false;
    }

    /**
     * Gets employee list.
     *
     * @return the employee list
     */
    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    /**
     * Gets center coordinator by email.
     *
     * @param email the email
     * @return the center coordinator by email
     * @throws InstanceNotFoundException the instance not found exception
     */
    public CenterCoordinator getCenterCoordinatorByEmail(String email) throws InstanceNotFoundException {
        for (Employee cc: employeeList) {
            if(cc.getClass().getSimpleName().equalsIgnoreCase("CenterCoordinator") && email.equalsIgnoreCase(cc.getEmail())){
                return (CenterCoordinator) cc;
            }
        }
        throw new InstanceNotFoundException("There's no Center Coordinator registered with this email");
    }

    /**
     * Add to array list.
     */
//Apenas para testes
    public void addToArrayList(){
        VaccinationCenter vc1 = App.getInstance().getCompany().getVaccinationCenterStore().vaccinationCentersList.get(1);
        VaccinationCenter vc = App.getInstance().getCompany().getVaccinationCenterStore().vaccinationCentersList.get(0);
        this.employeeList.add(new Receptionist("Penélope Remígio Protásio","Rua Mário Pais da Costa Bairro Santa Luzia 3515-174 Viseu","966092022","pene@dgs.pt","281456939"));
        this.employeeList.add(new Nurse("Dália Aires Caldeira","Travessa de Santo António 3515-143 Viseu","935833194","dalia@dgs.pt","80496732"));
        this.employeeList.add(new Receptionist("Kimberly Granjeiro Peralta","Rua do Abade Dourado 3525-205 Caldas da Felgueira","934912154","kimberly@dgs.pt","24087915"));
        this.employeeList.add(new CenterCoordinator("Sandra Varejão Vital","Bairro da Corredoura 7630-127 Odemira","962162088","sandra@dgs.pt","23017584",vc1));
        this.employeeList.add(new CenterCoordinator("José António","Bairro da Corredoura 7630-127 Odemira","962162088","cc@arroz.pt","23017584",vc));
    }


}
