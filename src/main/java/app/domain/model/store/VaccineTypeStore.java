package app.domain.model.store;

import app.domain.model.VaccineType;
import app.domain.model.VaccineTypes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class VaccineTypeStore implements Serializable {

    private List<VaccineType> vaccineTypeList = new ArrayList<>();

    public VaccineTypeStore() {
    }

    public List<VaccineType> getVaccineTypeList() {
        return vaccineTypeList;
    }

    /**
     * Creates vaccine type
     * @param code
     * @param designation
     * @param whoId
     * @return new Vaccine type
     */
    public VaccineType createVaccineType (VaccineTypes vaccineTypes,String code, String designation, String whoId) {
        return new VaccineType(vaccineTypes,code, designation, whoId);
    }

    /**
     * Verifies if the vaccine type isn't null nor absent in Vaccine type list
     * @param vt
     * @return boolean
     */
    public boolean validateVaccineType (VaccineType vt) {
        if (vt == null)
            return false;
        return ! this.vaccineTypeList.contains(vt);
    }

    /**
     * Verifies if vaccine type is valid
     *Verifies if vaccine type is added in vaccine type list
     *
     * @param vt
     * @return boolean
     */

    public boolean saveVaccineType (VaccineType vt) {
        if (!validateVaccineType(vt))
            return false;
        return this.vaccineTypeList.add(vt);
    }

    public void addToArrayList(){
        vaccineTypeList.add(new VaccineType(VaccineTypes.Covid_19,"12312","Comirnaty","123312"));
        vaccineTypeList.add(new VaccineType(VaccineTypes.Covid_19,"32312","Spikevax","123122"));
        vaccineTypeList.add(new VaccineType(VaccineTypes.Covid_19,"22312","Vaxzevria","42312"));
        vaccineTypeList.add(new VaccineType(VaccineTypes.Flu,"12312","Pfizer","123312"));
        vaccineTypeList.add(new VaccineType(VaccineTypes.Hepatitis_A,"12312","Pfizer","123312"));

    }
}
