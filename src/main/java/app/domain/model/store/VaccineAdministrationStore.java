package app.domain.model.store;

import app.controller.App;
import app.controller.LoadCSVFileController;
import app.controller.SMSNotificationSender;
import app.domain.model.*;
import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.domain.model.dto.VaccineAdministrationDTO;
import app.ui.console.utils.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;
import java.util.Scanner;

/**
 * The type Vaccine administration store.
 */
public class VaccineAdministrationStore extends ScheduleAndAdminstrationStore implements Serializable, SMSNotificationSender {

    private ArrayList<VaccineAdministration> vaccineAdministrationsList = new ArrayList<>();
    private static ArrayList<VaccineAdministration> vaList = new ArrayList<>();

    /**
     * Instantiates a new Vaccine administration store.
     */
    public VaccineAdministrationStore() {
    }

    /**
     * Create vaccine administration vaccine administration.
     *
     * @param snsNumber                   the sns number
     * @param vaccineName                 the vaccine name
     * @param dose                        the dose
     * @param lotNumber                   the lot number
     * @param scheduleDateTime            the schedule date time
     * @param arrivalTime                 the arrival time
     * @param nurseAdministrationDateTime the nurse administration date time
     * @param leavingTime                 the leaving time
     * @return the vaccine administration
     */
    public VaccineAdministration createVaccineAdministration(String snsNumber, String vaccineName, String dose, String lotNumber, String scheduleDateTime, String arrivalTime, String nurseAdministrationDateTime, String leavingTime) {
        return new VaccineAdministration(snsNumber, vaccineName, dose, lotNumber, scheduleDateTime, arrivalTime, nurseAdministrationDateTime, leavingTime);
    }

    /**
     * Validate vaccine administration.
     *
     * @param va the va
     * @return the boolean
     */
    public boolean validateVaccineAdministration(VaccineAdministration va) {
        if (Objects.isNull(va))
            return false;
        else {
            VaccineAdministrationDTO vaDTO = va.getVaccineAdministrationDTO();
            if (vaDTO.getArrivalTime() == null || vaDTO.getLeavingTime() == null || vaDTO.getDose() == null || vaDTO.getSnsNumber() == null || vaDTO.getLotNumber() == null || vaDTO.getScheduleDateTime() == null || vaDTO.getNurseAdministrationDateTime() == null
                    || vaDTO.getArrivalTime().length() == 0 || vaDTO.getLeavingTime().length() == 0 || vaDTO.getDose().length() == 0 || vaDTO.getSnsNumber().length() == 0 || vaDTO.getLotNumber().length() == 0 || vaDTO.getScheduleDateTime().length() == 0 || vaDTO.getNurseAdministrationDateTime().length() == 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Add vaccination administration.
     *
     * @param va the va
     * @return the boolean
     */
    public boolean addVaccinationAdministration(VaccineAdministration va) {
        if (validateVaccineAdministration(va)) {
            if (!checkDuplicates(va)) {
                vaccineAdministrationsList.add(va);
                return true;
            } else {
                return false;
            }
        }
        return false;
    }


    private boolean checkDuplicates(VaccineAdministration va) {
        return vaccineAdministrationsList.contains(va);
    }


    /**
     * Gets va list.
     *
     * @return the va list
     */
    public ArrayList<VaccineAdministration> getVaList() {
        return vaList;
    }

    public ArrayList<VaccineAdministration> getVaccineAdministrationsList() {
        return vaccineAdministrationsList;
    }

    /**
     * Gets va list from file.
     *
     * @param file the file
     * @return the va list from file
     */
    public static ArrayList<VaccineAdministration> getVaListFromFile(File file) {
        VaccineAdministration va;
        ArrayList<VaccineAdministration> vaList= new ArrayList<>();

        try {
            Scanner in = new Scanner(file);
            String splitLine = "";
            if (LoadCSVFileController.verifyIfFileHasHeader(file)) {
                try {
                    in = new Scanner(file, "UTF-8");
                    in.nextLine();
                    splitLine = ";";
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    in = new Scanner(file, "UTF-8");
                    splitLine = ",";
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
            while (in.hasNextLine()) {
                String[] fileInfo = in.nextLine().split(splitLine);
                try {
                    if (fileInfo[1].length() == 0) {
                        fileInfo[1] = "NA";
                    }
                    va = new VaccineAdministration(fileInfo[0], fileInfo[1], fileInfo[2], fileInfo[3], fileInfo[4], fileInfo[5], fileInfo[6], fileInfo[7]);

                    vaList.add(va);

                } catch (ArrayIndexOutOfBoundsException ex) {
                    System.out.println("Array out off bounds");
                }

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if (vaList.equals(null)) {
            System.out.println("Vaccine Administration is null");
        }
        return vaList;
    }


    /**
     * Add sns user to choice box array list.
     *
     * @param vc the vc
     * @return the array list
     */
    public ArrayList<String> addSNSUserToChoiceBox(VaccinationCenter vc) {
        Company company = App.getInstance().getCompany();
        ArrivalTimeStore store = company.getArrivalTimeStore();
        ArrayList<String> snsUserNumberList = new ArrayList<>();
        for (ArrivalTime a : store.getArrivalTimeByVC(vc)) {
            SNSUser snsUser = company.getSnsUserStore().getSNSUserWithAGivenSNSUserNumber(a.getArrivalTimeDTO().getSnsNum());
            if (snsUser != null)
                snsUserNumberList.add(String.format("Name: %s \nSNS User Number: %s", snsUser.getSNSUserDTO().getName(), a.getArrivalTimeDTO().getSnsNum()));
        }
        return snsUserNumberList;
    }

    /**
     * Gets arrival time by sns number.
     *
     * @param snsUserNumber the sns user number
     * @param vc            the vc
     * @return the arrival time by sns number
     */
    public ArrivalTime getArrivalTimeBySNSNumber(String snsUserNumber, VaccinationCenter vc) {
        Company company = App.getInstance().getCompany();
        ArrivalTimeStore store = company.getArrivalTimeStore();
        ArrayList<ArrivalTime> arrivalList = store.getArrivalTimeByVC(vc);
        for (ArrivalTime a : arrivalList) {
            if (a.getArrivalTimeDTO().getSnsNum().equalsIgnoreCase(snsUserNumber)) {
                return a;
            }
        }
        return null;
    }

    /**
     * Gets vaccination schedule by sns number.
     *
     * @param snsUserNumber the sns user number
     * @param scheduleDate  the schedule date
     * @return the vaccination schedule by sns number
     */
    public VaccinationSchedule getVaccinationScheduleBySNSNumber(String snsUserNumber, String scheduleDate) {
        Company company = App.getInstance().getCompany();
        for (VaccinationSchedule vs : company.getVaccinationScheduleStore().getVaccinationScheduleList()) {
            String date = Utils.convertToSimpleDataFormat(vs.getScheduleDTO().getDate());
            if (vs.getScheduleDTO().getSnsUserNumber().equalsIgnoreCase(snsUserNumber) && date.equalsIgnoreCase(scheduleDate))
                return vs;
        }
        return null;
    }

    /**
     * Fill vaccine box array list.
     *
     * @param vt the vt
     * @return the array list
     */
    public ArrayList<String> fillVaccineBox(VaccineType vt) {
        Company company = App.getInstance().getCompany();
        ArrayList<String> vaccineListByType = new ArrayList<>();
        for (VaccineType vts : company.getVaccineTypeStore().getVaccineTypeList()) {
            if (vts.getVaccineTypeDTO().getType().name().equalsIgnoreCase(vt.getVaccineTypeDTO().getType().name()))
                vaccineListByType.add(String.format("Vaccine Type: %s\nVaccine Designation: %s", vts.getVaccineTypeDTO().getType().toString(), vts.getVaccineTypeDTO().getDesignation()));
        }
        return vaccineListByType;
    }

    /**
     * Selection sort by arrival time.
     *
     * @param vaList the va list
     */
    public static void selectionSortByArrivalTime(ArrayList<VaccineAdministration> vaList, boolean ascendant) {

        int n = vaList.size();

        // One by one move boundary of unsorted subarray
        for (int i = 0; i < n - 1; i++) {

            try {

                String arrivalTime = vaList.get(i).getVaccineAdministrationDTO().getArrivalTime();
                int hour = StringIntoTimeArray(arrivalTime)[0];
                int minute = StringIntoTimeArray(arrivalTime)[1];

                // Find the minimum element in unsorted array
                int min = i;

                for (int j = i + 1; j < n; j++) {

                    String arrivalTimeMIN = vaList.get(j).getVaccineAdministrationDTO().getArrivalTime();
                    int hourMIN = StringIntoTimeArray(arrivalTimeMIN)[0];
                    int minuteMIN = StringIntoTimeArray(arrivalTimeMIN)[1];

                    if(ascendant){

                        if (hourMIN < hour) {
                            min = j;
                            hour =hourMIN;
                            minute=minuteMIN;

                        } else {
                            if (hourMIN == hour && minuteMIN < minute) {
                                min = j;
                                minute=minuteMIN;

                            }
                        }
                    }else{
                        if (hourMIN > hour) {
                            min = j;
                            hour =hourMIN;
                            minute=minuteMIN;

                        } else {
                            if (hourMIN == hour && minuteMIN > minute) {
                                min = j;
                                minute=minuteMIN;
                            }
                        }
                    }
                }


                // Swap the found minimum element with the first
                // element
                Collections.swap(vaList, min, i);
            } catch (NumberFormatException e) {

                System.out.println("Arrival Time '"+vaList.get(i).getVaccineAdministrationDTO().getArrivalTime()+"' is not valid!");
            }

        }

       // Utils.showList(vaList, "\nSelection sort by Arrival time");

    }

    /**
     * Selection sort by leaving time.
     *
     * @param vaList the va list
     */
    public static void selectionSortByLeavingTime(ArrayList<VaccineAdministration> vaList, boolean ascendant) {

        int n = vaList.size();

        // One by one move boundary of unsorted subarray
        for (int i = 0; i < n - 1; i++) {

            try {

                String leavingTime = vaList.get(i).getVaccineAdministrationDTO().getLeavingTime();
                int hour = StringIntoTimeArray(leavingTime)[0];
                int minute = StringIntoTimeArray(leavingTime)[1];

                // Find the minimum element in unsorted array
                int min = i;
                for (int j = i + 1; j < n; j++) {

                    String leavingTimeMIN = vaList.get(j).getVaccineAdministrationDTO().getLeavingTime();
                    int hourMIN = StringIntoTimeArray(leavingTimeMIN)[0];
                    int minuteMIN = StringIntoTimeArray(leavingTimeMIN)[1];

                    if(ascendant){

                    if (hourMIN < hour) {
                        min = j;
                        hour =hourMIN;
                        minute=minuteMIN;

                    } else {
                        if (hourMIN == hour && minuteMIN < minute) {
                            min = j;
                            minute=minuteMIN;
                        }
                    }
                }else{
                        if (hourMIN > hour) {
                            min = j;
                            hour =hourMIN;
                            minute=minuteMIN;
                        } else {
                            if (hourMIN == hour && minuteMIN > minute) {
                                min = j;
                                minute=minuteMIN;
                            }
                        }
                }
                }

                // Swap the found minimum element with the first
                // element
                Collections.swap(vaList, min, i);

            } catch (NumberFormatException e) {

                System.out.println("Leaving Time '"+vaList.get(i).getVaccineAdministrationDTO().getLeavingTime() +"' is not valid!");
            }

            //Utils.showList(vaList, "\nSelection sort by Leaving time");
        }
    }

    /**
     * Bubble sort by arrival time.
     *
     * @param vaList the va list
     */
    public static void bubbleSortByArrivalTime(ArrayList<VaccineAdministration> vaList, boolean ascendant) {
        int n = vaList.size();

        for (int i = 0; i < n - 1; i++) {

            try {

            for (int j = 0; j < n - i - 1; j++) {


                String arrivalTime = vaList.get(j).getVaccineAdministrationDTO().getArrivalTime();
                int hour = StringIntoTimeArray(arrivalTime)[0];
                int minute = StringIntoTimeArray(arrivalTime)[1];

                String arrivalTimeMIN = vaList.get(j + 1).getVaccineAdministrationDTO().getArrivalTime();
                int hourNext = StringIntoTimeArray(arrivalTimeMIN)[0];
                int minuteNext = StringIntoTimeArray(arrivalTimeMIN)[1];

                if (ascendant){

                if (hour == hourNext && minute > minuteNext) {
                    // System.out.println(hourNext + "-" + minuteNext);
                    // swap arr[j+1] and arr[j]
                    Collections.swap(vaList, j, j + 1);
                } else {
                    if (hour > hourNext) {
                        // System.out.println(hourNext);
                        // swap arr[j+1] and arr[j]
                        Collections.swap(vaList, j, j + 1);
                    }
                }
            }else{

                    if (hour == hourNext && minute < minuteNext) {
                        // System.out.println(hourNext + "-" + minuteNext);
                        // swap arr[j+1] and arr[j]
                        Collections.swap(vaList, j, j + 1);
                    } else {
                        if (hour < hourNext) {
                            // System.out.println(hourNext);
                            // swap arr[j+1] and arr[j]
                            Collections.swap(vaList, j, j + 1);
                        }
                    }

                }
            }
            } catch (NumberFormatException e) {
                System.out.println("Arrival Time '"+vaList.get(i).getVaccineAdministrationDTO().getArrivalTime()+"' is not valid!");
            }
        }
       // Utils.showList(this.vaList, "Bubble sort by Arrival Time");
    }

    /**
     * Bubble sort by leaving time.
     *
     * @param vaList the va list
     */
    public static void bubbleSortByLeavingTime(ArrayList<VaccineAdministration> vaList, boolean ascendant) {
        int n = vaList.size();

        for (int i = 0; i < n - 1; i++) {

            try {

            for (int j = 0; j < n - i - 1; j++) {


                    String leavingTime = vaList.get(j).getVaccineAdministrationDTO().getLeavingTime();
                    int hour = StringIntoTimeArray(leavingTime)[0];
                    int minute = StringIntoTimeArray(leavingTime)[1];

                    String leavingTimeMIN = vaList.get(j + 1).getVaccineAdministrationDTO().getLeavingTime();
                    int hourNext = StringIntoTimeArray(leavingTimeMIN)[0];
                    int minuteNext = StringIntoTimeArray(leavingTimeMIN)[1];


                if (ascendant){

                    if (hour == hourNext && minute > minuteNext) {
                        // System.out.println(hourNext + "-" + minuteNext);
                        // swap arr[j+1] and arr[j]
                        Collections.swap(vaList, j, j + 1);
                    } else {
                        if (hour > hourNext) {
                            // System.out.println(hourNext);
                            // swap arr[j+1] and arr[j]
                            Collections.swap(vaList, j, j + 1);
                        }
                    }
                }else{

                    if (hour == hourNext && minute < minuteNext) {
                        // System.out.println(hourNext + "-" + minuteNext);
                        // swap arr[j+1] and arr[j]
                        Collections.swap(vaList, j, j + 1);
                    } else {
                        if (hour < hourNext) {
                            // System.out.println(hourNext);
                            // swap arr[j+1] and arr[j]
                            Collections.swap(vaList, j, j + 1);
                        }
                    }

                }
            }

            } catch (NumberFormatException e) {

                System.out.println("Leaving Time '"+vaList.get(i).getVaccineAdministrationDTO().getLeavingTime() +"' is not valid!");

            }
        }
        //Utils.showList(this.vaList, "Bubble Sort by Leaving Time");
    }


    /**
     * String into time array int [ ].
     *
     * @param ArrivalOrLeavingDate the arrival or leaving date
     * @return the int [ ]
     */
    public static int[] StringIntoTimeArray(String ArrivalOrLeavingDate) {

        // try {
        String[] date = ArrivalOrLeavingDate.split(" ");
        String[] time = date[1].split(":");

        int[] timeInt = new int[2];
        timeInt[0] = Integer.parseInt(time[0]);
        timeInt[1] = Integer.parseInt(time[1]);

        return timeInt;
        //} catch (NumberFormatException e) {}

    }


    /**
     * Gets vaccination center by name.
     *
     * @param name the name
     * @return the vaccination center by name
     */
    public VaccinationCenter getVaccinationCenterByName(String name) {
        for (VaccinationCenter vc : App.getInstance().getCompany().getVaccinationCenterStore().vaccinationCentersList) {
            if (name.equalsIgnoreCase(vc.getVaccinationCenterDTO().getName()))
                return vc;
        }
        return null;
    }

    @Override
    public void sendSMS(String snsNumber) {
        String userNumber = snsNumber;
        SNSUser snsUser = App.getInstance().getCompany().getUserStore().getSNSUserWithAGivenSNSUserNumber(userNumber);
        String phoneNumber = snsUser.getSNSUserDTO().getPhoneNumber();
        PrintWriter printWriter = null;

        try {
            printWriter = new PrintWriter(new File("LeavingSMSto" + phoneNumber + ".txt"));
            printWriter.print(("End of recovery period!\nYou can leave the center!\nHave a nice day!"));
            System.out.println("Leaving SMS has been sent successfully!");
        } catch (FileNotFoundException ex) {
            System.out.println("File Not Found");
        } finally {
            printWriter.close();
        }
    }
    /**
     * Add to array list.
     */
    public void addToArrayList(){
        vaccineAdministrationsList.add(new VaccineAdministration("111111119","Comirmaty","First","31232-12","6/22/2022 9:11","6/22/2022 9:15","6/22/2022 9:20","6/22/2022 9:50"));
        vaccineAdministrationsList.add(new VaccineAdministration("222222228","SpikeVax","First","31232-12","6/22/2022 10:11","6/22/2022 10:15","6/22/2022 10:20","6/22/2022 10:50"));
        vaccineAdministrationsList.add(new VaccineAdministration("333333337","Moderna","First","31232-12","6/22/2022 11:11","6/22/2022 11:15","6/22/2022 11:20","6/22/2022 11:50"));
        vaccineAdministrationsList.add(new VaccineAdministration("444444446","Pfizer","First","31232-12","6/22/2022 12:11","6/22/2022 12:15","6/22/2022 12:20","6/22/2022 12:50"));
        vaccineAdministrationsList.add(new VaccineAdministration("555555554","Comirmaty","First","31232-12","6/22/2022 9:11","6/22/2022 9:15","6/22/2022 9:20","6/22/2022 9:50"));
        vaccineAdministrationsList.add(new VaccineAdministration("666666665","SpikeVax","First","31232-12","6/22/2022 10:11","6/22/2022 10:15","6/22/2022 10:20","6/22/2022 10:50"));
        vaccineAdministrationsList.add(new VaccineAdministration("777777773","Moderna","First","31232-12","6/22/2022 11:11","6/22/2022 11:15","6/22/2022 11:20","6/22/2022 11:50"));
        vaccineAdministrationsList.add(new VaccineAdministration("999999992","Pfizer","First","31232-12","6/22/2022 12:11","6/22/2022 12:15","6/22/2022 12:20","6/22/2022 12:50"));
    }
}
