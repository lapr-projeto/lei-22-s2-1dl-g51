package app.domain.model.store;

import app.controller.App;
import app.domain.model.VaccineAdministration;
import app.domain.model.employees.CenterCoordinator;

import java.io.Serializable;


/**
 * The type Vaccination statistics store.
 */
public class VaccinationStatisticsStore implements Serializable {

    /**
     * Gets number of vaccinated.
     *
     * @param cc                 the cc
     * @param administrationDate the administration date
     * @return the number of vaccinated
     */
    public static int getNumberOfVaccinated(CenterCoordinator cc, String administrationDate) {
        int countVaccinated = 0;
        VaccineAdministrationStore vaStore = App.getInstance().getCompany().getVaccineAdministrationStore();
        for (VaccineAdministration va : vaStore.getVaccineAdministrationsList()) {
            String dateAdmin = va.getVaccineAdministrationDTO().getNurseAdministrationDateTime().split(" ")[0];
            if (cc.getVaccinationCenter().getVaccinationCenterDTO().getName() != null && va.getVaccineAdministrationDTO().getVaccinationCenter().equalsIgnoreCase(cc.getVaccinationCenter().getVaccinationCenterDTO().getName()) && administrationDate.equalsIgnoreCase(dateAdmin)) {
                countVaccinated++;
            }
        }
        return countVaccinated;
    }

}
