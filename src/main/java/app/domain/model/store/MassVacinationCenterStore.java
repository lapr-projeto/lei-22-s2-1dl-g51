package app.domain.model.store;

import app.domain.model.VaccinationCenter.MassVaccinationCenter;
import app.domain.model.VaccineType;

import java.io.Serializable;

/**
 * The type Mass vacination center store.
 */
public class MassVacinationCenterStore extends VaccinationCenterStore implements Serializable {

    /**
     * Instantiates a new Mass vacination center store.
     */
    public MassVacinationCenterStore(){
    }

    /**
     * Makes use of the previous vc creation method and creates a Mass Vaccination Center
     *
     * @param name           the name
     * @param address        the address
     * @param phoneNumber    the phone number
     * @param email          the email
     * @param openHours      the open hours
     * @param closingHours   the closing hours
     * @param vacinesSlotCap the vacines slot cap
     * @param slotDuration   the slot duration
     * @param vaccineType    the vaccine type
     * @return New MVC
     */
    public MassVaccinationCenter createMVC(String name, String address, String phoneNumber, String email, String openHours,
                                           String closingHours, String vacinesSlotCap, String slotDuration, VaccineType vaccineType) {
        return new MassVaccinationCenter(name, address, phoneNumber, email, openHours, closingHours, vacinesSlotCap, slotDuration,vaccineType);
    }

    /**
     * Verifies if the mass vaccination center isn't null nor absent in mass vaccination center list
     *
     * @param mvc the mvc
     * @return boolean boolean
     */
    public boolean validateMVC(MassVaccinationCenter mvc){
        if (mvc == null)
            return false;
        return ! vaccinationCentersList.contains(mvc);
    }

    /**
     * Verifies if mass vaccination center is valid
     * Verifies if mass vaccination center is added in mass vaccination center list
     *
     * @param mvc the mvc
     * @return boolean
     */
    public boolean saveMVC (MassVaccinationCenter mvc){
        if (!validateMVC(mvc))
            return false;
        return vaccinationCentersList.add(mvc);
    }
}
