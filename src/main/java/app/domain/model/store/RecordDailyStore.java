package app.domain.model.store;

import app.controller.App;
import app.domain.model.VaccineAdministration;

import java.io.Serializable;

/**
 * The type Record daily store.
 */
public class RecordDailyStore implements Serializable {
    /**
     * Gets number of administrations.
     *
     * @param data              the data
     * @param vaccinationCenter the vaccination center
     * @return the number of administrations
     */
    public static int getNumberOfAdministrations(String data, String vaccinationCenter) {
        int count = 0;
        for (VaccineAdministration va : App.getInstance().getCompany().getVaccineAdministrationStore().getVaccineAdministrationsList()
        ) {
            String data2 = va.getVaccineAdministrationDTO().getNurseAdministrationDateTime().split(" ")[0];
            if (data.equalsIgnoreCase(data2) && vaccinationCenter.equalsIgnoreCase(va.getVaccineAdministrationDTO().getVaccinationCenter())) {
                count++;
            }
        }
        return count;

    }
}
