package app.domain.model.store;

import app.domain.model.PerformanceData;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InvalidObjectException;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.chrono.ChronoLocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * The type Performance analysis store.
 */
public class PerformanceAnalysisStore implements Serializable {

    /**
     * The Start.
     */
    static int start = 0;
    /**
     * The End.
     */
    static int end = 0;

    private static ArrayList<PerformanceData> performanceList = new ArrayList<>();

    private ArrayList<PerformanceData> intervalTime = new ArrayList<>();


    /**
     * Validate file boolean.
     *
     * @param file the file
     * @return the boolean
     * @throws InvalidObjectException the invalid object exception
     */
    public static boolean validateFile(File file) throws InvalidObjectException {
        Scanner readFile = null;
        try {
            readFile = new Scanner(file, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String firstLine = readFile.nextLine();
        if (firstLine.contains(";")) {
            return true;
        }else {throw new InvalidObjectException("Invalid File");
        }
    }

    /**
     * Data list.
     *
     * @param file the file
     */
    public static void dataList(File file) {
        Scanner readFile = null;
        try {
            readFile = new Scanner(file);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        readFile.nextLine();
        while (readFile.hasNextLine()) {
            String[] performanceInfo = readFile.nextLine().split(";");
           // SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
            LocalDateTime arrivalTime = null;
            LocalDateTime leavingTime= null;

            arrivalTime = LocalDateTime.parse(performanceInfo[5], DateTimeFormatter.ofPattern("M/dd/yyyy H:mm"));
            leavingTime = LocalDateTime.parse(performanceInfo[7], DateTimeFormatter.ofPattern("M/dd/yyyy H:mm"));
            PerformanceData performanceData = new PerformanceData(arrivalTime, leavingTime);
            performanceList.add(performanceData);

        }

    }

    /**
     * Contiguous sublist list.
     *
     * @param array the array
     * @return the list
     */
    public static List<Integer> contiguousSublist(List <Integer> array){
            List <Integer> sublistMaxSum = new ArrayList<>();

            if (array == null || array.size() == 0) {
                return null;
            }
            int maxSum = array.get(0);

            for (int i = 0; i < array.size(); i++) {
                int sum = 0;

                for (int j = i; j < array.size(); j++) {
                    sum += array.get(j);
                    if (maxSum < sum) {
                        maxSum = sum;
                        start = i;
                        end = j;
                    }
                }
            }

            for (int k = start; k < end + 1; k++) {
                sublistMaxSum.add(array.get(k));
            }

            return sublistMaxSum;
        }


    /**
     * Max sum contiguous sublist int.
     *
     * @param subList the sub list
     * @return the int
     */
    public static int maxSumContiguousSublist (List <Integer> subList) {
            int sum = 0;
            for (int i = 0; i < subList.size(); i++) {
                sum = sum + subList.get(i);
            }
            return sum;
        }


    /**
     * Gets interval.
     *
     * @param a the a
     * @return the interval
     * @throws InvalidObjectException the invalid object exception
     */
    public static int getInterval(int a) throws InvalidObjectException {
        if ( 720%a == 0){
            return 720/a;
        }
        else {
            throw new InvalidObjectException("Invalid interval of time");
        }
    }

    /**
     * Analyse list.
     *
     * @param interval the interval
     * @param date     the date
     * @param data     the data
     * @return the list
     * @throws InvalidObjectException the invalid object exception
     */
    public List<Integer> analyse(int interval, LocalDate date, List<PerformanceData> data) throws InvalidObjectException {
        List<Integer> diffList = new ArrayList<>();

        int listLength = getInterval(interval);


        LocalDateTime currentInterval = LocalDateTime.of(LocalDate.of(date.getYear(), date.getMonthValue(), date.getDayOfMonth()), LocalTime.of(8,0,0));

        intervalTime = new ArrayList<>();

        for (int i = 0; i < listLength; i++) {
            int entered = 0;
            int left = 0;
            for (PerformanceData presenceTime: data){
                if(presenceTime.getArrivalDate().isAfter(ChronoLocalDateTime.from(currentInterval.minusMinutes(1))) && presenceTime.getArrivalDate().isBefore(ChronoLocalDateTime.from(currentInterval.plusMinutes(interval)))){
                    entered ++;
                }

                if(presenceTime.getLeavingDate().isAfter(ChronoLocalDateTime.from(currentInterval.minusMinutes(1))) && presenceTime.getLeavingDate().isBefore(ChronoLocalDateTime.from(currentInterval.plusMinutes(interval)))){
                    left ++;
                }
            }

            diffList.add(entered-left);
            PerformanceData definedInterval = new PerformanceData(currentInterval, currentInterval.plusMinutes(interval));
            intervalTime.add(definedInterval);
            currentInterval = currentInterval.plusMinutes(interval);


        }

        return diffList;
    }

    /**
     * Gets interval time.
     *
     * @return the interval time
     */
    public ArrayList<PerformanceData> getIntervalTime() {return intervalTime;}

    /**
     * Gets performance list.
     *
     * @return the performance list
     */
    public static ArrayList<PerformanceData> getPerformanceList() {
        return performanceList;
    }

    /**
     * Gets end.
     *
     * @return the end
     */
    public static int getEnd() {
        return end;
    }

    /**
     * Gets start.
     *
     * @return the start
     */
    public static int getStart() {
        return start;
    }


}



