package app.domain.model.store;

import app.controller.App;
import app.domain.model.ArrivalTime;
import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.domain.model.dto.ArrivalTimeDTO;
import app.domain.model.dto.VaccineAdministrationDTO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Represents the Store for the arrival time data
 */
public class ArrivalTimeStore implements Serializable {

    private ArrayList<ArrivalTime> arrivaltimes = new ArrayList<>();

    private ArrayList<ArrivalTime> arrivaltimesVC = new ArrayList<>();

    /**
     * Gets arrivaltimes.
     *
     * @return the arrivaltimes
     */
    public ArrayList<ArrivalTime> getArrivaltimes() {
        return arrivaltimes;
    }


    /**
     * Validates if the arrival of the SNSUser coincides with the appointment time.
     *
     * @param vc the vc
     * @return In true case it return the array list of the arrival time, otherwise it returns an empty array list.
     */
    public ArrayList<ArrivalTime> getArrivalTimeByVC(VaccinationCenter vc){
        arrivaltimesVC = new ArrayList<>();
        for (ArrivalTime arrivalTime: arrivaltimes) {
            if (arrivalTime.getArrivalTimeDTO().getVc().equals(vc)){
                arrivaltimesVC.add(arrivalTime);
            }
        }
        return arrivaltimesVC;
    }

    /**
     * Remove from waiting list when vaccine administrated.
     *
     * @param va the va
     */
    public void removeFromWaitingListWhenVaccineAdministrated(VaccineAdministrationDTO va){
        ArrayList<ArrivalTime> arrivalTimesCopy = new ArrayList<>(arrivaltimes);
        for (ArrivalTime arrivalTime: arrivalTimesCopy) {
            ArrivalTimeDTO timeDTO = arrivalTime.getArrivalTimeDTO();
            if(timeDTO.getSnsNum().equalsIgnoreCase(va.getSnsNumber()))
                arrivaltimes.remove(arrivalTime);
            }
        }


    /**
     * For test
     */
    public void addToArrayList() {
        VaccinationCenter vc1= App.getInstance().getCompany().getVaccinationCenterStore().getVaccinationCentersList().get(0);
        VaccinationCenter vc2= App.getInstance().getCompany().getVaccinationCenterStore().getVaccinationCentersList().get(1);
        VaccinationCenter vc3= App.getInstance().getCompany().getVaccinationCenterStore().getVaccinationCentersList().get(2);
        VaccinationCenter vc4= App.getInstance().getCompany().getVaccinationCenterStore().getVaccinationCentersList().get(3);

        this.arrivaltimes.add(new ArrivalTime("160478539",vc1,new Date(),"15:30"));
        this.arrivaltimes.add(new ArrivalTime("173056892",vc1,new Date(),"18:30"));
        this.arrivaltimes.add(new ArrivalTime("123456789",vc1,new Date(),"17:30"));
        this.arrivaltimes.add(new ArrivalTime("799888776",vc3,new Date(),"12:30"));
        this.arrivaltimes.add(new ArrivalTime("111111111",vc2,new Date(),"12:30"));
        this.arrivaltimes.add(new ArrivalTime("222222222",vc3,new Date(),"12:30"));
        this.arrivaltimes.add(new ArrivalTime("333333333",vc3,new Date(),"12:30"));
        this.arrivaltimes.add(new ArrivalTime("444444444",vc4,new Date(),"12:30"));
    }

}
