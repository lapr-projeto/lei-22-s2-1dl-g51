package app.domain.model.store;

import app.domain.model.AdverseReactions;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * The type Adverse reactions store.
 */
public class AdverseReactionsStore implements Serializable {

    private ArrayList<AdverseReactions> adverseReactionsList = new ArrayList<>();

    /**
     * Instantiates a new Adverse reactions store.
     */
    public AdverseReactionsStore() {
    }

    /**
     * Gets adverse reactions list.
     *
     * @return the adverse reactions list
     */
    public ArrayList<AdverseReactions> getAdverseReactionsList() {
        return adverseReactionsList;
    }

    /**
     * Add to adverse reactions list.
     *
     * @param ar the ar
     */
    public void addToAdverseReactionsList(AdverseReactions ar){
        adverseReactionsList.add(ar);
    }

    /**
     * Get adverse reactions by sns number adverse reactions.
     *
     * @param snsNumber the sns number
     * @return the adverse reactions
     */
    public AdverseReactions getAdverseReactionsBySnsNumber(String snsNumber){
        for (AdverseReactions ar: adverseReactionsList) {
            if(ar.getAdverseReactionsDTO().getSnsUserNumber().equalsIgnoreCase(snsNumber))
                return ar;
        }
        return null;
    }

    /**
     * Set adverse reactions.
     *
     * @param snsNumber        the sns number
     * @param adverseReactions the adverse reactions
     */
    public void setAdverseReactions(String snsNumber,String adverseReactions){
        AdverseReactions ar = getAdverseReactionsBySnsNumber(snsNumber);

        if(ar != null){
            ar.setAdverseReactions(adverseReactions);
        }else{
            ar = new AdverseReactions(snsNumber,adverseReactions);
            addToAdverseReactionsList(ar);
        }
    }
}
