package app.domain.model;

import app.controller.App;
import app.controller.RecordDailyController;
import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.domain.model.store.RecordDailyStore;
import app.domain.model.store.VaccinationCenterStore;

import java.io.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * The type Record daily.
 * Starts the loop for record daily the total number of people vaccinated in the CSV file.
 */
public class RecordDaily extends TimerTask implements Serializable {
    private static VaccinationCenterStore vaccinationCenterStore = App.getInstance().getCompany().getVaccinationCenterStore();

    public void run() {
        System.out.println("The system has recorded all of today's data and updated the CSV File!");

        for (VaccinationCenter vcs : vaccinationCenterStore.getVaccinationCentersList()) {
            try (PrintWriter writer = new PrintWriter(new FileWriter("src/main/resources/dgsdata.csv", true))) {
                LocalDateTime today = LocalDateTime.now();
                String date2 = today.format(DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm"));
                StringBuilder sb = new StringBuilder();
                sb.append(date2);
                sb.append(',');
                sb.append(vcs.getVaccinationCenterDTO().getName());
                sb.append(',');
                String date = today.format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
                sb.append(RecordDailyStore.getNumberOfAdministrations(date,vcs.getVaccinationCenterDTO().getName()));
                sb.append('\n');

                writer.write(sb.toString());

            } catch (FileNotFoundException e) {
                System.out.println(e.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Administration count.
     *This method uses the date set in the "config.properties" and when the loop end regists all the data in the "dgsdata.csv".
     * Gets the date in config.properties
     * Change the date in string to int
     * Register all the data in the CSV File ( Register at each vaccination center the number of people vaccinated in the end of the loop)
     */

    public void writeAdministrationCountCSV(){
        String hour = App.getInstance().getProperties().getProperty("HOUR");
        String minute = App.getInstance().getProperties().getProperty("MINUTE");
        String seconds = App.getInstance().getProperties().getProperty("SECOND");


        LocalDateTime ldt = LocalDateTime.now();
        Date date = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
        date.setHours(Integer.valueOf(hour));
        date.setMinutes(Integer.valueOf(minute));
        date.setSeconds(Integer.valueOf(seconds));


        try (PrintWriter writer = new PrintWriter(new FileWriter("src/main/resources/dgsdata.csv", true))) {

            StringBuilder sb = new StringBuilder();
            sb.append("Register Date");
            sb.append(',');
            sb.append("Vaccination Center");
            sb.append(',');
            sb.append("Total people vaccinated");
            sb.append('\n');


            writer.write(sb.toString());

        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }



        Timer timer = new Timer();

        /**
        Use period to set the milliseconds for next loop
        86 400 000 means a full day! (24 hours)
        10000(10 seconds only for demonstration purpose)
         **/

        int period = 10000;
        timer.schedule(new RecordDaily(), date, period);


    }

}

