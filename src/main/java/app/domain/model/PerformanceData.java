package app.domain.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * The type Performance data.
 */
public class PerformanceData implements Serializable {

    private LocalDateTime arrivalDate;


    private LocalDateTime leavingDate;


    /**
     * Instantiates a new Performance data.
     *
     * @param arrivalTime the arrival time
     * @param leavingTime the leaving time
     */
    public PerformanceData (LocalDateTime arrivalTime, LocalDateTime leavingTime){
        this.arrivalDate = arrivalTime;
        this.leavingDate = leavingTime;

    }

    /**
     * Gets arrival date.
     *
     * @return the arrival date
     */
    public LocalDateTime getArrivalDate() {
        return arrivalDate;
    }

    /**
     * Gets leaving date.
     *
     * @return the leaving date
     */
    public LocalDateTime getLeavingDate() {
        return leavingDate;
    }

    @Override
    public String toString() {
        return arrivalDate.format(DateTimeFormatter.ofPattern("HH:mm")) + " | " + leavingDate.format(DateTimeFormatter.ofPattern("HH:mm"));
    }

    /**
     * To string 2 string.
     *
     * @return the string
     */
    public String toString2(){
        return arrivalDate.format(DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm"));// + ", " //+ leavingDate.format(DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm"));
    }
}
