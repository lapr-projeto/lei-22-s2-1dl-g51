package app.domain.model.VaccinationCenter;

import app.domain.model.dto.VaccinationCenterDTO;

import java.io.Serializable;
import java.util.Objects;

/**
 * The type Vaccination center.
 */
public class VaccinationCenter implements Serializable {
    private String name;
    private String address;
    private String phoneNumber;
    private String email;
    private String openHours;
    private String closingHours;
    private String vaccinesSlotCap;
    private String slotDuration;


    //SPRINT C
    private VaccinationCenterDTO centerDTO;

    /**
     * Instantiates a new Vaccination center.
     *
     * @param name            the name
     * @param address         the address
     * @param phoneNumber     the phone number
     * @param email           the email
     * @param openHours       the open hours
     * @param closingHours    the closing hours
     * @param vaccinesSlotCap the vaccines slot cap
     * @param slotDuration    the slot duration
     */
    public VaccinationCenter(String name, String address, String phoneNumber, String email, String openHours, String closingHours, String vaccinesSlotCap, String slotDuration) {
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.openHours = openHours;
        this.closingHours = closingHours;
        this.vaccinesSlotCap = vaccinesSlotCap;
        this.slotDuration = slotDuration;

        //SPRINT C
        this.centerDTO = new VaccinationCenterDTO(name,address,phoneNumber,email,openHours,closingHours, vaccinesSlotCap,slotDuration);
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets address.
     *
     * @param address the address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Sets phone number.
     *
     * @param phoneNumber the phone number
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Sets open hours.
     *
     * @param openHours the open hours
     */
    public void setOpenHours(String openHours) {
        this.openHours = openHours;
    }


    /**
     * Sets closing hours.
     *
     * @param closingHours the closing hours
     */
    public void setClosingHours(String closingHours) {
        this.closingHours = closingHours;
    }

    /**
     * Sets vaccines slot cap.
     *
     * @param vaccinesSlotCap the vaccines slot cap
     */
    public void setVaccinesSlotCap(String vaccinesSlotCap) {
        this.vaccinesSlotCap = vaccinesSlotCap;
    }

    /**
     * Sets slot duration.
     *
     * @param slotDuration the slot duration
     */
    public void setSlotDuration(String slotDuration) {
        this.slotDuration = slotDuration;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VaccinationCenter that = (VaccinationCenter) o;
        return Objects.equals(name, that.name) && Objects.equals(address, that.address) && Objects.equals(phoneNumber, that.phoneNumber) && Objects.equals(email, that.email) && Objects.equals(openHours, that.openHours) && Objects.equals(closingHours, that.closingHours) && Objects.equals(vaccinesSlotCap, that.vaccinesSlotCap) && Objects.equals(slotDuration, that.slotDuration);
    }

    @Override
    public String toString() {
        return String.format("\nName: %s\nAddress: %s\nPhone Number: %s\nEmail: %s\nOpen Hours: %s\n" +
                "Closing Hours: %s\nMaximum number of vaccines that can be given per slot: %s\nSlot Duration: %s\n",
                name,address,phoneNumber,email,openHours,closingHours,vaccinesSlotCap,slotDuration);
    }

    /**
     * To simple string string.
     *
     * @return the string
     */
    public String toSimpleString(){
        return String.format("\n  Name: %s\n  Address: %s\n  Phone Number: %s\n  Email: %s\n  Open Hours: %s\n  Closing Hours: %s",name,address,phoneNumber,email,openHours,closingHours);
    }


    /**
     * Gets vaccination center dto.
     *
     * @return the vaccination center dto
     */
//SPRINT C
    public VaccinationCenterDTO getVaccinationCenterDTO() {
        return centerDTO;
    }

}
