package app.domain.model.VaccinationCenter;

import app.domain.model.VaccineType;
import app.domain.model.dto.HealthCenterDTO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 * The type Health center.
 */
public class HealthCenter extends VaccinationCenter implements Serializable {
    private ArrayList<VaccineType> vaccineTypes;
    private HealthCenterDTO healthCenterDTO;

    /**
     * Instantiates a new Health center.
     *
     * @param name           the name
     * @param address        the address
     * @param phoneNumber    the phone number
     * @param email          the email
     * @param openHours      the open hours
     * @param closingHours   the closing hours
     * @param vacinesSlotCap the vacines slot cap
     * @param slotDuration   the slot duration
     * @param vaccineTypes   the vaccine types
     */
    public HealthCenter(String name, String address, String phoneNumber, String email, String openHours, String closingHours, String vacinesSlotCap, String slotDuration, ArrayList<VaccineType> vaccineTypes) {
        super(name, address, phoneNumber, email, openHours, closingHours, vacinesSlotCap, slotDuration);
        this.vaccineTypes = vaccineTypes;

        this.healthCenterDTO = new HealthCenterDTO(name, address, phoneNumber, email, openHours, closingHours, vacinesSlotCap, slotDuration,vaccineTypes);
    }

    /**
     * Sets vaccine types.
     *
     * @param vaccineTypes the vaccine types
     */
    public void setVaccineTypes(ArrayList<VaccineType> vaccineTypes) {
        this.vaccineTypes = vaccineTypes;
    }

    /**
     * Gets health center dto.
     *
     * @return the health center dto
     */
    public HealthCenterDTO getHealthCenterDTO() {
        return healthCenterDTO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        HealthCenter that = (HealthCenter) o;
        return Objects.equals(vaccineTypes, that.vaccineTypes);
    }

    public String toString() {
        return String.format("\n==================HEALTH CARE CENTER==================%sVaccine Types: \n%s======================================================", super.toString(),vaccineTypesToString());
    }

    /**
     * Vaccine types to string string.
     *
     * @return the string
     */
    public String vaccineTypesToString(){
        StringBuilder stringBuilder = new StringBuilder();

        for (VaccineType vc:vaccineTypes) {
            if(vc != null) {
                String type = vc.getVaccineTypeDTO().getType().name();
                if(!stringBuilder.toString().contains(type)) {
                    stringBuilder.append("  ");
                    stringBuilder.append(type);
                    stringBuilder.append("\n");
                }
            }
        }
        return stringBuilder.toString();
    }
}
