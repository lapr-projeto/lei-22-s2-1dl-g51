package app.domain.model.VaccinationCenter;

import app.domain.model.VaccineType;
import app.domain.model.dto.MassVaccinationCenterDTO;

import java.io.Serializable;
import java.util.Objects;


/**
 * The type Mass vaccination center.
 */
public class MassVaccinationCenter extends VaccinationCenter implements Serializable {
    private VaccineType vaccineType;
    private MassVaccinationCenterDTO massVaccinationCenterDTO;

    /**
     * Instantiates a new Mass vaccination center.
     *
     * @param name           the name
     * @param address        the address
     * @param phoneNumber    the phone number
     * @param email          the email
     * @param openHours      the open hours
     * @param closingHours   the closing hours
     * @param vacinesSlotCap the vacines slot cap
     * @param slotDuration   the slot duration
     * @param vaccineType    the vaccine type
     */
    public MassVaccinationCenter(String name, String address, String phoneNumber, String email, String openHours,
                                 String closingHours, String vacinesSlotCap, String slotDuration, VaccineType vaccineType) {
        super(name, address, phoneNumber, email, openHours, closingHours, vacinesSlotCap, slotDuration);
        this.vaccineType = vaccineType;

        this.massVaccinationCenterDTO = new MassVaccinationCenterDTO(name, address, phoneNumber, email, openHours, closingHours, vacinesSlotCap, slotDuration,vaccineType);
    }

    /**
     * Gets mass vaccination center dto.
     *
     * @return the mass vaccination center dto
     */
    public MassVaccinationCenterDTO getMassVaccinationCenterDTO() {
        return massVaccinationCenterDTO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MassVaccinationCenter that = (MassVaccinationCenter) o;
        return Objects.equals(vaccineType, that.vaccineType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vaccineType);
    }

    public String toString() {
        return String.format("\n==================MASS VACCINATION CENTER==================%sVaccine Type: %s\n===========================================================\n", super.toString(),vaccineType.getVaccineTypeDTO().getType());
    }
}
