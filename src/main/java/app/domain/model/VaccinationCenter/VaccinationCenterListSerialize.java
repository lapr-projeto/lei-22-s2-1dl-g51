package app.domain.model.VaccinationCenter;


import java.io.*;
import java.util.List;

/**
 * The type Vaccination center list serialize.
 */
public class VaccinationCenterListSerialize implements Serializable {

    private List<VaccinationCenter> vcList;

    /**
     * Read vaccination center list serialize.
     *
     * @param file the file
     * @return the vaccination center list serialize
     * @throws IOException            the io exception
     * @throws ClassNotFoundException the class not found exception
     */
    public static VaccinationCenterListSerialize read(String file) throws IOException, ClassNotFoundException {
        VaccinationCenterListSerialize vcl;

        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));

            try {
                vcl = (VaccinationCenterListSerialize) in.readObject();

            } finally {
                in.close();

            }
            return vcl;
        } catch (IOException | ClassNotFoundException e) {
            return new VaccinationCenterListSerialize();
        }
    }

    /**
     * Write boolean.
     *
     * @param vcl  the vcl
     * @param file the file
     * @return the boolean
     */
    public static boolean write(VaccinationCenterListSerialize vcl, String file) {

        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));

            try {
                out.writeObject(vcl);

            } finally {
                out.close();

            }
            return true;
        } catch (IOException e) {
            return false;
        }

    }
}