package app.domain.model;

import app.domain.model.dto.SNSUserDTO;
import app.ui.console.utils.Utils;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Represents a SNS User enrolled in the system.
 */
public class SNSUser extends Person implements Serializable {

    /**
     * The gender of the SNS User
     */
    private String gender;

    /**
     * The birthdate of the SNS User
     */
    private Date birthDate;

    /**
     * The sns user number of the SNS User
     */
    private String snsUserNumber;

    private SNSUserDTO userDTO;


    /**
     * Creates a new SNS User with the given name, address, phoneNumber, email, citizenCardNumber, gender, birthdate and
     * sns user number.
     *
     * @param name              snsUser name
     * @param address           snsUser address
     * @param phoneNumber       snsUser phone number
     * @param email             snsUser email
     * @param citizenCardNumber snsUser citizen Card Number
     * @param gender            snsUser gender
     * @param birthDate         snsUser birthDate
     * @param snsUserNumber     snsUser sns User Number
     */
    public SNSUser(String name, String address, String phoneNumber, String email, String citizenCardNumber, String gender, Date birthDate, String snsUserNumber) {
        super(name, address, phoneNumber, email, citizenCardNumber);

        if(gender == null || gender.length() == 0)
            gender = "NA";
        else
            this.gender = gender;

        this.birthDate = birthDate;
        this.snsUserNumber = snsUserNumber;


        //SPRINT_C
        this.userDTO = new SNSUserDTO(name,address,phoneNumber,email,citizenCardNumber,gender,birthDate,snsUserNumber);
    }


    /**
     * Gets the gender of the SNSUser
     *
     * @return this SNSUser's gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * Changes the gender of this SNSUser. It's the only attribute that can be changed after SNS user registration in the system.
     *
     * @param gender snsUser gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * Gets the birthdate of the SNSUser
     *
     * @return this SNSUser's birthdate
     */
    public Date getBirthDate() {
        return birthDate;
    }


    /**
     * Gets the sns user number of the SNSUser
     *
     * @return this SNSUser's sns user number
     */
    public String getSnsUserNumber() {
        return snsUserNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SNSUser snsUser = (SNSUser) o;
        return Objects.equals(snsUserNumber, snsUser.snsUserNumber);
    }

    @Override
    public String toString() {
        if(this.gender.length() > 0)
            return String.format("===========SNS USER===========\n%sGender: %s\nBirth Date: %s\nSNS User Number: %s\n==============================",
                super.toString(),gender, Utils.convertToSimpleDataFormat(birthDate),snsUserNumber);
        else
            return String.format("===========SNS USER===========\n%sBirth Date: %s\nSNS User Number: %s\n==============================",
                super.toString(),Utils.convertToSimpleDataFormat(birthDate),snsUserNumber);
    }

    /**
     * Gets sns user dto.
     *
     * @return the sns user dto
     */
//SPRINT C
    public SNSUserDTO getSNSUserDTO() {
        return userDTO;
    }
}
