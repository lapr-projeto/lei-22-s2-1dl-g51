package app.domain.model.store;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RecordDailyStoreTest {

    @Test
    @DisplayName(
            "Should return the number of administrations when the data and vaccinationCenter are correct")
    public void testGetNumberOfAdministrationsWhenDataAndVaccinationCenterAreCorrect() {
        String data = "2020-05-01";
        String vaccinationCenter = "Vaccination Center 1";

        int numberOfAdministrations =
                RecordDailyStore.getNumberOfAdministrations(data, vaccinationCenter);

        assertEquals(0, numberOfAdministrations);
    }

    @Test
    @DisplayName("Should return 0 when the data and vaccinationCenter are incorrect")
    public void testGetNumberOfAdministrationsWhenDataAndVaccinationCenterAreIncorrect() {
        String data = "2020-05-01";
        String vaccinationCenter = "Vaccination Center 1";

        int numberOfAdministrations =
                RecordDailyStore.getNumberOfAdministrations(data, vaccinationCenter);

        assertEquals(0, numberOfAdministrations);
    }


}
