package app.domain.model.store;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.VaccinationCenter.MassVaccinationCenter;
import app.domain.model.VaccinationSchedule;
import app.domain.model.VaccineType;
import app.domain.model.VaccineTypes;
import app.domain.model.dto.VaccinationCenterDTO;
import app.ui.console.utils.Hour;
import app.ui.console.utils.Utils;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class VaccinationScheduleStoreTest {

    private Company companyTest;

    private VaccineType vaccineTest, vaccineTest2;

    private MassVaccinationCenter mvcTest, mvcTest2;

    private VaccinationSchedule scheduleTest, scheduleTest1, scheduleTest2, scheduleTest3, scheduleTest4;

    private VaccinationScheduleStore vsStoreTest;

    private VaccineTypeStore vtStoreTest;

    private VaccinationCenterStore vcStoreTest;

    private VaccinationCenterDTO vcDto1, vcDto2;


    @BeforeEach
    void setUp() {
        companyTest = App.getInstance().getCompany();

        vaccineTest = new VaccineType(VaccineTypes.Covid_19, "12312", "Pfizer", "123312");
        vaccineTest2 = new VaccineType(VaccineTypes.Flu, "12312", "Pfizer", "123312");

        mvcTest = new MassVaccinationCenter("CS Rebordosa2", "Rua Cooperativa A CELER, 4585-846 Rebordosa", "245913278", "covidRebor@dgs.pt", "08:30", "19:00", "4", "5", vaccineTest);
        mvcTest2 = new MassVaccinationCenter("CS Rebordosa3", "Rua Cooperativa A CELER, 4585-846 Rebordosa", "245913278", "covidRebor@dgs.pt", "10:00", "12:00", "5", "30", vaccineTest);

        vtStoreTest = companyTest.getVaccineTypeStore();
        vtStoreTest.getVaccineTypeList().add(vaccineTest);

        vcStoreTest = companyTest.getVcStore();
        vcStoreTest.getVaccinationCentersList().add(mvcTest);

        //Correct Data
        scheduleTest = new VaccinationSchedule("111111111", mvcTest, new Date(), "14:00", vaccineTest);

        scheduleTest1 = new VaccinationSchedule("111111333", mvcTest, new Date(), "14:00", vaccineTest);


        //SnsNumber = null
        scheduleTest2 = new VaccinationSchedule(null, mvcTest, new Date(), "16:00", vaccineTest);

        //Vaccination Center not added to the list
        scheduleTest3 = new VaccinationSchedule("333333333", mvcTest2, new Date(), "18:00", vaccineTest);

        //Vaccine Type not added to the list
        scheduleTest4 = new VaccinationSchedule("333333333", mvcTest2, new Date(), "19:00", vaccineTest2);

        vsStoreTest = companyTest.getVaccinationScheduleStore();

        vcDto1 = mvcTest.getMassVaccinationCenterDTO();

        vcDto2 = mvcTest2.getMassVaccinationCenterDTO();

    }

    @Test
    @Order(1)
    void validateVaccineSchedule() {
        assertTrue(vsStoreTest.validateVaccineSchedule(scheduleTest));
        assertFalse(vsStoreTest.validateVaccineSchedule(scheduleTest2));
        assertFalse(vsStoreTest.validateVaccineSchedule(scheduleTest3));
        assertFalse(vsStoreTest.validateVaccineSchedule(scheduleTest4));
    }

    @Test
    @Order(2)
    void checkDuplicates() {
        assertFalse(vsStoreTest.checkDuplicates(scheduleTest));
        vsStoreTest.getVaccinationScheduleList().add(scheduleTest);
        assertTrue(vsStoreTest.checkDuplicates(scheduleTest));
    }

    @Test
    @Order(3)
    void addVaccinationSchedule() {
        assertTrue(vsStoreTest.addVaccinationSchedule(scheduleTest1));
        assertFalse(vsStoreTest.addVaccinationSchedule(scheduleTest1));
    }

    @Test
    @Order(4)
    void counterSchedulesPerSlot() {
        assertEquals(vsStoreTest.counterSchedulesPerSlot("12:00", new Date().toString(), vcDto1), 4);
        assertEquals(vsStoreTest.counterSchedulesPerSlot("12:00", new Date().toString(), vcDto2), 5);
    }

    @Test
    @Order(5)
    void checkAvailableHours() {
        List<String> availableHoursTest = new ArrayList<>();
        availableHoursTest.add("10:00");
        availableHoursTest.add("10:30");
        availableHoursTest.add("11:00");
        availableHoursTest.add("11:30");
        availableHoursTest.add("12:00");

        assertArrayEquals(availableHoursTest.toArray(), vsStoreTest.checkAvailableHours(vcDto2, "23-03-2024").toArray());

        availableHoursTest.add("9:00");
        assertFalse(Arrays.equals(availableHoursTest.toArray(), vsStoreTest.checkAvailableHours(vcDto2, "23-03-2024").toArray()));

        availableHoursTest.clear();
        assertTrue(availableHoursTest.isEmpty());
    }

    @Test
    @Order(6)
    void checkAvailableHoursToday() {
        //the method gets the actual hour and date! Must fail in certain circumstances.
        assertFalse(vsStoreTest.checkAvailableHoursToday(new Hour("2:00"), Utils.convertToSimpleDataFormat(new Date())));
        assertTrue(vsStoreTest.checkAvailableHoursToday(new Hour("23:59"), Utils.convertToSimpleDataFormat(new Date())));
        assertTrue(vsStoreTest.checkAvailableHoursToday(new Hour("2:00"), "23-03-2025"));
    }


}