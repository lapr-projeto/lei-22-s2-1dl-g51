package app.domain.model.store;

import app.controller.App;
import app.domain.model.SNSUser;
import app.domain.shared.Constants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pt.isep.lei.esoft.auth.AuthFacade;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class SNSUserStoreTest {

    private SNSUser userTest1, userTest2, userTest3, userTest4,userTest5;
    private SNSUserStore storeTest;
    private AuthFacade authFacade;

    public SNSUserStoreTest() {
    }

    @BeforeEach
    void setUp() {
        App app = App.getInstance();
        storeTest = app.getCompany().getUserStore();
        userTest1 = new SNSUser("test","test","999999999","test@test.com","9999999","Other",new Date(6-5-2022),"9999999");
        userTest2 = new SNSUser(null,"test","999999999","test@test.com","9999999","Other",new Date(6-5-2022),"9999999");
        userTest3 = null;
        userTest4 = new SNSUser("test","test","999999999","test@email.com","9999999","Other",new Date(6-5-2022),"9999999");
        userTest5 = new SNSUser("test","test","999999999","test1@test1.com","9999999","Other",new Date(6-5-2022),"9999999");
        authFacade = App.getInstance().getCompany().getAuthFacade();
        authFacade.addUserWithRole("test1","test@email.com","9999999", Constants.ROLE_SNS_USER);

    }

    @Test
    void validateSNSUser() {
        assertTrue(storeTest.validateSNSUser(userTest1));
        assertFalse(storeTest.validateSNSUser(userTest2));
        assertFalse(storeTest.validateSNSUser(userTest3));
    }

    @Test
    void checkDuplicates() {
        assertFalse(storeTest.checkDuplicates(userTest4));
        assertTrue(storeTest.checkDuplicates(userTest5));
    }

    @Test
    void generatePassword() {
        String pass1 = storeTest.generatePassword();
        String pass2 = storeTest.generatePassword();

        assertEquals(pass1.length(),pass2.length());
        assertNotNull(pass1);
        assertNotNull(pass2);
        assertNotEquals(pass1,pass2);
    }

    @Test
    void addSNSUser() {
        assertTrue(storeTest.addSNSUser(userTest1));
        assertFalse(storeTest.addSNSUser(userTest4));
    }
}