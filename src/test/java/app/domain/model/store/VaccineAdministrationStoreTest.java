package app.domain.model.store;

import app.controller.App;
import app.domain.model.*;
import app.domain.model.VaccinationCenter.MassVaccinationCenter;
import app.domain.model.VaccinationCenter.VaccinationCenter;
import app.ui.console.utils.Utils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class VaccineAdministrationStoreTest {
    ArrayList<VaccineAdministration> vaList1 = new ArrayList<>();
    ArrayList<VaccineAdministration> vaList2 = new ArrayList<>();
    ArrayList<VaccineAdministration> vaList2LeaveSort = new ArrayList<>();
    ArrayList<VaccineAdministration> vaList2ArriveSort = new ArrayList<>();
    VaccineAdministration va1;
    VaccineAdministration va2;
    VaccineAdministration va3;
    VaccineAdministration va4, va5;
    VaccineAdministration vaError;

    private VaccinationCenter vc1, vc2;

    private VaccineType vaccineTest, vaccineTest2;

    private VaccineAdministrationStore vaStore;
    @BeforeEach
    void setUp() {

        va1 = (new VaccineAdministration(
                "161593120",
                "Spikevax",
                "Primeira",
                "21C16-05",
                "5/30/2022 8:00",
                "5/30/2022 11:24",
                "5/30/2022 9:11",
                "5/30/2022 9:43"));

        va2 = (new VaccineAdministration(
                "161593121",
                "Spikevax",
                "Primeira",
                "21C16-05",
                "5/30/2022 8:00",
                "5/30/2022 8:00",
                "5/30/2022 8:20",
                "5/30/2022 8:45"));

        va3 = (new VaccineAdministration(
                "161593122",
                "Spikevax",
                "Primeira",
                "21C16-05",
                "5/30/2022 8:00",
                "5/30/2022 9:00",
                "5/30/2022 9:20",
                "5/30/2022 10:55"));

        va4 = (new VaccineAdministration(
                "161593121",
                "Spikevax",
                "Primeira",
                "21C16-05",
                "5/30/2022 8:00",
                "5/30/2022 8:50",
                "5/30/2022 9:10",
                "5/30/2022 8:50"));

        vaError = (new VaccineAdministration(
                "161593121",
                "Spikevax",
                "Primeira",
                "21C16-05",
                "5/30/2022 8:00",
                "5/30/2022 8:50",
                "5/30/2022 9:10",
                "5/30/2022 8:50"));

        vaList1.add(va1);
        vaList1.add(va2);
        vaList1.add(va3);
        vaList1.add(vaError);

        vaList2.add(va1);
        vaList2.add(va2);
        vaList2.add(va3);
        vaList2.add(va4);

        vaList2LeaveSort.add(va2);
        vaList2LeaveSort.add(va4);
        vaList2LeaveSort.add(va1);
        vaList2LeaveSort.add(va3);

        vaList2ArriveSort.add(va2);
        vaList2ArriveSort.add(va4);
        vaList2ArriveSort.add(va3);
        vaList2ArriveSort.add(va1);
        vc1 = App.getInstance().getCompany().getVaccinationCenterStore().vaccinationCentersList.get(0);
        vc2 = App.getInstance().getCompany().getVaccinationCenterStore().vaccinationCentersList.get(1);
        va5 = new VaccineAdministration(null,"Spikevax","Primeira","21C16-05","5/30/2022 8:00","5/30/2022 8:50","5/30/2022 9:10","5/30/2022 8:50");

        vaStore = App.getInstance().getCompany().getVaccineAdministrationStore();

        vaccineTest = new VaccineType(VaccineTypes.Covid_19, "12312", "Pfizer", "123312");
        vaccineTest2 = new VaccineType(VaccineTypes.Hepatitis_A, "12312", "Pfizer", "123312");
    }
/*
    @Test
    @DisplayName("Should return an empty list when the file is empty")
    public void testGetVaListFromFileWhenFileIsEmptyThenReturnEmptyList() {
        File file = null;
        VaccineAdministrationStore vaStore = new VaccineAdministrationStore();

        assertEquals(0, vaStore.getVaListFromFile(file).size());
    }

 */

    /*
    @Test
    @DisplayName("Should return a list with one element when the file has one line")
    public void testGetVaListFromFileWhenFileHasOneLineThenReturnAListWithOneElement() {
        File file = new File("src/test/resources/vaccine_administration_one_line.csv");
        VaccineAdministrationStore vaccineAdministrationStore = new VaccineAdministrationStore();
        ArrayList<VaccineAdministration> vaList =
                vaccineAdministrationStore.getVaListFromFile(file);
        assertEquals(1, vaList.size());
    }

     */

    @Test
    @DisplayName("Should return null when the snsUserNumber is not in the arrivalList")
    public void
    testGetArrivalTimeBySNSNumberWhenSnsUserNumberIsNotInTheArrivalListThenReturnNull() {
        VaccinationCenter vc =
                new VaccinationCenter(
                        "Vaccination Center 1",
                        "Rua da Alegria",
                        "912341234",
                        "vc1@gmail.com",
                        "8:00",
                        "20:00",
                        "10",
                        "30");
        ArrivalTime arrivalTime = new ArrivalTime("161593120", vc, new Date(), "8:00");
        ArrivalTime arrivalTime1 = new ArrivalTime("161593121", vc, new Date(), "8:00");
        ArrivalTime arrivalTime2 = new ArrivalTime("161593122", vc, new Date(), "8:00");

        ArrayList<ArrivalTime> arrivalList = new ArrayList<>();
        arrivalList.add(arrivalTime);
        arrivalList.add(arrivalTime1);
        arrivalList.add(arrivalTime2);

        VaccineAdministrationStore vaccineAdministrationStore = new VaccineAdministrationStore();

        assertNull(vaccineAdministrationStore.getArrivalTimeBySNSNumber("161593123", vc));
    }

    /*
    @Test
    @DisplayName("Should return an ArrivalTime when the snsUserNumber is in the arrivalList")
    public void
    testGetArrivalTimeBySNSNumberWhenSnsUserNumberIsInTheArrivalListThenReturnAnArrivalTime() {
        VaccinationCenter vc =
                new VaccinationCenter(
                        "Vaccination Center 1",
                        "Rua da Alegria",
                        "912341234",
                        "vc1@gmail.com",
                        "8:00",
                        "20:00",
                        "10",
                        "30");
        ArrivalTime arrivalTime = new ArrivalTime("161593120", vc, new Date(), "8:00");
        ArrayList<ArrivalTime> arrivalList = new ArrayList<>();
        arrivalList.add(arrivalTime);

        VaccineAdministrationStore vaccineAdministrationStore = new VaccineAdministrationStore();
        ArrivalTime result = vaccineAdministrationStore.getArrivalTimeBySNSNumber("161593120", vc);

        assertEquals(arrivalTime, result);
    }

    /**
     * Selection sort by arrival time.
     */

    @Test
    void selectionSortByArrivalTime() {
        VaccineAdministrationStore.selectionSortByArrivalTime(vaList2,true);
        //assertArrayEquals(vaList2ArriveSort,vaList2,"Check");
        assertEquals(vaList2ArriveSort,vaList2);
    }

    /**
     * Selection sort by leaving time.
     */
    @Test
    void selectionSortByLeavingTime() {
        VaccineAdministrationStore.selectionSortByLeavingTime(vaList2,true);
        assertEquals(vaList2LeaveSort,vaList2);
    }

    /**
     * Bubble sort by arrival time.
     */
    @Test
    void bubbleSortByArrivalTime() {
        VaccineAdministrationStore.bubbleSortByArrivalTime(vaList2,true);
        assertEquals(vaList2ArriveSort,vaList2);
    }

    /**
     * Bubble sort by leaving time.
     */
    @Test
    void bubbleSortByLeavingTime() {
        VaccineAdministrationStore.bubbleSortByLeavingTime(vaList2,true);
        assertEquals(vaList2LeaveSort,vaList2);
    }

    /**
     * String into time array.
     */
    @Test
    void stringIntoTimeArray() {
        int hour= VaccineAdministrationStore.StringIntoTimeArray(va1.getVaccineAdministrationDTO().getArrivalTime())[0];
        int minute= VaccineAdministrationStore.StringIntoTimeArray(va1.getVaccineAdministrationDTO().getArrivalTime())[1];
        int hourError= VaccineAdministrationStore.StringIntoTimeArray(vaError.getVaccineAdministrationDTO().getArrivalTime())[0];
        int minuteError= VaccineAdministrationStore.StringIntoTimeArray(vaError.getVaccineAdministrationDTO().getArrivalTime())[1];

        assertEquals(11,hour);
        assertEquals(24,minute);
        assertEquals(25,hourError,"String not convertable to hour intenger");
        assertEquals(61,minuteError,"String not convertable to minute intenger");

    }

    @Test
    void createVaccineAdministration() {
        VaccineAdministration va = vaStore.createVaccineAdministration("161593121","Spikevax","Primeira","21C16-05","5/30/2022 8:00","5/30/2022 8:50","5/30/2022 9:10","5/30/2022 8:50");
        VaccineAdministration vaTest1 = vaStore.createVaccineAdministration("161593142","Spikevax","Primeira","21C16-05","5/30/2022 8:00","5/30/2022 8:50","5/30/2022 9:10","5/30/2022 8:50");
        assertEquals(va, va4);
        assertNotEquals(va, va1);
        assertNotEquals(va, vaTest1);
    }

    @Test
    void validateVaccineAdministration() {
        assertFalse(vaStore.validateVaccineAdministration(null));
        assertTrue(vaStore.validateVaccineAdministration(va2));
        assertFalse(vaStore.validateVaccineAdministration(va5));
    }

    @Test
    void addVaccinationAdministration() {
        assertTrue(vaStore.addVaccinationAdministration(va1));
        assertTrue(vaStore.addVaccinationAdministration(va2));
        assertFalse(vaStore.addVaccinationAdministration(va1));
        assertFalse(vaStore.addVaccinationAdministration(null));
    }

    @Test
    void getArrivalTimeBySNSNumber() {
        ArrivalTime aExpected = new ArrivalTime("987654231",vc1,new Date(),"14:50");
        ArrivalTime aNotExpected = new ArrivalTime("123232123",vc1,new Date(),"14:50");

        App.getInstance().getCompany().getArrivalTimeStore().getArrivaltimes().add(aExpected);
        ArrivalTime aResult = vaStore.getArrivalTimeBySNSNumber("987654231",vc1);
        ArrivalTime aNull = vaStore.getArrivalTimeBySNSNumber("123232123",vc1);

        assertEquals(aExpected,aResult);
        assertNotEquals(aResult,aNotExpected);
        assertNull(aNull);


    }

    @Test
    void getVaccinationScheduleBySNSNumber() {
        VaccinationSchedule vsExpected = new VaccinationSchedule("111111112", vc1, new Date(), "14:00", vaccineTest);
        App.getInstance().getCompany().getVaccinationScheduleStore().getVaccinationScheduleList().add(vsExpected);

        VaccinationSchedule vsNotExpected = new VaccinationSchedule("123132123", vc1, new Date(), "14:00", vaccineTest);

        VaccinationSchedule vsResult = vaStore.getVaccinationScheduleBySNSNumber("111111112", Utils.convertToSimpleDataFormat(new Date()));
        VaccinationSchedule vsResultNotExpected = vaStore.getVaccinationScheduleBySNSNumber("111111113", Utils.convertToSimpleDataFormat(new Date()));

        assertEquals(vsExpected,vsResult);
        assertNotEquals(vsResult,vsNotExpected);
        assertNull(vsResultNotExpected);
    }

    @Test
    void fillVaccineBox() {
        ArrayList<String> vaccineList = vaStore.fillVaccineBox(vaccineTest2);
        ArrayList<String> vaccineList2 = new ArrayList<>();
        vaccineList2.add(String.format("Vaccine Type: %s\nVaccine Designation: %s", "Hepatitis A","Pfizer"));
        assertArrayEquals(vaccineList.toArray(),vaccineList2.toArray());
    }

    @Test
    void getVaccinationCenterByName() {
        MassVaccinationCenter mvcExpected = new MassVaccinationCenter("CS RebordosaTeste", "Rua Cooperativa A CELER, 4585-846 Rebordosa", "245913278", "covidRebor@dgs.pt", "00:00", "22:00", "5", "60",vaccineTest);
        App.getInstance().getCompany().getVaccinationCenterStore().getVaccinationCentersList().add(mvcExpected);

        MassVaccinationCenter mvcNotExpected = new MassVaccinationCenter("CS Arcozelo", "Rua Cooperativa A CELER, 4585-846 Rebordosa", "245913278", "covidRebor@dgs.pt", "00:00", "22:00", "5", "60",vaccineTest);
        App.getInstance().getCompany().getVaccinationCenterStore().getVaccinationCentersList().add(mvcNotExpected);

        VaccinationCenter mcvResult = vaStore.getVaccinationCenterByName("CS RebordosaTeste");

        VaccinationCenter mcvNull = vaStore.getVaccinationCenterByName("");

        assertEquals(mcvResult,mvcExpected);
        assertNotEquals(mcvResult,mvcNotExpected);
        assertNull(mcvNull);
    }

    @Test
    void addSNSUserToChoiceBox() {
        VaccinationCenter vcTest = App.getInstance().getCompany().getVaccinationCenterStore().vaccinationCentersList.get(1);
        ArrayList<String> snsExpected = new ArrayList<>();
        snsExpected.add(String.format("Name: %s \nSNS User Number: %s", "Ze Silva", "111111111"));

        ArrayList<String> snsNotExpected = new ArrayList<>();
        snsNotExpected.add(String.format("Name: %s \nSNS User Number: %s", "Zes Silva", "111111111"));

        ArrayList<String> snsResult = vaStore.addSNSUserToChoiceBox(vcTest);

        assertArrayEquals(snsExpected.toArray(),snsResult.toArray());
        assertFalse(Arrays.equals(snsResult.toArray(),snsNotExpected.toArray()));
    }
}