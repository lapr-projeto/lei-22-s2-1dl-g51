package app.domain.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class VaccineTypeTest {
private VaccineType vaccineType;
    @BeforeEach
    void setUp() {
        vaccineType = new VaccineType(VaccineTypes.Covid_19, "88754", "Covid", "166");

    }

    @Test
    void getCode() {
        assertEquals("88754", vaccineType.getCode());
    }

    @Test
    void getDesignation() {
        assertEquals("Covid", vaccineType.getDesignation());
    }

    @Test
    void getWhoId() {
        assertEquals("166", vaccineType.getWhoId());
    }
}