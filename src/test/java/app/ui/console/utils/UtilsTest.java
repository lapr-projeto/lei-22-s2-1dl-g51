package app.ui.console.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class UtilsTest {

    private String phoneNumber1, phoneNumber2, phoneNumber3, phoneNumber4, citizenCardNumber1, citizenCardNumber2, citizenCardNumber3;
    private Date date1;


    @BeforeEach
    void setUp() {
        phoneNumber1 = "9232132";
        phoneNumber2 = "22232134312";
        phoneNumber3 = "22321323a";
        phoneNumber4 = "aaaa";
        citizenCardNumber1 = "31232";
        citizenCardNumber2 = "dsads";
        citizenCardNumber3 = "3123131393123123";
        date1 = new Date(122, 4, 6);
        date1.setHours(23);
        date1.setMinutes(49);
        date1.setSeconds(35);
    }

    @Test
     void validatePhoneNumber(){
        IllegalArgumentException ex1 = assertThrows(NumberFormatException.class, () -> Utils.validatePhoneNumber(phoneNumber1));
        IllegalArgumentException ex2 = assertThrows(NumberFormatException.class, () -> Utils.validatePhoneNumber(phoneNumber2));
        IllegalArgumentException ex3 = assertThrows(NumberFormatException.class, () -> Utils.validatePhoneNumber(phoneNumber3));
        IllegalArgumentException ex4 = assertThrows(NumberFormatException.class, () -> Utils.validatePhoneNumber(phoneNumber4));

        String expectedMessage1 = "Invalid Phone Number";

        String ex1msg = ex1.getMessage();
        String ex2msg = ex2.getMessage();
        String ex3msg = ex3.getMessage();
        String ex4msg = ex4.getMessage();

        assertTrue(ex1msg.contains(expectedMessage1));
        assertTrue(ex2msg.contains(expectedMessage1));
        assertTrue(ex3msg.contains(expectedMessage1));
        assertTrue(ex4msg.contains(expectedMessage1));

    }

    @Test
    void validatePortugueseCitizenCard() {
        IllegalArgumentException ex1 = assertThrows(IllegalArgumentException.class, () -> Utils.validatePortugueseCitizenCard(citizenCardNumber1));
        IllegalArgumentException ex2 = assertThrows(IllegalArgumentException.class, () -> Utils.validatePortugueseCitizenCard(citizenCardNumber2));
        IllegalArgumentException ex3 = assertThrows(IllegalArgumentException.class, () -> Utils.validatePortugueseCitizenCard(citizenCardNumber3));

        String expectedMessage = "Invalid Length";

        String ex1msg = ex1.getMessage();
        String ex2msg = ex2.getMessage();
        String ex3msg = ex3.getMessage();

        assertTrue(ex1msg.contains(expectedMessage));
        assertTrue(ex2msg.contains(expectedMessage));
        assertTrue(ex3msg.contains(expectedMessage));
    }

    @Test
    void convertToSimpleDataFormat() {
        String expectedDate = "06-05-2022";
        String nonExpectedDate1 = "2022-05-06";
        String nonExpectedDate2 = "06/05/2022";


        assertEquals(expectedDate, Utils.convertToSimpleDataFormat(date1));
        assertNotEquals(nonExpectedDate1, Utils.convertToSimpleDataFormat(date1));
        assertNotEquals(nonExpectedDate2, Utils.convertToSimpleDataFormat(date1));
    }

    @Test
    void convertFromDateToHours() {
        String expectedHour = "23:49:35";
        String nonExpectedHour1 = "00:00:00";
        String nonExpectedHour2 = "14:42:54";
        String nonExpectedHour3 = "23:49";

        assertEquals(expectedHour, Utils.convertFromDateToHours(date1));
        assertNotEquals(nonExpectedHour1, Utils.convertFromDateToHours(date1));
        assertNotEquals(nonExpectedHour2, Utils.convertFromDateToHours(date1));
        assertNotEquals(nonExpectedHour3, Utils.convertFromDateToHours(date1));
    }
}