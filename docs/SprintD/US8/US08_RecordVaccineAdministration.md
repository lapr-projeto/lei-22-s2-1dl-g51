# US 008 - Record Vaccine Administration as a Nurse

## 1. Requirements Engineering

### 1.1. User Story Description

As a nurse, I want to record the administration of a vaccine to a SNS user. 
At the end of the recovery period, the user should receive a SMS message informing the SNS user that he can leave the vaccination center.

### 1.2. Customer Specifications and Clarifications

**From the specifications document:**

>At any time, a nurse responsible for administering the vaccine will use the application to check the
list of SNS users that are present in the vaccination center to take the vaccine and will call one SNS
user to administer him/her the vaccine. Usually, the user that has arrived firstly will be the first one
to be vaccinated (like a FIFO queue). However, sometimes, due to operational issues, that might not
happen. The nurse checks the user info and health conditions in the system and in accordance with
the scheduled vaccine type, and the SNS user vaccination history, (s)he gets system instructions
regarding the vaccine to be administered (e.g.: vaccine and respective dosage considering the SNS
user age group). After giving the vaccine to the user, each nurse registers the event in the system,
more precisely, registers the vaccine type (e.g.: Covid-19), vaccine name/brand (e.g.: Astra Zeneca,
Moderna, Pfizer), and the lot number used. Afterwards, the nurse sends the user to a recovery room,
to stay there for a given recovery period (e.g.: 30 minutes). If there are no problems, after the given
recovery period, the user should leave the vaccination center. The system should be able to notify
(e.g.: SMS or email) the user that his/her recovery period has ended. If the nurse identifies any
adverse reactions during that recovery period, the nurse should record the adverse reactions in the
system.



**From the client clarifications:**

> **Question:** In US 08 says: "At the end of the recovery period, the user should receive a SMS message informing the SNS user that he can leave the vaccination center." How should the SNS user receive and have access to the SMS message?
>
> **Answer:** A file named SMS.txt should be used to receive/record the SMS messages. We will not use a real word service to send SMSs.


> **Question:** 1: Is the nurse responsible for registering in the system the recovery period? 2: If there are no adverse reactions detected/registered, after the given recovery period, the system notifies the user that his/her recovery period has ended, right? 3: If there are adverse reactions detected/registered, the system should not do anything additional?
>
> **Answer:**
> No. The recovery period starts automatically after registering the administration of a given vaccine.

> **Question:** "As we can read in Project Description, the vaccination flow follows these steps: 1. Nurse calls one user that is waiting in the waiting room to be vaccinated; Nurse checks the user's health data as well as which vaccine to administer; 3. Nurse administers the vaccine and registers its information in the system.
The doubt is: do you want US08 to cover steps 2 and 3, or just step 3?"
> **Answer:**
> 1.The nurse selects a SNS user from a list. 2. Checks user's Name, Age and Adverse Reactions registered in the system. 3. Registers information about the administered vaccine.

> **Question:** "To access the user info - scheduled vaccine type and vaccination history -, should the nurse enter user's SNS number?"
>
> **Answer:**
> The nurse should select a SNS user from a list of users that are in the center to take the vaccine.The nurse should select a vaccine and the administered dose number

### 1.3. Acceptance Criteria


* **AC1:** All required fields must be filled.
* **AC2:** The vaccination center of the vaccination schedule must be already registered in the System.
* **AC3:** The vaccine type of the vaccination schedule must be already registered in the System.
* **AC4:** The SNSUser must be already registered in the System.
* **AC5:** There must be a vaccine appointment for the administration day
* **AC6:** The user must be in the waiting list of the vaccination center
* **AC5:** The SNSUser receives a sms informing that the recovery period has ended. All the sms messages should be written to a file with the name SMSto(phoneNumber)
  .txt.

### 1.4. Found out Dependencies

**Depends on:**<p>
* **US1**: As a SNS user, I intend to use the application to schedule a vaccine.
* **US2**: As a receptionist at one vaccination center, I want to schedule a vaccination.
* **US3**: As a receptionist, I want to register a SNS user.
* **US4**: As a receptionist at a vaccination center, I want to register the arrival of a SNS user
  to take the vaccine.
* **US9**: As an administrator, I want to register a vaccination center to respond to a certain
  pandemic.
* **US12**: As an administrator, I intend to specify a new vaccine type.
* **US13**: As an administrator, I intend to specify a new vaccine and its administration
  process.

### 1.5 Input and Output Data

**Input Data:**

* Typed data:
  * Lot Number

* Selected data:
  * SNS User Number
  * Vaccine
  * Dose
  
**Output Data:**

* SMS with schedule appointment
* (In)Success of the operation

### 1.6. System Sequence Diagram (SSD)

![US008_SSD](US8_SSD.svg)

**Other alternatives might exist.**

### 1.7 Other Relevant Remarks

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US008_MD](US8_MD.svg)


## 3. Design - User Story Realization

### 3.1. Rationale

**SSD**

| Interaction ID | Question: Which class is responsible for...                      | Answer                          | Justification (with patterns)                                                                                                                                                                                                                                     |
|:---------------|:-----------------------------------------------------------------|:--------------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Step 1         | ... interacting with the actor?                                  | RegisterVaccineAdministrationUI | **Pure Fabrication:** there is no reason to assign this responsibility to any existing class in the Domain Model.                                                                                                                                                 |
| 	  		          | ... coordinating the US?                                         | RegisterVaccineController       | **Controller**                                                                                                                                                                                                                                                    |
| Step 2         | ...transfer the data typed and selected in the UI to the domain? | VaccineAdministrationDTO        | **DTO:** When there is so much data to transfer, it is better to opt by using a DTO in order to reduce coupling between UI and domain                                                                                                                             |
|                | ...saving the typed and selected data?                           | VaccineAdministration           | **IE:** a vaccine administration knows its own data                                                                                                                                                                                                               |                                                                  |                                |                                                                                                                   |
| 		             | ... instantiating a new VaccineAdministration?                   | VaccineAdministrationStore      | **Creator** and **HC+LC**: By the application of the Creator it would be the "Company". But, by applying HC + LC to the "Company", this delegates that responsibility to the "VaccineAdministrationStore"                                                         |
|                | ... knows VaccineAdministrationStore?                            | Company                         | **IE:** Company knows the VaccineAdministrationStore                                                                                                                                                                                                              |
| 	Step 3	       | ... validating all data (local validation)?                      | VaccineAdministration           | **IE:** an object knows its data                                                                                                                                                                                                                                  |
| 			  		        | ... validating all data (global validation)?                     | VaccineAdministrationStore      | **IE:** knows all the vaccination schedules data                                                                                                                                                                                                                  |
|                | ...saving the Vaccine Administration                             | VaccineAdministrationStore      | **IE:** knows all the vaccine administrations                                                                                                                                                                                                                     |
|                | ... sending SMS?                                                 | SMSNotificationSender           | **IE, Polymorphism and Protected Variations:** has all the required information and means to send the SMS (IE). However, to avoid code duplication (cf. US1, US2) this responsibility might be assign to a common and shared interface, specialized in this task. | 
|                | ... informing operation success?                                 | VaccinationConfirmationUI       | **IE:** is responsible for user interactions, when confirming the vaccine administration                                                                                                                                                                          |


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

* Company
* VaccineAdministration

Other software classes (i.e. Pure Fabrication) identified:

* RegisterVaccineAdministrationUI
* RegisterVaccineController
* VaccineAdministrationStore
* VaccineAdministrationDTO
* VaccinationConfirmationUI
* VaccinationConfirmationController

## 3.2. Sequence Diagram (SD)

![US008_SD](US8_SD.svg)

## 3.3. Class Diagram (CD)

![US008_CD](US8_CD.svg)

# 4. Tests

    @Test
    void getArrivalTimeBySNSNumber() {
        ArrivalTime aExpected = new ArrivalTime("987654231",vc1,new Date(),"14:50");
        ArrivalTime aNotExpected = new ArrivalTime("123232123",vc1,new Date(),"14:50");

        App.getInstance().getCompany().getArrivalTimeStore().getArrivaltimes().add(aExpected);
        ArrivalTime aResult = vaStore.getArrivalTimeBySNSNumber("987654231",vc1);
        ArrivalTime aNull = vaStore.getArrivalTimeBySNSNumber("123232123",vc1);

        assertEquals(aExpected,aResult);
        assertNotEquals(aResult,aNotExpected);
        assertNull(aNull);
    }

    @Test
    void getVaccinationScheduleBySNSNumber() {
        VaccinationSchedule vsExpected = new VaccinationSchedule("111111112", vc1, new Date(), "14:00", vaccineTest);
        App.getInstance().getCompany().getVaccinationScheduleStore().getVaccinationScheduleList().add(vsExpected);

        VaccinationSchedule vsNotExpected = new VaccinationSchedule("123132123", vc1, new Date(), "14:00", vaccineTest);

        VaccinationSchedule vsResult = vaStore.getVaccinationScheduleBySNSNumber("111111112", Utils.convertToSimpleDataFormat(new Date()));
        VaccinationSchedule vsResultNotExpected = vaStore.getVaccinationScheduleBySNSNumber("111111113", Utils.convertToSimpleDataFormat(new Date()));

        assertEquals(vsExpected,vsResult);
        assertNotEquals(vsResult,vsNotExpected);
        assertNull(vsResultNotExpected);
    }

# 5. Construction (Implementation)

## Class RegisterVaccineAdministrationController

    public class RegisterVaccineAdministrationController implements Serializable {


    private Company company;
    private VaccinationCenter vaccinationCenter;
    private ArrivalTime arrivalTime;
    private VaccinationSchedule vs;
    private VaccineAdministrationStore vaccineAdministrationStore;
    private static VaccineAdministration va;
    private String vaccinationCenterName = NurseController.getVaccinationCenterName();


    public void initialize() {
        this.company = App.getInstance().getCompany();
        this.vaccineAdministrationStore = company.getVaccineAdministrationStore();
        btnAdverseReactions.setVisible(false);
        lblScheduleTime.setVisible(false);
        lblArrivalTime.setVisible(false);

        vaccinationCenter = vaccineAdministrationStore.getVaccinationCenterByName(vaccinationCenterName);
        ObservableList<String> list = FXCollections.observableList(vaccineAdministrationStore.addSNSUserToChoiceBox(vaccinationCenter));
        cmbSNSUser.setItems(list);
        txtLot.setFocusTraversable(false);
    }

    @FXML
    public void fillVaccinationOptions(ActionEvent actionEvent) {
        btnAdverseReactions.setVisible(true);
        lblScheduleTime.setVisible(true);
        lblArrivalTime.setVisible(true);
        cmbSNSUser.setStyle("-fx-border-color: #029d02");
        if(cmbSNSUser.getValue() != null) {
            String snsUserNumber = cmbSNSUser.getValue().substring(cmbSNSUser.getValue().length() - 9);
            ShowUserDataController.setSnsUserNumber(snsUserNumber);
            arrivalTime = vaccineAdministrationStore.getArrivalTimeBySNSNumber(snsUserNumber, vaccinationCenter);
            String arrivedTime = String.format("User has arrived at %s, %s", Utils.convertFromDateToHours(arrivalTime.getArrivalTimeDTO().getArrivalTime()), Utils.convertToSimpleDataFormat(arrivalTime.getArrivalTimeDTO().getArrivalTime()));
            String scheduledTime = String.format("Vaccination Scheduled for %s, %s", arrivalTime.getArrivalTimeDTO().getScheduleHour(), Utils.convertToSimpleDataFormat(arrivalTime.getArrivalTimeDTO().getScheduleDate()));
            lblArrivalTime.setText(arrivedTime);
            lblScheduleTime.setText(scheduledTime);
            ObservableList<String> list = FXCollections.observableArrayList("First", "Second", "Third", "Fourth");
            cmbDose.setItems(list);
            vs = vaccineAdministrationStore.getVaccinationScheduleBySNSNumber(snsUserNumber, Utils.convertToSimpleDataFormat(arrivalTime.getArrivalTimeDTO().getScheduleDate()));
            VaccineType vt = vs.getScheduleDTO().getVaccineType();
            cmbVaccine.setItems(FXCollections.observableArrayList(vaccineAdministrationStore.fillVaccineBox(vt)));
        }

    }

    @FXML
    public void exit(ActionEvent actionEvent) {
        Stage stage = (Stage) btnExit.getScene().getWindow();
        stage.close();
    }

    @FXML
    public void createRecordVaccineAdministration(ActionEvent actionEvent){
        Stage stage = (Stage) ((Node) (actionEvent).getSource()).getScene().getWindow();
        stage.setAlwaysOnTop(false);
        if(vs != null) {
            VaccinationScheduleDTO vsDTO = vs.getScheduleDTO();
            String snsUserNumber = vsDTO.getSnsUserNumber();
            String vaccineName = vsDTO.getVaccineType().getVaccineTypeDTO().getDesignation();

            SimpleDateFormat formatOnlyDate = new SimpleDateFormat("MM/dd/yyyy");
            String scheduledTime = String.format("%s %s", formatOnlyDate.format(vsDTO.getDate()), vsDTO.getTime());
            SimpleDateFormat formatDateHours = new SimpleDateFormat("MM/dd/yyyy HH:mm");
            String arrivedTime = formatDateHours.format(arrivalTime.getArrivalTimeDTO().getArrivalTime());
            Calendar date = Calendar.getInstance();

            int recoveryPeriod = Integer.parseInt(App.getInstance().getProperties().getProperty("RECOVERY_PERIOD"));
            va = vaccineAdministrationStore.createVaccineAdministration(snsUserNumber, vaccineName, cmbDose.getValue(), txtLot.getText(), scheduledTime, arrivedTime, formatDateHours.format(new Date()), formatDateHours.format(new Date(date.getTimeInMillis() + ((long) recoveryPeriod * 60 * 1000))));

            ConfirmationWindowController.setVa(va);
        }

        boolean confirm = vaccineAdministrationStore.validateVaccineAdministration(va) && checkValidLotNumber(txtLot.getText());
        if (confirm) {
            ConfirmationWindowController.setPreviousStage(stage);
            new VaccinationConfirmationWindowGUI().run();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("All the requirements should be fulfilled");
            alert.showAndWait();
        }
    }

    private void validateLotNumber(String lotNumber) {
        if (!lotNumber.matches("(([A-Z]|\\d){5}-\\d{2})")) {
            throw new IllegalArgumentException("Incorrect Lot Number Format!");
        }
    }

    private boolean checkValidLotNumber(String lotNumber){
        try {
            validateLotNumber(lotNumber);
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;
    }
    @FXML
    public void checkValidatedLot(Event event) {
        try {
            validateLotNumber(txtLot.getText());
            txtLot.setStyle("-fx-border-color: #029d02");
        } catch (IllegalArgumentException e) {
            txtLot.setStyle("-fx-border-color: #c42525");
        }
    }

    @FXML
    public void turnDoseGreen(ActionEvent actionEvent) {
        cmbDose.setStyle("-fx-border-color: #029d02");
    }

    @FXML
    public void turnVaccineGreen(ActionEvent actionEvent) {
        cmbVaccine.setStyle("-fx-border-color: #029d02");
    }

    @FXML
    public void showUserData(ActionEvent actionEvent) {
        new ShowUserDataGUI().run();
    }


## Class RegisterVaccineAdministrationStore

    public VaccineAdministration createVaccineAdministration(String snsNumber, String vaccineName, String dose, String lotNumber, String scheduleDateTime, String arrivalTime, String nurseAdministrationDateTime, String leavingTime) {
        return new VaccineAdministration(snsNumber, vaccineName, dose, lotNumber, scheduleDateTime, arrivalTime, nurseAdministrationDateTime, leavingTime);
    }

    /**
     * Validate vaccine administration.
     *
     * @param va the va
     * @return the boolean
     */
    public boolean validateVaccineAdministration(VaccineAdministration va) {
        if (Objects.isNull(va))
            return false;
        else {
            VaccineAdministrationDTO vaDTO = va.getVaccineAdministrationDTO();
            if (vaDTO.getArrivalTime() == null || vaDTO.getLeavingTime() == null || vaDTO.getDose() == null || vaDTO.getSnsNumber() == null || vaDTO.getLotNumber() == null || vaDTO.getScheduleDateTime() == null || vaDTO.getNurseAdministrationDateTime() == null
                    || vaDTO.getArrivalTime().length() == 0 || vaDTO.getLeavingTime().length() == 0 || vaDTO.getDose().length() == 0 || vaDTO.getSnsNumber().length() == 0 || vaDTO.getLotNumber().length() == 0 || vaDTO.getScheduleDateTime().length() == 0 || vaDTO.getNurseAdministrationDateTime().length() == 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Add vaccination administration.
     *
     * @param va the va
     * @return the boolean
     */
    public boolean addVaccinationAdministration(VaccineAdministration va) {
        if (validateVaccineAdministration(va)) {
            if (!checkDuplicates(va)) {
                vaList.add(va);
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
    public VaccinationCenter getVaccinationCenterByName(String name) {
        for (VaccinationCenter vc : App.getInstance().getCompany().getVaccinationCenterStore().vaccinationCentersList) {
            if (name.equalsIgnoreCase(vc.getVaccinationCenterDTO().getName()))
                return vc;
        }
        return null;
    }

    @Override
    public void sendSMS(String snsNumber) {
        String userNumber = snsNumber;
        SNSUser snsUser = App.getInstance().getCompany().getUserStore().getSNSUserWithAGivenSNSUserNumber(userNumber);
        String phoneNumber = snsUser.getSNSUserDTO().getPhoneNumber();
        PrintWriter printWriter = null;

        try {
            printWriter = new PrintWriter(new File("LeavingSMSto" + phoneNumber + ".txt"));
            printWriter.print(("End of recovery period!\nYou can leave the center!\nHave a nice day!"));
            System.out.println("Leaving SMS has been sent successfully!");
        } catch (FileNotFoundException ex) {
            System.out.println("File Not Found");
        } finally {
            printWriter.close();
        }
    }

# 6. Integration and Demo

* A new option on the Nurse menu options was added.

* For some demo purposes some users are bootstrapped while system starts.

![demo_1](demo1.jpg)

![demo_2](demo2.jpg)

![demo_3](demo3.jpg)


# 7. Observations






