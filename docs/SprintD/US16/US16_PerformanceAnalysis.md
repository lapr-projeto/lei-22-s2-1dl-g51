# US 016 - Analyse the performance of a center as a Center Coordinator

## 1. Requirements Engineering

### 1.1. User Story Description

>As a center coordinator, I intend to analyze the performance of a center.
> 
### 1.2. Customer Specifications and Clarifications

**From the specifications document:**

>The goal of this US is to implement a procedure that, for a specific day, and time intervals of m minutes (for m = 30, 20, 10, 5, and l , for example) chosen by the coordinator of the center, with a daily work from 8 a.m. to 8 p.m., the procedure creates a list of length 720/m (respectively, lists of length 24, 36, 72, 144, 720), where the i-th value of the list is the difference between the number of new clients arriving and the number of clients leaving the center in that i-th time interval.
Then, the application should implement a brute-force algorithm (an algorithm which examines all the contiguous sublists of the input one) to determine the contiguous sublist with maximum sum. The output should be the input list, the maximum sum contiguous sublist and its sum, and the time interval corresponding to this contiguous sublist (for example, for time intervals of 1 hour, a list of length 12 is created; if, for example, the maximum sum contiguous sublist starts at the 2nd and ends at the 5th entries of the input list, with a sum s, it means that the vaccination center was less effective in responding from 9 a.m. to I p.m, with s clients inside the center).
The performance analysis should be documented in the application user manual (in the annexes) that must be delivered with the application. Also in the user manual, the implemented algorithm should be analyzed in terms of its worst-case time complexity. The complexity analysis must be accompanied by the observation of the execution time of the algorithms, and it should be compared to a benchmark algorithm provided, for inputs of variable size m, with m = 24, 36, 72, 144, 720, in order to observe the asymptotic behavior.




**From the client clarifications:**

> **Question:** The file loaded in US17 have only one day to analyse or it could have more than one day(?) and in US16 we need to select the day to analyse from 8:00 to 20:00
>
> **Answer:** The file can have data from more than one day. In US16 the center coordinator should select the day for which he wants to analyse the performance of the vaccination center.


> **Question:**  "Is the time of departure of an SNS user the time he got vaccinated plus the recovery time or do we have another way of knowing it?"
>
> **Answer:**
> The time of departure of an SNS user is the time he got vaccinated plus the recovery time.

> **Question:** "In US 16, should the coordinator have the option to choose which algorithm to run (e.g. via a configuration file or while running the application) in order to determine the goal sublist, or is the Benchmark Algorithm strictly for drawing comparisons with the Bruteforce one?"
>
> **Answer:**
>The algorithm to run should be defined in a configuration file.

### 1.3. Acceptance Criteria


* **AC1:** Must be a valid .csv file.


### 1.4. Found out Dependencies

N/A

### 1.5 Input and Output Data

**Input Data:**

* Typed data:
  * Time Interval (min)

* Selected data:
  * Date
  * File
  * Algorithm

  
**Output Data:**

* Input list
* Max Contiguous Sum List
* Max Sum
* Time interval of Max Contiguous Sum List


### 1.6. System Sequence Diagram (SSD)

![US016_SSD](US16_SSD.svg)

**Other alternatives might exist.**

### 1.7 Other Relevant Remarks

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US016_MD](US16_MD.svg)


## 3. Design - User Story Realization

### 3.1. Rationale

**SSD**

| Interaction ID        | Question: Which class is responsible for...                      | Answer                        | Justification (with patterns)                                                                                                                                                                           |
|:----------------------|:-----------------------------------------------------------------|:------------------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Step 1                | ... interacting with the actor?                                  | PerformanceAnalysisUI         | **Pure Fabrication:** there is no reason to assign this responsibility to any existing class in the Domain Model.                                                                                       |
| 	  		                 | ... coordinating the US?                                         | PerformanceAnalysisController | **Controller**                                                                                                                                                                                          |
| Step 2                | ...saving the typed and selected data?                           | PerformanceData               | **IE:** a performance data knows its own data                                                                                                                                                           |                                                                  |                                |                                                                                                                   |
| 		                    | ... instantiating a new PerformanceData?                         | PerformanceAnalysisStore      | **Creator** and **HC+LC**: By the application of the Creator it would be the "Company". But, by applying HC + LC to the "Company", this delegates that responsibility to the "PerformanceAnalysisStore" |
|                       | ... knows PerformanceAnalysisStore?                              | Company                       | **IE:** Company knows the PerformanceAnalysisStore                                                                                                                                                      |
| 	Step 3	              | ... validating all data (local validation)?                      | PerformanceData               | **IE:** an object knows its data                                                                                                                                                                        |
| 			  		               | ... validating all data (global validation)?                     | PerformanceAnalysisStore      | **IE:** knows all the performance analysis data                                                                                                                                                         |
|                       | ...saving the PerformanceAnalysis                                | PerformanceAnalysisStore      | **IE:** knows all the performance analysis data                                                                                                                                                         |


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

* Company
* PerformanceData

Other software classes (i.e. Pure Fabrication) identified:

* PerformanceAnalysisUI
* PerformanceAnalysisController
* PerformanceAnalysisStore
* PerformanceListPopUpController
* PerformancePopUpGUI

## 3.2. Sequence Diagram (SD)

![US016_SD](US16_SD.svg)

## 3.3. Class Diagram (CD)

![US016_CD](US16_CD.svg)

# 4. Tests

 Not implemented

# 5. Construction (Implementation)

## Class PerformanceAnalysisController

    public class PerformanceAnalysisController implements Serializable {

    @javafx.fxml.FXML
    private Label lblFileName;
    @javafx.fxml.FXML
    private DatePicker dateChooser;
    @javafx.fxml.FXML
    private TextField lblTime;
    @javafx.fxml.FXML
    private Button btnBack;

    private static List<Integer> performance;

    private static List<Integer> contList;

    private static int maxSumContList;
    @javafx.fxml.FXML
    private ComboBox cmbAlgorithm;
    @javafx.fxml.FXML
    private Button btnChooseFile;
    @javafx.fxml.FXML
    private Button btnAnalyseFile;

    public void initialize() {
        ObservableList<String> list = FXCollections.observableArrayList("Brute-Force Algorithm", "Benchmark Algorithm");
        cmbAlgorithm.setItems(list);
        lblFileName.getStyleClass().add("labelBlack");
    }

    @javafx.fxml.FXML
    public void openFile(ActionEvent actionEvent) {

        Stage stage = (Stage) ((Node) (actionEvent).getSource()).getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("CSV Files", "*.csv")
        );
        File file = fileChooser.showOpenDialog(stage);
        lblFileName.setText(file.getName());
        lblFileName.getStyleClass().add("labelGreen");

        try {
            PerformanceAnalysisStore.validateFile(file);
            PerformanceAnalysisStore.dataList(file);
        } catch (InvalidObjectException e) {
            lblFileName.getStyleClass().add("labelRed");
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(e.getMessage());
            alert.showAndWait();
        }

    }


    @javafx.fxml.FXML
    public void analyseBtnAction(ActionEvent actionEvent) {


        if (dateChooser.getValue() != null && lblTime != null && cmbAlgorithm.getValue() != null && lblTime.getText().length() != 0) {

            int inputInterval = Integer.parseInt(lblTime.getText());
            LocalDate inputDate = dateChooser.getValue();
           // inputDate.format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
            List<PerformanceData> data = PerformanceAnalysisStore.getPerformanceList();


                try {
                    performance = App.getInstance().getCompany().getPerformanceAnalysisStore().analyse(inputInterval, inputDate, data);
                    if (cmbAlgorithm.getValue().toString().equalsIgnoreCase("Brute-Force Algorithm")) {
                        contList = PerformanceAnalysisStore.contiguousSublist(performance);
                    }
                    if (cmbAlgorithm.getValue().toString().equalsIgnoreCase("Benchmark Algorithm")) {
                        Integer[] array = Arrays.copyOfRange(performance.toArray(), 0, performance.size(), Integer[].class);
                        int[] primitiveArray = Utils.toint(array);
                        int[] benchMarkArray = Sum.Max(primitiveArray);

                        Integer[] arraySum = Utils.toConvertInteger(benchMarkArray);
                        contList = Arrays.asList(arraySum);
                    }
                    maxSumContList = PerformanceAnalysisStore.maxSumContiguousSublist(contList);
                    new PerfomancePopUpGUI().run();


                } catch (InvalidObjectException e) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setContentText(e.getMessage());
                    alert.showAndWait();
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Please fill all the requirements!");
                alert.showAndWait();
            }


    }

    public static List<Integer> getPerformance() {
        return performance;
    }

    public static List<Integer> getContList() {
        return contList;
    }

    public static int getMaxSumContList() {
        return maxSumContList;
    }

    @javafx.fxml.FXML
    public void backAction(ActionEvent actionEvent) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        stage.close();
        new CenterCoordinatorGUI().run();
    }

    @javafx.fxml.FXML
    public void getAnalyseDate(ActionEvent actionEvent) {
    }
}


## Class PerformanceAnalysisStore

    public class PerformanceAnalysisStore implements Serializable {

    static int start = 0;
    static int end = 0;

    private static ArrayList<PerformanceData> performanceList = new ArrayList<>();

    private ArrayList<PerformanceData> intervalTime = new ArrayList<>();


    public static boolean validateFile(File file) throws InvalidObjectException {
        Scanner readFile = null;
        try {
            readFile = new Scanner(file, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String firstLine = readFile.nextLine();
        if (firstLine.contains(";")) {
            return true;
        }else {throw new InvalidObjectException("Invalid File");
        }
    }

    public static void dataList(File file) {
        Scanner readFile = null;
        try {
            readFile = new Scanner(file);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        readFile.nextLine();
        while (readFile.hasNextLine()) {
            String[] performanceInfo = readFile.nextLine().split(";");
           // SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
            LocalDateTime arrivalTime = null;
            LocalDateTime leavingTime= null;

            arrivalTime = LocalDateTime.parse(performanceInfo[5], DateTimeFormatter.ofPattern("M/dd/yyyy H:mm"));
            leavingTime = LocalDateTime.parse(performanceInfo[7], DateTimeFormatter.ofPattern("M/dd/yyyy H:mm"));
            PerformanceData performanceData = new PerformanceData(arrivalTime, leavingTime);
            performanceList.add(performanceData);

        }

    }

        public static List<Integer> contiguousSublist(List <Integer> array){
            List <Integer> sublistMaxSum = new ArrayList<>();

            if (array == null || array.size() == 0) {
                return null;
            }
            int maxSum = array.get(0);

            for (int i = 0; i < array.size(); i++) {
                int sum = 0;

                for (int j = i; j < array.size(); j++) {
                    sum += array.get(j);
                    if (maxSum < sum) {
                        maxSum = sum;
                        start = i;
                        end = j;
                    }
                }
            }

            for (int k = start; k < end + 1; k++) {
                sublistMaxSum.add(array.get(k));
            }

            return sublistMaxSum;
        }


        public static int maxSumContiguousSublist (List <Integer> subList) {
            int sum = 0;
            for (int i = 0; i < subList.size(); i++) {
                sum = sum + subList.get(i);
            }
            return sum;
        }


    public static int getInterval(int a) throws InvalidObjectException {
        if ( 720%a == 0){
            return 720/a;
        }
        else {
            throw new InvalidObjectException("Invalid interval of time");
        }
    }
    public List<Integer> analyse(int interval, LocalDate date, List<PerformanceData> data) throws InvalidObjectException {
        List<Integer> diffList = new ArrayList<>();

        int listLength = getInterval(interval);


        LocalDateTime currentInterval = LocalDateTime.of(LocalDate.of(date.getYear(), date.getMonthValue(), date.getDayOfMonth()), LocalTime.of(8,0,0));

        intervalTime = new ArrayList<>();

        for (int i = 0; i < listLength; i++) {
            int entered = 0;
            int left = 0;
            for (PerformanceData presenceTime: data){
                if(presenceTime.getArrivalDate().isAfter(ChronoLocalDateTime.from(currentInterval.minusMinutes(1))) && presenceTime.getArrivalDate().isBefore(ChronoLocalDateTime.from(currentInterval.plusMinutes(interval)))){
                    entered ++;
                }

                if(presenceTime.getLeavingDate().isAfter(ChronoLocalDateTime.from(currentInterval.minusMinutes(1))) && presenceTime.getLeavingDate().isBefore(ChronoLocalDateTime.from(currentInterval.plusMinutes(interval)))){
                    left ++;
                }
            }

            diffList.add(entered-left);
            PerformanceData definedInterval = new PerformanceData(currentInterval, currentInterval.plusMinutes(interval));
            intervalTime.add(definedInterval);
            currentInterval = currentInterval.plusMinutes(interval);


        }

        return diffList;
    }

    public ArrayList<PerformanceData> getIntervalTime() {return intervalTime;}

    public static ArrayList<PerformanceData> getPerformanceList() {
        return performanceList;
    }

    public static int getEnd() {
        return end;
    }

    public static int getStart() {
        return start;
    }
    }







# 6. Integration and Demo

* A new option on the Center Coordinator menu options was added.

* For some demo purposes some users are bootstrapped while system starts.



# 7. Observations






