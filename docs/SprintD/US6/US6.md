# US 6 - DGS wants to record daily the total number of people vaccinated in each vaccination center

## 1. Requirements Engineering

*The system must set a time to start a daily loop. Once the loop start time is set, 
the system must set a time interval for the loop to restart, 
at the end of each loop the system records in a CSV file the number of people vaccinated in each vaccination centre and the date of that loop. *

### 1.1. User Story Description

*  DGS wants to record daily the total number of people vaccinated in each vaccination center *

### 1.2. Customer Specifications and Clarifications

*From the Specifications Document:*
• : The algorithm should run automatically at a time defined in a configuration file and should register the date, 
the name of the vaccination center and the total number of vaccinated users.

*From the client clarifications:*

*Cliente Clarification #1*
Question: I'd like to clarify something, should we implement some sort of message for when the file is saved or warning if there were any errors saving the file using JavaFX or use JavaFX in any other away in US06?
Also, should the time be edited directly in the configuration file or should we have a way for an administrator or some other employee to change it.

Answer: 1- No. The user story runs automatically without user interaction.
        2- I already answered this question. The algorithm should run automatically at a time defined in a configuration file and should register the date,
        the name of the vaccination center and the total number of vaccinated users.

*Cliente Clarification #2*
Question: "I'd like to clarify something you mentioned in the last reunion and make it so that every student has access to your answer.

After asking you to be more clear with your answer to my previous questions, you said that we can either access directly the "vaccinations report" file or create the option for center coordinators and administrators to view it's content, is this correct?

Answer: The file should be available in the file system and anyone has access to the file system can read the file contents

*Cliente Clarification #3*

Question: "Should the Company choose first the vaccination center that wants to analyze or should the program show the information of all the vaccination centers?."

Answer:The application should show the information for all vaccination centers.

*Cliente Clarification #4*

Question: "In this situation, the system automatically prints what is requested so we dont need any user input.
So, my question is: Do we need to create an UI for this US? ."

Answer: No.


### 1.3. Acceptance Criteria

*AC1: The algorithm should run automatically at a time defined in a configuration file and should register the date, the name of the vaccination center and the total number of vaccinated users.*
*AC2: The Vaccination Administration must coincide with the data of the loop.*
*AC3: The system must verify each vaccination center*



### 1.4. Found out Dependencies
*No dependencies.*
### 1.5 Input and Output Data
*Input Data*

• (none)

*Output Data*

• (In)Success of the operation 

*Record in CSV file the number of people vaccinated at each vaccination center at the end of the loop*

### 1.6. System Sequence Diagram (SSD)

![US6-SSD](US6_SSD.svg)

### 1.7 Other Relevant Remarks
None.
## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US6_DM](US6_DM.svg)

### 2.2. Other Remarks
None.
## 3. Design - User Story Realization
### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1  		 |... starting the program?                                                  |none                           |
|                |... knowing the user using the system                                      |AuthFacade                     | Information Expert
|                |... Coordinating the us?                                                   |RecordDaily                    | Controller direct communication with Store and RecordDailyFile
|                |... returning the time and filePath                                        |Company                        | Information Expert : Configuration.properties has a time defined 
|                |... returning the VaccineAdministrationStore?                              |Company                        | Information Expert: owns the stores
|                |... returning the receptionist's center?                                   |VaccinationAdministrationStore | PureFabrication: in order to promote reune and atteng high cohesion and Low Coupling patterns, the VaccineAdministrationStore exists to be responsible for saving and returning the vaccine administrations.
|         		 |... owning the vaccine administrations?     						         |VaccinationAdministrationStore | PureFabrication :to promote reune and attend high cohesion and Low Coupling patterns, the CenterStore exists to be responsible for saving and returning the centers.
|        		 |... counting the vaccine administrations?                                  |RecordDaily                    | IE : responsible for counting the administrations because they are saved in the store
|                |... writing the data required in the CSV file?                             |RecordDaily                    | IE : responsible for writing the number of vaccinations at each vaccination center 
| Step 2         |... generating report with the total number of people vaccinated in each vaccination center?                |RecordDaily                    | IE:  responsible for record in the CSV file all the data required  
                                                         
 




### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

* Company
* VaccineAdministration

Other software classes (i.e. Pure Fabrication) identified:

* VaccinationAdministrationStore
* RecordDaily
* RegisterVaccineAdministrationUI
* RegisterVaccineController
* VaccineAdministrationStore
* VaccineAdministrationDTO





## 3.2. Sequence Diagram (SD)

*In this section, it is suggested to present an UML dynamic view stating the sequence of domain related software objects' interactions that allows to fulfill the requirement.*

![US6-SD](US6_SD.svg)

## 3.3. Class Diagram (CD)

*In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods.*

![US6-CD](US6_CD.svg)

# 4. Test
**Test 1:**
        @Test
        @DisplayName(
        "Should return the number of administrations when the data and vaccinationCenter are correct")
        public void testGetNumberOfAdministrationsWhenDataAndVaccinationCenterAreCorrect() {
        String data = "2020-05-01";
        String vaccinationCenter = "Vaccination Center 1";

        int numberOfAdministrations =
                RecordDailyStore.getNumberOfAdministrations(data, vaccinationCenter);

        assertEquals(0, numberOfAdministrations);
    }
**Test 2 :** 
    @Test
    @DisplayName("Should return 0 when the data and vaccinationCenter are incorrect")
    public void testGetNumberOfAdministrationsWhenDataAndVaccinationCenterAreIncorrect() {
        String data = "2020-05-01";
        String vaccinationCenter = "Vaccination Center 1";

        int numberOfAdministrations =
                RecordDailyStore.getNumberOfAdministrations(data, vaccinationCenter);

        assertEquals(0, numberOfAdministrations);
    }

	

# 5. Construction (Implementation)
none.

# 6. Integration and Demo

none.

# 7. Observations

*No observations*