# US 007 - Record adverse reactions of a SNS User

## 1. Requirements Engineering

### 1.1. User Story Description

*As a nurse, I intend to record adverse reactions of a SNS user.*

### 1.2. Customer Specifications and Clarifications 

>    If the nurse identifies any adverse reactions during that recovery period, the nurse should record the adverse reactions in the system.

>    The nurse must enter the user's SNS number and must describe the adverse reactions.

>    The adverse reactions should only be recorded on the program. The text that the nurse writes must have a maximum of 300 characters*

### 1.3. Acceptance Criteria

**Not Provided**

### 1.4. Found out Dependencies

**US13:** *As an administrator, I intend to specify a new vaccine and its administration process.*

### 1.5 Input and Output Data

**Input Data:**

* Typed data:
    * a technical description of adverse reactions
    * SNS User data


* Selected data:
    * Classifying task category

**Output Data:**

* List of existing task categories
* (In)Success of the operation


### 1.6. System Sequence Diagram (SSD)

![US007-SSD](US007_SSD.svg)


### 1.7 Other Relevant Remarks

**Not Provided**


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 

![US007-MD](US007_DM.svg)

### 2.2. Other Remarks

**Not Provided** 


## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1  		 |	... interacting with the actor?	|    AdverseReactionUI | **Pure Fabrication:** there is no reason to assign this responsibility to any existing class in the Domain Model. | 
|  		 |... coordinating the US?	|  AdverseReactionController           | **Controller**                             |
| Step 2 		 |...saving the typed and selected data?	| AdverseReaction            |  **IE:** class knows its own data                            |
|  		 |... validate SNS User data	| SNSUser            |  **IE:** class knows its own data  |
| Step 3  		 |... store adverse reaction|Adverse reaction |                              |
| Step 4  		 |... informing operation success?	| AdverseReactionUI |**IE:** is responsible for user interactions                              |              



### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * Company
 * AdverseReaction

Other software classes (i.e. Pure Fabrication) identified: 
 * AdverseReactionUI  
 * AdverseReactionController

## 3.2. Sequence Diagram (SD)

![US007-SD](C:\Users\ASUS\IdeaProjects\lei-22-s2-1dl-g51\docs\SprintD\US7\US007_SD.svg)

## 3.3. Class Diagram (CD)

![CD_US7](US007_CD.svg)

# 4. Tests 
*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling.* 

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**Test 1:** Check that it is not possible to create an instance of the Example class with null values. 

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

*It is also recommended to organize this content by subsections.* 

# 5. Construction (Implementation)

## Adverse Reaction DTO

    public class AdverseReactionsDTO implements Serializable {

    private String snsUserNumber;

    private String adverseReactions;

    public AdverseReactionsDTO(String snsUserNumber, String adverseReactions) {
        this.snsUserNumber = snsUserNumber;
        this.adverseReactions = adverseReactions;
    }

    public String getSnsUserNumber() {
        return snsUserNumber;
    }

    public String getAdverseReactions() {
        return adverseReactions;
    }

    public void setAdverseReactions(String adverseReactions) {
        this.adverseReactions = adverseReactions;
    }
    }

## AdverseReaction.java

    public class AdverseReactions implements Serializable {

    private String snsUserNumber;
    private String adverseReactions;
    private AdverseReactionsDTO adverseReactionsDTO;

    public AdverseReactions(String snsUserNumber, String adverseReactions) {
        this.snsUserNumber = snsUserNumber;
        this.adverseReactions = adverseReactions;
        this.adverseReactionsDTO = new AdverseReactionsDTO(snsUserNumber,adverseReactions);
    }

    public AdverseReactionsDTO getAdverseReactionsDTO() {
        return adverseReactionsDTO;
    }

    public void setSnsUserNumber(String snsUserNumber) {
        this.snsUserNumber = snsUserNumber;
    }

    public void setAdverseReactions(String adverseReactions) {
        this.adverseReactions = adverseReactions;
        this.adverseReactionsDTO.setAdverseReactions(adverseReactions);
    }
    }

## Record Adverse Reactions GUI

    public class RecordAdverseReactionsGUI extends Application implements Runnable, Serializable {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("RecordAdverseReactions.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("Record Adverse Reactions");
        stage.getIcons().add(new Image("images/DGS_logo.png"));
        stage.setScene(scene);
        stage.getScene().getStylesheets().add("Styles.css");
        stage.show();
        stage.setAlwaysOnTop(true);
    }

    @Override
    public void run() {
        try {
            start(new Stage());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    }

## Record Adverse Reactions Controller 

     public class RecordAdverseReactionsController implements Serializable {
     @javafx.fxml.FXML
     private TextField search;
     @javafx.fxml.FXML
     private ListView list;
     @javafx.fxml.FXML
     private Button btnBack;
     @javafx.fxml.FXML
     private Button btnSave;
     @javafx.fxml.FXML
     private Button btnUserData;
     @javafx.fxml.FXML
     private TextArea txtAdverseReactions;


    public void initialize(){
        btnSave.setVisible(false);
        btnUserData.setVisible(false);
        txtAdverseReactions.setStyle("-fx-border-width: 1 1 1 1; -fx-border-radius: 2%");
        search.setFocusTraversable(false);
        list.setVisible(false);
        txtAdverseReactions.setVisible(false);

       /* LoadCSVFileController ctrl = new LoadCSVFileController();
        ctrl.createSNSUsers(new File("CSVFilesWithUsers/SNSUserDataFromGaoFuNationalCenterDoPortoVaccinationCenter.csv"));
        */
        ArrayList<String> snsNumbers = new ArrayList<>();
        for (SNSUser sns : App.getInstance().getCompany().getUserStore().getSnsUserList()) {
            snsNumbers.add(sns.getSNSUserDTO().getSnsUserNumber());
        }

        ObservableList<String> list1 = FXCollections.observableArrayList(snsNumbers);

        FilteredList<String> filteredData = new FilteredList<>(list1, s -> true);
        list.setItems(filteredData);
        search.textProperty().addListener(obs->{
            String filter = search.getText();
            if(filter == null || filter.length() == 0) {
                filteredData.setPredicate(s -> true);
            }
            else {
                filteredData.setPredicate(s -> s.startsWith(filter));
            }
        });


    }

    @javafx.fxml.FXML
    public void showList(Event event) {
        list.setVisible(true);
        txtAdverseReactions.setVisible(false);
        btnSave.setVisible(false);
        btnUserData.setVisible(false);
    }

    @javafx.fxml.FXML
    public void getSNSNumber(Event event) {
        search.setText(String.valueOf(list.getSelectionModel().getSelectedItem()));
        list.setVisible(false);
        txtAdverseReactions.setVisible(true);
        btnSave.setVisible(true);
        btnUserData.setVisible(true);
    }

    @javafx.fxml.FXML
    public void save(ActionEvent actionEvent) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        stage.setAlwaysOnTop(false);
        App.getInstance().getCompany().getAdverseReactionsStore().setAdverseReactions(search.getText(),txtAdverseReactions.getText());
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setContentText("Adverse Reactions Saved Successfully!");
        alert.showAndWait();
        txtAdverseReactions.setVisible(false);
        btnSave.setVisible(false);
        btnUserData.setVisible(false);
        search.clear();
    }

    @javafx.fxml.FXML
    public void back(ActionEvent actionEvent) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        stage.close();
    }

    @javafx.fxml.FXML
    public void showUserData(ActionEvent actionEvent) {
        ShowUserDataController.setSnsUserNumber(search.getText());
        new ShowUserDataGUI().run();
    }
    }


# 6. Integration and Demo 

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*


# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*





