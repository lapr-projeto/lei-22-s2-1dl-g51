# OO Analysis #

The construction process of the domain model is based on the client specifications, especially the nouns (for _concepts_) and verbs (for _relations_) used. 

## Rationale to identify domain conceptual classes ##
To identify domain conceptual classes, start by making a list of candidate conceptual classes inspired by the list of categories suggested in the book "Applying UML and Patterns: An Introduction to Object-Oriented Analysis and Design and Iterative Development". 


### _Conceptual Class Category List_ ###

<li>Company
<li>Vaccination Center
<li>Vaccine
<li>Vaccination Schedule
<li>Vaccine Administration
<li>Healthcare Center
<li>Community Mass Vaccination Center
<li>Vaccine Type
<li>SNS User
<li>SNS User Adverse Reaction
<li>Vaccination Certificate
<li>Nurse
<li>Administrator
<li>Receptionist
<li>Center Coordinator
<li>Employee
<li>Performance Report
<li>VaccinationProcessStatistic
</li>


**Business Transactions**

* HealthCare Center and Vaccination Center administers Vaccine Type
* Nurse generates Vaccination Certificate


**Transaction Line Items**

* Performance Report

**Product/Service related to a Transaction or Transaction Line Item**

* EU Covid Digital certificate
* Vaccine

**Transaction Records**

*  

**Roles of People or Organizations**

* Nurse checks the SNS User information and proceeds to generate the Vaccination Certificate
* Receptionist sets arrival
* Center Coordinator possesses Center
* SNS User schedules Vaccination Schedule and obtains Vaccination Certificate

**Places**

* Vaccination Center
* HealthCare Center
---

**Noteworthy Events**

* SNS User schedules Vaccination Schedule
* HealthCare Center and Vaccination Center administers the Vaccine Type
* Receptionist sets arrival of Vaccination Schedule
* Nurse generates Vaccination Certificate
* Center Coordinator generates Report

**Physical Objects**


**Descriptions of Things**
* Vaccine
* Vaccination Certificate

**Catalogs**

* Performance Report

**Containers**

* Vaccination Schedule holds information of SNS User

**Elements of Containers**

*  

**Organizations**

* ARS
* AGES
* DGS
* SNS

**Other External/Collaborating Systems**

*  Notification


---


**Records of finance, work, contracts, legal matters**

* Report
* Vaccination Certificate

**Financial Instruments**
*
**Documents mentioned/used to perform some work/**
*
###**Rationale to identify associations between conceptual classes**

| Concept (A)                    | Association                                                 | Concept (B)                                                                                                        |
|--------------------------------|:------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|
| Company                        | <ul><li>manages<li>issues<li>applies                        | <ul><li>VaccinationCenter<li>VaccinationCertificate<li>Vaccine                                                     |
| VaccinationCenter              | <ul><li>has<li>has                                          | <ul><li>VaccinationProcessStatistic<li>PerformanceReport                                                           |
| Vaccine                        | <ul><li>is of                                               | <ul><li>VaccineType                                                                                                |
| VaccineSchedule                | <ul><li>to take                                             | <ul><li>VaccineType                                                                                                |
| VaccineAdministration          | <ul><li>of<li>fulfilling<li>administered on                 | <ul><li>Vaccine<li>VaccineSchedule<li>SNSUser                                                                      |
| HealthCareCenter               | <ul><li>is a<li>administers                                 | <ul><li>VaccinationCenter<li>VaccineType                                                                           |
| CommunityMassVaccinationCenter | <ul><li>is a<li>administers                                 | <ul><li>VaccinationCenter<li>VaccineType                                                                           |
| SNSUser                        | <ul><li>creates<li>can request<li>can have                  | <ul><li>VaccineSchedule<li>VaccinationCertificate<li>SNSUserAdverseReaction                                        |
| Administrator                  | <ul><li>specifies<li>works for<li>registers<li>registers    | <ul><li>VaccineType<li>Company<li>VaccinationCenter<li>Employee                                                    |
| CenterCoordinator              | <ul><li>is a<li>coordinates<li>analyses<li>generates        | <ul><li>Employee<li>VaccinationCenter<li>VaccinationProcessStatistic<li>PerformanceReport                          |
| Nurse                          | <ul><li>is a<li>works for<li>reports<li>can deliver<li>does | <ul><li>Employee<li>VaccinationCenter<li>SNSUserAdverseReaction<li>VaccinationCertificate<li>VaccineAdministration |
| Receptionist                   | <ul><li>is a<li>works for<li>validates<li>can schedule      | <ul><li>Employee<li>VaccinationCenter<li>**VaccinationSchedule**<li>VaccineSchedule                                    |

## Domain Model


![DM.svg](Images/DM.svg)



