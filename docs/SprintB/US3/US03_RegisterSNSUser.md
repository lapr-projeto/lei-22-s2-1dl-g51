# US 003 - Register SNS User

## 1. Requirements Engineering


### 1.1. User Story Description


As a receptionist, I want to register a SNS User


### 1.2. Customer Specifications and Clarifications 


**From the specifications document:**

>	When the SNS user arrives at the vaccination center, a receptionist registers the arrival of the user to
take the respective vaccine.


>	As long as it is not published, access to the SNS User is exclusive to the employees of the respective organization. 



**From the client clarifications:**

> **Question:** What are the necessary components in order to register an SNS User?
>  
> **Answer:** The attributes that should be used to describe a SNS user are: Name, Address, Sex, Phone Number, E-mail, Birth Date, SNS User Number and Citizen Card Number.
The Sex attribute is optional. All other fields are required.

-

> **Question:** For the SNS user account, is a username necessary, or does the SNS number function as a username?
>  
> **Answer:** 
>Login: is the user e-mail;
>Password: the password should be randomly generated. In the project description we get “All those who wish to use the application must be authenticated with a password holding seven alphanumeric characters, including three capital letters and two digits.”



### 1.3. Acceptance Criteria


* **AC1:** All required fiels must be filled in but gender.
* **AC2:** The password must have seven alphanumeric characters, including three capital letters and two digits.
* **AC3:** When a new SNS User with an already existing reference, the system must reject such operation and the user must have the change to modify the typed reference.


### 1.4. Found out Dependencies


### 1.5 Input and Output Data


**Input Data:**

* Typed data:
    * name
    * address
    * phone number
    * email
    * citizen card number
    * gender
    * birthdate
    * sns user number

**Output Data:**

* Email and password for the login of the SNS User
* (In)Success of the operation

### 1.6. System Sequence Diagram (SSD)


![US003_SSD](US3_SSD.svg)


**Other alternatives might exist.**

### 1.7 Other Relevant Remarks

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 

![US003_MD](US3_MD.svg)

### 2.2. Other Remarks

n/a


## 3. Design - User Story Realization 

### 3.1. Rationale

**SSD**

| Interaction ID | Question: Which class is responsible for...        | Answer                    | Justification (with patterns)                                                                                 |
|:---------------|:---------------------------------------------------|:--------------------------|:--------------------------------------------------------------------------------------------------------------|
| Step 1  		     | 	... interacting with the actor?                   | SNSUserRegisterUI         | Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model. |
| 			  		        | 	... coordinating the US?                          | RegisterSNSUserController | Controller                                                                                                    |
| 			  		        | 	... instantiating a new SNS User?                 | SNSUser                   | in the DM, Company knows a user                                                                               |
| 			  		        | ... knowing the user using the system?             | Company                   | IE: knows/has all the users already registered                                                                |
| 			  		        | 	... knowing to which company the user belongs to? | Company                   | IE: has registed all the users                                                                                |
| 			  		        | 							                                            | Company                   | IE: knows/has its own users                                                                                   |
| 			  		        | 							                                            | SNS User                  | IE: knows its own data (e.g. email)                                                                           |
| Step 2  		     | 	...saving the inputted data?                      | SNS User                  | IE: object created in step 1 has its own data.                                                                |
| Step 3  		     | 	...knowing the roles to show?                     | Company                   | IE: User roles are defined by the Company.                                                                    |
| Step 4  		     | 	... saving the selected role?                     | SNS User                  | IE: object created in step 1 is classified in one role.                                                       |
| Step 5  		     | 	... validating all data (local validation)?       | SNS User                  | IE: owns its data.                                                                                            | 
| 			  		        | 	... validating all data (global validation)?      | SNSUserStore              | IE: knows all its tasks.                                                                                      | 
| 			  		        | 	... saving the created user?                      | RegisterSNSUserController | IE: owns all its tasks.                                                                                       | 
| Step 6  		     | 	... informing operation success?                  | SNSUserRegisterUI         | IE: is responsible for user interactions.                                                                     | 

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * Company
 * SNSUser
 * Receptionist

Other software classes (i.e. Pure Fabrication) identified: 

 * SNSUserRegisterUI  
 * RegisterSNSUserController
 * SNSUserStore


## 3.2. Sequence Diagram (SD)

![US003_SD](US3_SD.svg)


## 3.3. Class Diagram (CD)


![US003_CD](US3_CD.svg)

# 4. Tests 

**To be implemented**

# 5. Construction (Implementation)


## Class RegisterSNSUserController 

    public void createSNSUser(SNSUser user) {
        App app = App.getInstance();
        store = app.getCompany().getUserStore();
        if (store.validateSNSUser(user.getName(),user.getAddress(), user.getPhoneNumber(), user.getEmail(), user.getCitizenCardNumber(), user.getGender(), user.getBirthDate(), user.getSnsUserNumber())) {
            SNSUser s1 = new SNSUser(user.getName(),user.getAddress(), user.getPhoneNumber(), user.getEmail(), user.getCitizenCardNumber(), user.getGender(), user.getBirthDate(), user.getSnsUserNumber());
        }
    }
    public void saveSNSUser(SNSUser user) {
        System.out.printf("\n==============================\n%s\n==============================", user);
            if (Utils.confirm("Confirm the data? (y/n)")) {
                if (store.addSNSUser(user)) {
                    System.out.print("\nSNS User added successfully!");
                }
            } else {
                System.out.print("\nInvalid Option!");
            }
        }


## Class RegisterSNSUserUI


    public RegisterSNSUserUI() {
    }

    public void run() {
        List<MenuItem> options = new ArrayList<MenuItem>();

        options.add(new MenuItem("Register a new SNS User", new ShowTextUI("You have chosen to register a new SNS User.")));

        int option = 0;


        do {
            option = Utils.showAndSelectIndex(options, "\n\nReceptionist Menu:");
            if ((option >= 0) && (option < options.size())) {
                options.get(option).run();
                if (option == 0) {
                    String name = Utils.readLineFromConsole("Name:");
                    String address = Utils.readLineFromConsole("Address:");
                    String phoneNumber = Utils.getValidPhoneNumber();
                    String email = Utils.getValidEmail();
                    String gender = Utils.readLineFromConsole("Gender:");
                    String citizenCardNumber = Utils.readLineFromConsole("Citizen Card Number:");
                    Date birthDate = Utils.readDateFromConsole("BirthDate");
                    String snsUserNumber = Utils.readLineFromConsole("SNS User Number:");
                    snsRegister = new RegisterSNSUserController();

                    SNSUser s1 = new SNSUser(name, address, phoneNumber, email, citizenCardNumber, gender, birthDate, snsUserNumber);

                    snsRegister.createSNSUser(s1);
                    snsRegister.saveSNSUser(s1);
                }
            }
        } while (option != -1);

    }
## Class SNSUserStore

    public boolean validateSNSUser(String name, String address, String phoneNumber, String email, String citizenCardNumber, String gender, Date birthDate, String snsUserNumber) {
        if (name == null || address == null || phoneNumber == null || email == null || citizenCardNumber == null || birthDate == null || snsUserNumber == null || gender == null) {
            return false;
        }
        return true;
    }

    public boolean checkDuplicates(SNSUser user) {
        email = user.getEmail();
        boolean duplicatedUser = App.getInstance().getCompany().getAuthFacade().existsUser(email);
        if (duplicatedUser) {
            System.out.println("Duplicated User!");
            return false;
        }
        return true;
    }

    public String generatePassword() {
        pwdGenerator = new PasswordGenerator(7);
        return pwdGenerator.passwordGenerator();
    }

    public boolean addSNSUser(SNSUser user) {
        if (checkDuplicates(user)) {
            authFacade = App.getInstance().getCompany().getAuthFacade();
            email = user.getEmail();
            name = user.getName();
            pwd = generatePassword();
            this.authFacade.addUserWithRole("SNS User " + name, email, pwd, Constants.ROLE_SNS_USER);
            System.out.printf("\nEmail: %s\nPassword: %s",email,pwd);
            return true;
        }
        return false;
    }

## Class SNSUser

    public SNSUser(String name, String address, String phoneNumber, String email, String citizenCardNumber, String gender, Date birthDate, String snsUserNumber) {
        super(name, address, phoneNumber, email, citizenCardNumber);
        this.gender = gender;
        this.birthDate = birthDate;
        this.snsUserNumber = snsUserNumber;
    }

    public SNSUser(String name, String address, String phoneNumber, String email, String citizenCardNumber, Date birthDate, String snsUserNumber) {
        super(name, address, phoneNumber, email, citizenCardNumber);
        this.gender = GENDER_BY_OMISSION;
        this.birthDate = birthDate;
        this.snsUserNumber = snsUserNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getSnsUserNumber() {
        return snsUserNumber;
    }

    public void setSnsUserNumber(String snsUserNumber) {
        this.snsUserNumber = snsUserNumber;
    }


    @Override
    public String toString() {
        if(this.gender.length() > 1)
            return String.format("SNS User\n%sGender: %s\nBirth Date: %s\nSNS User Number: %s",
                super.toString(),gender, Utils.convertToSimpleDataFormat(birthDate),snsUserNumber);
        else
            return String.format("SNS User\n%sBirth Date: %s\nSNS User Number: %s",
                super.toString(),Utils.convertToSimpleDataFormat(birthDate),snsUserNumber);
    }



# 6. Integration and Demo 

* A new option on the Receptionist menu options was added.

* For some demo purposes some users are bootstrapped while system starts.

![demo_1](demo1.jpg)

![demo_2](demo2.jpg)

![demo_3](demo3.jpg)



# 7. Observations






