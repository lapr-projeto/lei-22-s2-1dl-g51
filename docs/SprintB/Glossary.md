# Glossary

**Pandemic Vaccination Management System.**

| **_TEA_** (EN)  | **_TEA_** (PT) | **_Description_** (EN)                                           |                                       
|:------------------------|:-----------------|:--------------------------------------------|
| **Administrator** | **Administrador** | Responsible for properly configuring and managing the core information (e.g.: type of vaccines, vaccines, vaccination centers, employees) required for this application to be operated daily by SNS users, nurses, receptionists, etc.  |
| **Employees** | **Funcionários** | Nurses, receptionists,DGS administrators.|
| **EU COVID Digital Certificate** | **Certificado Digital COVID Europeu** | Digital proof that a person has either been vaccinated against COVID-19 received a negative test result or recovered from COVID-19|
| **FIFO queue** | **Primeiro a entrar, primeiro a sair** | First-in First-out: Queue that operates on the first-in, first-out principle, hence the name. This is also referred to as the first-come, first-served principle.|
| **GDH** | **DGS (Direção Geral de Saúde)** | *General Directorate of Health* - Ministry of Health’s central service, integrated in the State’s direct administration, gifted with administrative autonomy. |
| **HCG** | **AGES (Agrupamentos de Centros de Saúde)** | *Health Centre Groupings*|
| **Health Centers** | **Centros de Saúde** |  Provide a wide variety of health services to the citizens of a certain area. It’s associated to a certain RHA (Regional Health Administration) and AGES (Health Centre Groupings), being able to administrate vaccines to any disease. Besides that, the nurses working at the health centres are allowed to send and deliver a vaccination certificate every time a SNS user asks for it. |
| **HRD** | **ARS (Administração Regional de Saúde )** |*Health Regional Directorate* |
| **Intellij IDE** | **...** | IntelliJ IDEA is an integrated development environment (IDE) written in Java for developing computer software. |
| **JavaFX 11** | **...** | Software platform for creating and delivering desktop applications, as well as web applications that can run across a wide variety of devices. |
| **Netbeans** | **...** | Integrated development environment (IDE) for Java, allows applications to be developed from a set of modular software components called modules. |
| **NHS** | **SNS (Serviço Nacional de Saúde)** | *National Health Service* - group of institutions and services which depend on the Ministry of Health. Their purpose is to make sure every citizen has access to health care, within the human, technical and financial resources limits.  |
| **Nurses** | **Enfermeiros** | Responsible for the vaccination.|
| **Receptionist** | **Rececionistas** | Registers the individual on the system (personal data) and schedules the day of the vaccination.|
| **Type of vaccines** | **Tipo de vacina** | Eg.: Covid vaccines - Pfizer, Moderna, Astra Zeneca, etc. |
| **Vaccine variation** | **Variação de vacinas** | Ex.: Dengue Vaccine, Tetanus Vaccine, etc.|
| **Vaccination Centers** | **Centros de Vacinação** | Facilities that administer vaccines (of a specific disease) in response to a pandemic outbreak.  |











