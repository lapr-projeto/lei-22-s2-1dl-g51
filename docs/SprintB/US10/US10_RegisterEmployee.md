# US10 - To register an employee

## 1. Requirements Engineering


### 1.1. User Story Description


As an administrator, I want to register an employee.



### 1.2. Customer Specifications and Clarifications 


**From the specifications document:**

>	The administrator is able to register different employees, select their roles and create their system authentications.


>	As long as it is not published, access to the Employee is exclusive to the administrator.



**From the client clarifications:**

> **Question:**  What is the correct format for the employee's phone number and CC?
>  
> **Answer:**  Consider that these two attributes follow the portuguese format.

-

> **Question:**  Besides a password and a user name, what other (if any) information should the Admin use to register a new employee? Are any of them optional?
>  
> **Answer:** Every Employee has only one role (Coordinator, Receptionist, Nurse).
Employee attributes: Id (automatic), Name, address, phone number, e-mail and Citizen Card number.
All attributes are mandatory.


### 1.3. Acceptance Criteria


* **AC1:** Each user must have a single role defined in the system. The "auth" component available on the repository must be reused (without modifications).
* **AC2:** The password must have seven alphanumeric characters, including three capital letters and two digits.
* **AC3:** All required fiels must be filled in but gender.


### 1.4. Found out Dependencies


n/a

### 1.5 Input and Output Data


**Input Data:**

* Typed data:
    * a name,
    * an address,
    * a phone number,
    * an email,
    * a citizen card number.



**Output Data:**

* List of employees registered
* (In)Success of the operation

### 1.6. System Sequence Diagram (SSD)

**Alternative 1**

![US006_SSD](US10_SSD.svg)

**Other alternatives might exist.**

### 1.7 Other Relevant Remarks

n/a


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 

![US006_MD](US10_MD.svg)

### 2.2. Other Remarks

n/a


## 3. Design - User Story Realization 

### 3.1. Rationale

**SSD - Alternative 1 is adopted.**

| Interaction ID | Question: Which class is responsible for...             | Answer          | Justification (with patterns)                                                                                 |
|:-------------  |:--------------------------------------------------------|:----------------|:--------------------------------------------------------------------------------------------------------------|
| Step 1  		 | 	... interacting with the actor?                        | AdminUi         | Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model. |
| 			  		 | 	... coordinating the US?                               | AdminController | Controller                                                                                                    |
| 			  		 | 	... instantiating a new Employee?                      | Employee        | In the DM Company knows an Employee.                                                                          |
| 			  		 | ... knowing the user using the system?                  | Company         | IE: knows/has all the Employees already registered                                                            |
| 			  		 | 	... knowing to which organization the user belongs to? | Company         | IE: has registed all the Employees.                                                                           |
| 			  		 | 							                                                 | Company         | IE: knows/has its own Employees                                                                               |
| 			  		 | 							                                                 | Employee        | IE: knows its own data (e.g. email)                                                                           |
| Step 2  		 | 							                                                 |                 |                                                                                                               |
| Step 3  		 | 	...saving the inputted data?                           | EmployeeStore   | IE: object created in step 1 has its own data.                                                                |
| Step 4  		 | 	...knowing the roles to show?                          | Administrator   | IE: Employee roles are defined by the administrator.                                                          |
| Step 5  		 | 	... saving the selected role?                          | AdminUI         | IE: object created in step 1 is classified in AdminUI.                                                        |
| Step 6  		 | 							                                                 |                 |                                                                                                               |              
| Step 7  		 | 	... validating all data (local validation)?            | Employee        | IE: owns its data.                                                                                            | 
| 			  		 | 	... validating all data (global validation)?           | Company         | IE: knows all its tasks.                                                                                      | 
| 			  		 | 	... saving the created Employee?                       | EmployeeStore   | IE: owns all its tasks.                                                                                       | 
| Step 8  		 | 	... informing operation success?                       | AdminUI         | IE: confirms the registration of a new Employee.                                                              | 

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * Organization
 * Platform
 * Task

Other software classes (i.e. Pure Fabrication) identified: 

 * AdminUI  
 * AdminController


## 3.2. Sequence Diagram (SD)

**Alternative 1**

![US006_SD](US10_SD.svg)

## 3.3. Class Diagram (CD)

**From alternative 1**

![US006_CD](US10_CD.svg)

# 4. Tests 

To be implemented


# 5. Construction (Implementation)

To be implemented

# 6. Integration and Demo 

To be implemented


# 7. Observations

To be implemented





