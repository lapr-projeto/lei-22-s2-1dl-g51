    # Use Case Diagram (UCD)

**In the scope of this project, there is a direct relationship of _1 to 1_ between Use Cases (UC) and User Stories (US).**

However, be aware, this is a pedagogical simplification. On further projects and curricular units might also exist _1 to N **and/or** N to 1 relationships between US and UC.

![Use Case Diagram](Images/UCD.svg)

**For each UC/US, it must be provided evidences of applying main activities of the software development process (requirements, analysis, design, tests and code). Gather those evidences on a separate file for each UC/US and set up a link as suggested below.**

# Use Cases / User Stories
| UC/US     | Description                                                           |                   
|:----------|:----------------------------------------------------------------------|
| UC 01     | Validates the arrival of the user to take the vaccine                 |                                                                       |
| UC 02     | Acknowledges the system that the user is ready to take the vaccine.   |
| **UC 03** | **Registers SNSUser**                                                 |
| UC 04     | Registers vaccine administration                                      |
| UC 05     | Checks the list of SNS users present in the vaccination center        |
| UC 06     | Records adverse reactions of a SNS User                               |
| UC 07     | Schedules vaccine administration                                      |
| UC 08     | Requests the issuance of the EU COVID Digital Certificate             |
| **US 09** | **Registers the vaccination center to respond to a certain pandemic** |
| **US 10** | **Registers vaccination center employee**                             |
| **US 11** | **Gets a list of Employees with a given function/role**               |
| **US 12** | **Specifies a new vaccine type**                                      |
| **US 13** | **Specifies a new vaccine type and its administration process**       |
| UC 14     | Registers SNSUser                                                     |
| UC 15     | Analyses statistics and charts from their vaccination center          |
| UC 16     | Generate performance reports of the vaccination process               |
| UC 17     | Analyses data from other vaccination centers                          |
| UC 18     | Sends vaccination schedule confirmation by SMS                        |
