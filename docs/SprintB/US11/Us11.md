# US 11 - List of Employees

## 1. Requirements Engineering


### 1.1. User Story Description
As an administrator, I want to get a list of Employees with a given function/ role.

**From the document specifications:** 

- "An Administrator is responsible for properly configuring and managing the core information (e.g.:
  type of vaccines, vaccines, vaccination centers, employees) required for this application to be
  operated daily by SNS users, nurses, receptionists, etc."
 

### 1.2. Customer Specifications and Clarifications

n/a

### 1.3. Acceptance Criteria

The user must choose role first in order to get the Employees' list. 

### 1.4. Found out Dependencies

There is a dependency to US10, since the Employees' list in each function/role will depend on their registration.

### 1.5 Input and Output Data

**Input data:**

- Selected data: Selecting function/role of the Employee.




**Output data:**

- List of Employees with a given function/role.



### 1.6. System Sequence Diagram (SSD)

![US11_SD](US11_SSD.png)


### 1.7 Other Relevant Remarks

n/a


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US11_DM](US11_DM.png)

### 2.2. Other Remarks

n/a

## 3. Design - User Story Realization

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:---------------|:--------------------- |:------------|:---------------------------- |
| Step 1  		     |							 |             |                              |
| Step 2  		     |							 |             |                              |
| Step 3  		     |							 |             |                              |
| Step 4  		     |							 |             |                              |
| Step 5  		     |							 |             |                              |
| Step 6  		     |							 |             |                              |              
| Step 7  		     |							 |             |                              |
| Step 8  		     |							 |             |                              |
| Step 9  		     |							 |             |                              |
| Step 10  		    |							 |             |                              |  


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

* Class1
* Class2
* Class3

Other software classes (i.e. Pure Fabrication) identified:
* xxxxUI
* xxxxController

## 3.2. Sequence Diagram (SD)


![US11_SD](US11_SD.png)

## 3.3. Class Diagram (CD)


![US11_CD](US11_CD.png)

# 4. Tests
*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling.*

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**Test 1:** Check that it is not possible to create an instance of the Example class with null values.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

*It is also recommended to organize this content by subsections.*

# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in accordance with the previously carried out design. Furthermore, it is recommeded to mention/describe the existence of other relevant (e.g. configuration) files and highlight relevant commits.*

*It is also recommended to organize this content by subsections.*

# 6. Integration and Demo

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*


# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*





