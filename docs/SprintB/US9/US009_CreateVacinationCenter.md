# US009 - Vacination Center registration

## 1. Requirements Engineering

### 1.1. User Story Description

As an administrator I want to register a new vaccination center to respond a certain pandemic

### 1.2. Customer Specifications and Clarifications

- "In case of a new vaccination center, the receptionist registers the VC in the application. To register a VC the administrator needs the VC’s name, address, phone number, email address, opening hour, closing hour, slot duration and maximum number of vaccines per slot."

**From the document specifications:**

- "...the application should be designed to easily support managing other future pandemic events requiring a massive vaccination of the population. The
  software application should also be conceived having in mind that it can be further commercialized
  to other companies and/or organizations and/or healthcare systems besides DGS."


- "The DGS vaccination process is mainly carried out through community mass vaccination centers
  and health care centers distributed across the country. Different from the health care centers, which
  provide a wide range of healthcare services to citizens in a certain area, the community mass
  vaccination centers are facilities specifically created to administer vaccines of a single type as
  response to an ongoing disease outbreak (e.g.: Covid-19)."


- "...vaccination centers...characterized by a name, an address, a phone number, an e-mail address, a
  fax number, a website address, opening and closing hours, slot duration (e.g.: 5 minutes) and the
  maximum number of vaccines that can be given per slot (e.g.: 10 vaccines per slot). In addition,
  each vaccination center has one coordinator. Furthermore, receptionists and nurses registered in the
  application will work in the vaccination process. As the allocation of receptionists and nurses to
  vaccination centers might be complex, by now, the system might assume that receptionists and
  nurses can work on any vaccination center."


- "Each vaccination center has a Center Coordinator that has the responsibility to manage the Covid19 vaccination process. The Center Coordinator wants to monitor the vaccination process, to see
  statistics and charts, to evaluate the performance of the vaccination process, generate reports and
  analyze data from other centers, including data from law systems."
-
-
-
-

### 1.3. Acceptance Criteria

- AC1: All required fields must be filled in.
- AC2: If the vacination center registered already exists, the system must reject the operation
- AC3: If the vacination center registers already exists but has changes on the data, the program must add the new/changed information.   

### 1.4. Found out Dependencies

- There must be a "VaccineType" already defined for the Community mass vaccination center.

### 1.5 Input and Output Data

**Input Data:**

* Typed data:
  * name
  * address
  * phoneNumber
  * email
  * address
  * openingHour
  * closingHour
  * slotDuration
  * maximumNumberVaccinesPerSlot

* Selected data:
  * Classifying the vaccine type


**Output Data:**

* List of existing vaccine types
* (In)Success of the operation


### 1.6. System Sequence Diagram (SSD)

**Alternative 1**

![US009_SSD](imagens\US009_SSD.svg)

**Other alternatives might exist.**

### 1.7 Other Relevant Remarks

*None for now*

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US009_MD](imagens\US009_MD.svg)

### 2.2. Other Remarks

*None for now*


## 3. Design - User Story Realization

### 3.1. Rationale

**SSD - Alternative 1 is adopted**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1  		     | 	... interacting with the actor?                        | RegisterNewVaccinationCenterUI | Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model. |
| 			  		        | 	... coordinating the US?                               | RegisterNewVaccinationCenterController | Controller                                                                                                    |
| 			  		        | 	... instantiating a new SNS User?                      | Administrator                   | in the DM, the administrator registers a vaccination center                                                                              |
| 			  		        | ... knowing the user using the system?                  | Company                   | IE: knows/has all the users already registered                                                                |
| 			  		        | 	... knowing to which organization the user belongs to? | Company                   | IE: has registed all the users                                                                                |
| 			  		        | 							                                                 | Company                   | IE: knows/has its own users                                                                                   |
| 			  		        | 							                                                 | SNS User                  | IE: knows its own data (e.g. email)                                                                           |
| Step 2  		     | 	...saving the inputted data?                           | Controller                  | IE: object created in step 1 has its own data.                                                                |
| Step 3  		     | 	...knowing the roles to show?                          | Company                   | IE: User roles are defined by the Company.                                                                    |
| Step 4  		     | 	... saving the selected category (vaccine type)?                      | Controller                 | IE: object created in step 1 is classified in one role.                                                       |
| Step 5  		     | 	... validating all data (local validation)?            | Controller                  | IE: owns its data.                                                                                            | 
| 			  		        | 	... validating all data (global validation)?           | Company                   | IE: knows all its tasks.                                                                                      | 
| 			  		        | 	... saving the created task?                           | Company                   | IE: owns all its tasks.                                                                                       | 
| Step 6  		     | 	... informing operation success?                       | RegisterNewVaccinationCenterUI              | IE: is responsible for user interactions.                                                                     | 


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

* VaccineCenter
* Company

Other software classes (i.e. Pure Fabrication) identified:
* CreateVacinationCenterUI
* CreateVacinationnCenterController

## 3.2. Sequence Diagram (SD)

**Alternative 1**

![US009_SD](imagens\US009_SD.svg)

**Other alternatives might exist.**

## 3.3. Class Diagram (CD)

**From alternative 1**

![US009_CD](imagens\US009_CD.svg)

# 4. Tests

**Test 1:** Check that it is not possible to create an instance of the Example class with null values.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}


# 5. Construction (Implementation)

##Class CreateVacinationCenterUI

##Class CreateVacinationCenterController

##Class Company


# 6. Integration and Demo


# 7. Observations






