    # Use Case Diagram (UCD)

**In the scope of this project, there is a direct relationship of _1 to 1_ between Use Cases (UC) and User Stories (US).**

However, be aware, this is a pedagogical simplification. On further projects and curricular units might also exist _1 to N **and/or** N to 1 relationships between US and UC.

![Use Case Diagram](Images/UCD.svg)

**For each UC/US, it must be provided evidences of applying main activities of the software development process (requirements, analysis, design, tests and code). Gather those evidences on a separate file for each UC/US and set up a link as suggested below.**

# Use Cases / User Stories
| UC/US | Description                                                         |                   
|:------|:--------------------------------------------------------------------|
| UC 01 | Specifies a new vaccine type and its administration process         |
| UC 02 | Registers vaccination center employees                              |
| UC 03 | Registers the vaccination centers                                   |
| UC 04 | Registers SNSUsers                                                  |
| UC 05 | Registers vaccine administration                                    |
| UC 06 | Checks the list of SNS users present in the vaccination center      |
| UC 07 | Records adverse reactions of a SNS User                             |
| UC 08 | Validates the arrival of the user to take the vaccine               |
| UC 09 | Acknowledges the system that the user is ready to take the vaccine. |
| UC 10 | Schedules vaccine administration                                    |
| UC 11 | Requests the issuance of the EU COVID Digital Certificate           |
| UC 12 | Analyses statistics and charts from their vaccination center        |
| UC 13 | Generate performance reports of the vaccination process             |
| UC 14 | Analyses data from other vaccination centers                        |
| UC 15 | Sends vaccination schedule confirmation by SMS                      |
