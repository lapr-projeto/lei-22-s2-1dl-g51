# US 002 - Schedule Vaccine as a Receptionist

## 1. Requirements Engineering

### 1.1. User Story Description

As a receptionist at one vaccination center, I want to schedule a vaccination.

### 1.2. Customer Specifications and Clarifications

**From the specifications document:**

> Some users (e.g.: older ones) may want to go to a healthcare center to schedule the
> vaccine appointment with the help of a receptionists at one vaccination center.

> Then, the application should check the vaccination center capacity for that day/time and, if possible,
> confirm that the vaccination is schedule.

> The SNS user may also authorize the DGS to send a SMS message with
> information about the scheduled appointment. If the user authorizes the sending of the SMS, the
> application should send an SMS message when the vaccination event is scheduled and registered in
> the system.



**From the client clarifications:**

> **Question:** US1 and US2 acceptance criteria?
>
> **Answer:** A SNS user cannot schedule the same vaccine more than once. The algorithm should check if the SNS User is
> within the age and time since the last vaccine.

>**Question:**
"For the US1, the acceptance criteria is: A SNS user cannot schedule the same vaccine more than once. For the US2, the acceptance criteria is: The algorithm should check if the SNS User is within the age and time since the last vaccine.
[1] Are this acceptance criteria exclusive of each US or are implemented in both?
[2] To make the development of each US more clear, could you clarify the differences between the two US?"
> 
>**Answer:** 1 - The acceptance criteria for US1 and US2 should be merged. The acceptance criteria por US1 and US2 is: A SNS user cannot schedule the same vaccine more than once. The algorithm should check if the SNS User is within the age and time since the last vaccine.<p>
>2 - In US1 the actor is the SNS user, in US2 the actor is the receptionist. In US1 the SNS user is already logged in the system and information that is required and that exists in the system should be automatically obtained. In US2 the receptionist should ask the SNS user for the information needed to schedule a vaccination. Information describing the SNS user should be automatically obtained by introducing the SNS user number.

> **Question:** We are unsure if it's in this user stories that's asked to implement the "send a SMS message with
> information about the scheduled appointment" found on the Project Description available in moodle. Could you clarify?
>
> **Answer:**
> A file named SMS.txt should be used to receive/record the SMS messages. We will not use a real word service to send
> SMSs.

> **Question:** "Regarding US02, i would like to know if a receptionist has the ability to schedule an appointment in different vaccination centres or only on their own."
> 
> **Answer:** The receptionist has the ability to schedule the vaccine in any vaccination center. The receptionist should ask the SNS user to indicate/select the preferred vaccination center.

### 1.3. Acceptance Criteria

* **AC1:** All required fields must be filled.
* **AC2:** The algorithm should check if the SNS User is within the age and time since the last vaccine.
* **AC3:** The vaccination center of the vaccination schedule must be already registered in the System.
* **AC4:** The vaccine type of the vaccination schedule must be already registered in the System.
* **AC5:** The SNSUser must be already registered in the System.
* **AC6:** The SNSUser receives a sms informing that the vaccination schedule was registered successfully. The sms
  includes the schedule appointment. All the sms messages should be written to a file with the name SMSto(phoneNumber)
  .txt on the vaccinationScheduleSMS directory.

### 1.4. Found out Dependencies

**Depends on:**<p>

* **US3**: As a receptionist, I want to register a SNS user.
* **US9**: As an administrator, I want to register a vaccination center to respond to a certain pandemic.
* **US12**: As an administrator, I intend to specify a new vaccine type.
* **US13**: As an administrator, I intend to specify a new vaccine and its administration
  process.

### 1.5 Input and Output Data

**Input Data:**

* Typed data:
  * SNS User Number
  * Date

* Selected data:
  * Vaccination center
  * Time
  * Type of Vaccine

**Output Data:**

* SMS with schedule appointment
* (In)Success of the operation

### 1.6. System Sequence Diagram (SSD)

![US001_SSD](US2_SSD.svg)

**Other alternatives might exist.**

### 1.7 Other Relevant Remarks

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US001_MD](US2_MD.svg)

### 2.2. Other Remarks

n/a

## 3. Design - User Story Realization

### 3.1. Rationale

**SSD**


| Interaction ID | Question: Which class is responsible for...                      | Answer                              | Justification (with patterns)                                                                                                                                                                                                                    |
|:---------------|:-----------------------------------------------------------------|:------------------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Step 1         | ... interacting with the actor?                                  | VaccinationScheduleByRecepcionistUI | **Pure Fabrication:** there is no reason to assign this responsibility to any existing class in the Domain Model.                                                                                                                                |
| 	  		          | ... coordinating the US?                                         | VaccinationScheduleController       | **Controller**                                                                                                                                                                                                                                   |
| Step 2         | ...transfer the data typed and selected in the UI to the domain? | VaccinationScheduleDTO              | **DTO:** When there is so much data to transfer, it is better to opt by using a DTO in order to reduce coupling between UI and domain                                                                                                            |
|                | ...saving the typed and selected data?                           | VaccinationSchedule                 | **IE:** a vaccination schedule knows its own data                                                                                                                                                                                                |                                                                  |                                |                                                                                                                   |
| 		             | ... instantiating a new VaccinationSchedule?                     | VaccinationScheduleStore            | **Creator** and **HC+LC**: By the application of the Creator it would be the "Company". But, by applying HC + LC to the "Company", this delegates that responsibility to the "VaccinationScheduleStore"                                          |
| Step 3         | ... knows VaccinationScheduleStore?                              | Company                             | **IE:** Company knows the VaccinationScheduleStore to which it is delegating some tasks                                                                                                                                                          |
| 		             | ... validating all data (local validation)?                      | VaccinationSchedule                 | **IE:** an object knows its data                                                                                                                                                                                                                 |
| 			  		        | ... validating all data (global validation)?                     | VaccinationScheduleStore            | **IE:** knows all the vaccination schedules data                                                                                                                                                                                                 |
|                | ...saving the vaccination schedule                               | VaccinationScheduleStore            | **IE:** knows all the vaccination schedules                                                                                                                                                                                                      |
|                | ... informing operation success?                                 | VaccinationScheduleByReceptionistUI | **IE:** is responsible for user interactions                                                                                                                                                                                                     |
|                | ... asks for SMS authorization?                                  | VaccinationScheduleByReceptionistUI | **IE:** is responsible for user interactions                                                                                                                                                                                                     |                                                                                                                                                                                                                                                   |
| Step 4         | ... sending SMS?                                                 | SMSNotificationSender               | **IE and PureFabrication:** has all the required information and means to send the SMS (IE). However, to avoid code duplication (cf. US1) this responsibility might be assign to a common and shared artificial class, specialized in this task. | 
|                | ... informing that SMS has been sent successfully?               | VaccinationScheduleByReceptionistUI | **IE:** is responsible for user interactions                                                                                                                                                                                                     |
### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

* Company
* VaccineSchedule
* SNSUser

Other software classes (i.e. Pure Fabrication) identified:

* VaccinationScheduleByReceptionistUI
* VaccinationScheduleController
* VaccinationScheduleStore
* VaccinationScheduleDTO
* SMSNotificationSender

## 3.2. Sequence Diagram (SD)

![US001_SD](US2_SD.svg)

## 3.3. Class Diagram (CD)

![US001_CD](US2_CD.svg)

# 4. Tests

    @Test
    @Order(1)
    void validateVaccineSchedule() {
        assertTrue(vsStoreTest.validateVaccineSchedule(scheduleTest));
        assertFalse(vsStoreTest.validateVaccineSchedule(scheduleTest2));
        assertFalse(vsStoreTest.validateVaccineSchedule(scheduleTest3));
        assertFalse(vsStoreTest.validateVaccineSchedule(scheduleTest4));
    }

    @Test
    @Order(2)
    void checkDuplicates() {
        assertFalse(vsStoreTest.checkDuplicates(scheduleTest));

        vsStoreTest.getVaccinationScheduleList().add(scheduleTest);
        assertTrue(vsStoreTest.checkDuplicates(scheduleTest));
    }

    @Test
    @Order(3)
    void addVaccinationSchedule() {
        assertTrue(vsStoreTest.addVaccinationSchedule(scheduleTest1));
        assertFalse(vsStoreTest.addVaccinationSchedule(scheduleTest1));
    }

    @Test
    @Order(4)
    void counterSchedulesPerSlot() {
        assertEquals(vsStoreTest.counterSchedulesPerSlot("12:00",new Date().toString(),vcDto1),4);
        assertEquals(vsStoreTest.counterSchedulesPerSlot("12:00",new Date().toString(),vcDto2),5);
    }

    @Test
    @Order(5)
    void checkAvailableHours() {
        List<String> availableHoursTest = new ArrayList<>();
        availableHoursTest.add("10:00");
        availableHoursTest.add("10:30");
        availableHoursTest.add("11:00");
        availableHoursTest.add("11:30");
        availableHoursTest.add("12:00");

        assertArrayEquals(availableHoursTest.toArray(),vsStoreTest.checkAvailableHours(vcDto2,"23-03-2024").toArray());

        availableHoursTest.add("9:00");
        assertFalse(Arrays.equals(availableHoursTest.toArray(),vsStoreTest.checkAvailableHours(vcDto2,"23-03-2024").toArray()));

        availableHoursTest.clear();
        assertTrue(availableHoursTest.isEmpty());
    }

    @Test
    @Order(6)
    void checkAvailableHoursToday() {
        //the method gets the actual hour and date! Must fail in certain circumstances.
        assertFalse(vsStoreTest.checkAvailableHoursToday(new Hour("2:00"), Utils.convertToSimpleDataFormat(new Date())));
        assertTrue(vsStoreTest.checkAvailableHoursToday(new Hour("23:59"), Utils.convertToSimpleDataFormat(new Date())));
        assertTrue(vsStoreTest.checkAvailableHoursToday(new Hour("2:00"), "23-03-2025"));
    }

# 5. Construction (Implementation)

## Class VaccinationScheduleController

    public boolean checkUserRegistration(String snsUserNumber) {
        snsUserStore = company.getUserStore();
        return snsUserStore.checkUserRegistration(snsUserNumber);
    }

    public VaccinationSchedule createVaccinationSchedule(String snsUserNumber, VaccinationCenter vaccinationCenter, Date date, String time, VaccineType vaccineType){
        VaccinationSchedule vaccinationSchedule = new VaccinationSchedule(snsUserNumber,vaccinationCenter,date,time,vaccineType);
        if(vaccinationScheduleStore.validateVaccineSchedule(vaccinationSchedule))
            return vaccinationSchedule;
        return null;
    }

    public void saveVaccinationSchedule(VaccinationSchedule vs){
        if(vaccinationScheduleStore.validateVaccineSchedule(vs)){
            if(vaccinationScheduleStore.addVaccinationSchedule(vs)){
                System.out.println("Vaccine Schedule created successfully!");
            }
        }
    }

    public void sendSMS (VaccinationSchedule vs) {
        String userNumber = vs.getScheduleDTO().getSnsUserNumber();
        SNSUser snsUser = snsUserStore.getSNSUserWithAGivenSNSUserNumber(userNumber);
        String phoneNumber = snsUser.getSNSUserDTO().getPhoneNumber();
        PrintWriter printWriter = null;

        try {
            printWriter = new PrintWriter(new File("vaccinationScheduleSMS/SMSto" + phoneNumber + ".txt"));
            printWriter.print(String.format("Vaccination Schedule\n  Name: %s\n  SNS User Number: %s\n  Vaccination Center: %s\n"
                            + "  Vaccine:%s\n  Date:%s\n  Hour:%s\nRegards,\n%s", snsUser.getSNSUserDTO().getName(), userNumber,
                    vs.getScheduleDTO().getVaccinationCenter().getVaccinationCenterDTO().getName(), vs.getScheduleDTO().getVaccineType().getVaccineTypeDTO().getType().name(), Utils.convertToSimpleDataFormat(vs.getScheduleDTO().getDate()), vs.getScheduleDTO().getTime(), company.getDesignation()));
            System.out.println("SMS has been sent successfully");
        } catch (FileNotFoundException ex) {
            System.out.println("File Not Found");
        }
        finally {
            printWriter.close();
        }
    }

## Class VaccinationScheduleUI

    @Override
    public void run() {
        Company company = App.getInstance().getCompany();
        VaccinationCenterStore vaccinationCenterStore = company.getVaccinationCenterStore();
        VaccineTypeStore vaccineTypeStore = company.getVaccineTypeStore();
        VaccinationScheduleStore vaccinationScheduleStore = company.getVaccinationScheduleStore();

        vsController = new VaccinationScheduleController();
        VaccineType vaccineType = null;
        String snsUserNumber = Utils.getValidSNSNumber();
        if (vsController.checkUserRegistration(snsUserNumber)) {
            do {
                vaccineType = (VaccineType) Utils.showAndSelectOne(vaccineTypeStore.getVaccineTypeList(), "\n\nChoose one of the Vaccine Types");
            }while (vaccineType == null);

            List<VaccinationCenter> vaccinationCenterListByVaccineType = vaccinationCenterStore.getVaccinationCentersWithGivenVaccineType(vaccineType);
            VaccinationCenter vaccinationCenter = (VaccinationCenter) Utils.showAndSelectOne(vaccinationCenterListByVaccineType, "\n\nChoose one of the Vaccination Centers");
            Date date = Utils.getValidScheduleDate("Date: ");
            String time = (String) Utils.showAndSelectOne(vaccinationScheduleStore.checkAvailableHours(vaccinationCenter.getVaccinationCenterDTO(), Utils.convertToSimpleDataFormat(date)), "\nChoose one of the hours available: ");

            schedule = vsController.createVaccinationSchedule(snsUserNumber, vaccinationCenter, date, time, vaccineType);

            if (schedule != null) {
                System.out.printf("\n%s\n", schedule);
                if (Utils.confirm("Confirm the data? (y/n)")) {
                    vsController.saveVaccinationSchedule(schedule);
                    correctInfo = true;
                }
            } else {
                System.out.println("Invalid Vaccination Schedule!");
            }
        } else {
            System.out.println("SNS User has not been registered in the System yet!");
        }
    }

}

## Class VaccinationScheduleByReceptionistUI

     public void run() {
        super.run();
        if(correctInfo) {
            if (Utils.confirm("Do you want to send a SMS with vaccination schedule information? (y/n)")) {
                vsController.sendSMS(schedule);
            }
        }
    }

## Class VaccinationScheduleStore

    public VaccinationScheduleStore() {
    }

    public ArrayList<VaccinationSchedule> getVaccinationScheduleList() {
        return vaccinationScheduleList;
    }

    public boolean validateVaccineSchedule(VaccinationSchedule vaccinationSchedule) {
        Company company = App.getInstance().getCompany();
        VaccinationScheduleDTO vs = vaccinationSchedule.getScheduleDTO();
        VaccineType vaccineType = vs.getVaccineType();
        VaccinationCenter vaccinationCenter = vs.getVaccinationCenter();
        VaccineTypeStore vaccineTypeStore = company.getVaccineTypeStore();
        VaccinationCenterStore vaccinationCenterStore = company.getVaccinationCenterStore();

        if (Objects.isNull(vaccinationSchedule) || vs.getSnsUserNumber() == null || vs.getVaccinationCenter() == null || vs.getDate() == null || vs.getTime() == null
                || vs.getVaccineType() == null || !vaccineTypeStore.getVaccineTypeList().contains(vaccineType) || !vaccinationCenterStore.getVaccinationCentersList().contains(vaccinationCenter)
                || checkDuplicates(vaccinationSchedule)) {
            return false;
        }
        return true;
    }

    public boolean checkDuplicates(VaccinationSchedule vs) {
        return vaccinationScheduleList.contains(vs);
    }

    public boolean addVaccinationSchedule(VaccinationSchedule vs) {
        if (validateVaccineSchedule(vs)) {
            if (!checkDuplicates(vs)) {
                vaccinationScheduleList.add(vs);
                return true;
            } else {
                System.out.println("There is a duplicated schedule for this SNS User Number");
                return false;
            }
        }
        return false;
    }

    public int counterSchedulesPerSlot(String hours, String date, VaccinationCenterDTO vc) {
        int vaccinesPerSlot = Integer.parseInt(vc.getVaccinesSlotCap());
        for (VaccinationSchedule vs : vaccinationScheduleList) {
            if (vs.getScheduleDTO().getTime().equals(hours) && Utils.convertToSimpleDataFormat(vs.getScheduleDTO().getDate()).equalsIgnoreCase(date)) {
                vaccinesPerSlot -= 1;
            }
        }
        return vaccinesPerSlot;
    }

    public List<String> checkAvailableHours(VaccinationCenterDTO vc, String date) {
        List<String> hourList = new ArrayList<>();
        Hour openHour = new Hour(vc.getOpenHours());
        Hour closingHour = new Hour(vc.getClosingHours());

        for (Hour hours = openHour; Hour.isAfter(hours.toString(), closingHour.toString()); hours.advanceMinutes(Integer.parseInt(vc.getSlotDuration()))) {
            int slotsLeft = counterSchedulesPerSlot(hours.toString(), date, vc);
            boolean hoursAvailable = checkAvailableHoursToday(hours, date);
            if (slotsLeft > 0 && hoursAvailable)
                hourList.add(hours.toString());
        }

        if (hourList.isEmpty()) {
            System.out.println(vc.getName() + " is closed at the moment! Reopens tomorrow at " + vc.getOpenHours());
        }

        return hourList;
    }

    public boolean checkAvailableHoursToday(Hour openHour, String date) {
        Calendar calendar = GregorianCalendar.getInstance();

        Hour currentHour = new Hour(String.format("%02d:%02d", calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE)));
        if (Hour.isAfter(openHour.toString(), currentHour.toString()) && date.equalsIgnoreCase(Utils.convertToSimpleDataFormat(new Date()))) {
            return false;
        }
        return true;
    }

## Class VaccinationSchedule

    public VaccinationSchedule(String snsUserNumber, VaccinationCenter vaccinationCenter, Date date, String time, VaccineType vaccineType) {
        this.snsUserNumber = snsUserNumber;
        this.vaccinationCenter = vaccinationCenter;
        this.date = date;
        this.time = time;
        this.vaccineType = vaccineType;
        this.scheduleDTO = new VaccinationScheduleDTO(snsUserNumber,vaccinationCenter,date,time,vaccineType);
    }

    public VaccinationScheduleDTO getScheduleDTO() {
        return scheduleDTO;
    }

    public void setSnsUserNumber(String snsUserNumber) {
        this.snsUserNumber = snsUserNumber;
    }

    public void setVaccinationCenter(VaccinationCenter vaccinationCenter) {
        this.vaccinationCenter = vaccinationCenter;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setVaccineType(VaccineType vaccineType) {
        this.vaccineType = vaccineType;
    }

    @Override
    public String toString() {
        return String.format("===========VACCINATION SCHEDULE==========\nSNS User Number: %s\nVaccination Center %s\nVaccine Type %s\nDate: %s\nTime: %s\n=========================================",snsUserNumber,
                vaccinationCenter.toSimpleString(),vaccineType.toSimpleString(), Utils.convertToSimpleDataFormat(date),time);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VaccinationSchedule schedule = (VaccinationSchedule) o;
        return Objects.equals(snsUserNumber, schedule.snsUserNumber) && Objects.equals(vaccineType,schedule.vaccineType);
    }

# 6. Integration and Demo

* A new option on the Receptionist menu options was added.

* For some demo purposes some users are bootstrapped while system starts.

![demo_1](demo1.png)

![demo_2](demo2.jpg)

![demo_3](demo3.jpg)

![demo_4](demo4.jpg)

![demo_5](demo5.jpg)

![demo_6](demo6.jpg)

# 7. Observations






