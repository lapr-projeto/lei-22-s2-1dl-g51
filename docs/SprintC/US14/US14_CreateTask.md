# US 006 - To create a Task 

## 1. Requirements Engineering


### 1.1. User Story Description


As an administrator, I want to load a set of users from a CSV file.


### 1.2. Customer Specifications and Clarifications 


**From the specifications document:**

 n/a



**From the client clarifications:**

> **Question:** "What is the delimiter for the header? Does it have points between words, commas or something else?"
>
> **Answer:** Each type of CSV file has only one delimiter character.

> **Question:** "Are there any SNS User attributes that can be omitted?"
>
> **Answer:** The Sex attribute is optional (it can also take the NA value). All other fields are required.

>**Question:** "When the admin wants to upload a CSV file to be read, should the file be stored at a specific location on the computer (e.g. the desktop) or should the admin be able to choose the file he wants to upload in a file explorer?"
> 
>**Answer:** The Administrator should write the file path. In Sprint C we do not ask students to develop a graphical user interface.

>**Question:** "Should our application detect if the CSV file to be loaded contains the header, or should we ask the user if is submitting a file with a header or not?"
> 
>**Answer:** The application should automatically detect the CSV file type.

>**Question:** "What should the system do if the file to be loaded has information that is repeated? For example, if there are 5 lines that have the same information or that have the same attribute, like the phone number, should the whole file be discarded?"
> 
>**Answer:** If the file does not have other errors, all records should be used to register users. The business rules will be used to decide if all file records will be used to register a user. For instance, if all users in the CSV file are already registered in system, the file should be processed normally but no user will be added to the system (because the users are already in the system).

>**Question:** "Should we check if the users from the CSV file are already registered in the system? If so, which data should we use, the one already in the system or the one on the file?"
> 
>**Answer:** This feature of the system will be used to register a batch users. If the user is already registered in the system, then the information available in the CSV file should not be used to register that user.

### 1.3. Acceptance Criteria


* **AC1:** The application must support importing two types of CSV
  files: a) one type must have a header, column separation is done using “;”
  character; b) the other type does not have a header, column separation is done
  using “,” character.
* **AC2:** The file path must be inserted by the user in order to start it's examination.
* **AC3:** If there is an error in any element of the CSV file it gets totally rejected and does not load.

### 1.4. Found out Dependencies


* There is a dependency to "US3 As a receptionist, I want to register a SNS user." since it's methods will be used to confirm the users in the CSV file.


### 1.5 Input and Output Data


**Input Data:**

* Typed data:
	* file path
	
* Selected data:
	* n/a


**Output Data:**

* Confirmation of the data
* (In)Success of the operation

### 1.6. System Sequence Diagram (SSD)

**Alternative 1**

![US14_SSD](US14_SSD.png) 




### 1.7 Other Relevant Remarks

n/a


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 

![US14_MD](US14_MD.png)

### 2.2. Other Remarks

n/a


## 3. Design - User Story Realization 

### 3.1. Rationale

**SSD - Alternative 1 is adopted.**

| Interaction ID                                  | Question: Which class is responsible for...      | Answer  | Justification (with patterns)                                         |
|:------------------------------------------------|:-------------------------------------------------|:------------|:----------------------------------------------------------------------|
| Step 1: Asks to load a CSV file                 | ...loading a CSV file and store its information? |Administrator| The Administrator is the only user with permission to load a CSV file |
| Step 2: Asks for CSV file path                  | ...requesting the data needed?                   |UI| IE: responsible for the user interaction                              |
| Step 3: Inserts CSV file path                   | ...validating input data?                        |SNSUserStore| IE: knows all the SNS User objects                                    |
|                                                 | ...identifying the type of the CSV file?         |Company| IE: has access to the file data                                       |
| Step 4: Displays the data and requests approval | ... displaying the data?                         |UI| IE: responsible for user interaction                                  |
 | Step 5: Confirms data                           | ... the data confirmation?                       |Administrator | The Administrator ir the only user allowed                            |
| Step 6: Loads information                       | ... loading the information?                     | SNSUserStore | IE: knows all the SNS User objects |
### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * Company
 * SNSUserStore

Other software classes (i.e. Pure Fabrication) identified: 

 * LoadCSVFileUI 
 * LoadCSVFileController


## 3.2. Sequence Diagram (SD)

**Alternative 1**

![US14_SD](US14_SD.png)


## 3.3. Class Diagram (CD)

**From alternative 1**

![US14_CD](US14_CD.png)

# 4. Tests 

**Test 1**  

	@Test
    void validateSNSUser() {
        assertTrue(storeTest.validateSNSUser(userTest1));
        assertFalse(storeTest.validateSNSUser(userTest2));
        assertFalse(storeTest.validateSNSUser(userTest3));
    }



**Test 2** 

	@Test
    void checkDuplicates() {
        assertFalse(storeTest.checkDuplicates(userTest4));
        assertTrue(storeTest.checkDuplicates(userTest5));
    }

**Test 3**

    @Test
    void generatePassword() {
        String pass1 = storeTest.generatePassword();
        String pass2 = storeTest.generatePassword();

        assertEquals(pass1.length(),pass2.length());
        assertNotNull(pass1);
        assertNotNull(pass2);
        assertNotEquals(pass1,pass2);
    }

**Test 4**

    @Test
    void addSNSUser() {
        assertTrue(storeTest.addSNSUser(userTest1));
        assertFalse(storeTest.addSNSUser(userTest4));
    }




# 5. Construction (Implementation)

n/a




# 6. Integration and Demo 

n/a

# 7. Observations

n/a





