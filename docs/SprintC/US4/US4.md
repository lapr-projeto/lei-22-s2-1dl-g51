# US 4 - Register the arrival of a SNS user to take the vaccine

## 1. Requirements Engineering

*The Receptionist should have the possibility to choose one vaccination center to register the arrival of a SNS User to take the vaccine *

### 1.1. User Story Description

*  As a receptionist at a vaccination center, I want to register the arrival of a SNS user to take the vaccine. *

### 1.2. Customer Specifications and Clarifications

*From the Specifications Document:*
• “[…] On the scheduled day and time, the SNS user should go to the vaccination center to get the vaccine. When the SNS user arrives at the vaccination center, a receptionist registers the arrival of the user to
take the respective vaccine. The receptionist asks the SNS user for his/her SNS user number and confirms that he/she has the vaccine scheduled for the that day and time. If the information is
correct, the receptionist acknowledges the system that the user is ready to take the vaccine. Then, the receptionist should send the SNS user to a waiting room where (s)he should wait for his/her time "
*From the client clarifications:*
*Cliente Clarification #1*
Question: "Respectively to US04, after the receptionist registers the SNS User's arrival at the Vaccination Center, the system creates the list that will be available for the Nurse to view, correct? "

Answer: The nurse checks the list (of SNS users in the waiting room) in US05.
*Cliente Clarification #2*
Question: "Regarding US04, i would like to know what's the capacity of the waiting room."

Answer: The waiting room will not be registered or defined in the system. The waiting room of each vaccination center has the capacity to receive all users who take the vaccine on given slot.
*Cliente Clarification #3*
Question: "Regarding US04, the attribute "arrival time" should be considered to let the user enter the waiting room.
For example, a user that arrives 40 minutes after his appointment wont be allowed to enter the center, and another who only arrives 10 minutes late may proceed. If so, how much compensation time should we provide to the user."

Answer: In this sprint we are not going to address the problem of delays. All arriving users are attended and registered by the receptionist.


### 1.3. Acceptance Criteria

*AC1: No duplicate entries should be possible for the same SNS user on the same day or vaccine period*
*AC2: SNS User number cannot be empty and has, at maximum, 9 chars*
*AC3: The system must have at least one Vaccination Center registered.*
*AC4: The Vaccination schedule must coincide with the arrival date of a SNS user*


### 1.4. Found out Dependencies
*No dependencies.*
### 1.5 Input and Output Data
*Input Data
• Typed data: SNSNumber, VaccinationCenter
• Selected data: (none)
Output Data
• (In)Success of the operation*
### 1.6. System Sequence Diagram (SSD)

![US4-SSD](US4_SSD.svg)

### 1.7 Other Relevant Remarks
None.
## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt
![US4-MD](US4_MD.svg)

### 2.2. Other Remarks
None.
## 3. Design - User Story Realization

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1  		 |... interacting with the actor?                                            |ArrivalTimeUI          | Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model.|
|                |... coordinating the US?|ArrivalTimeController                             |Controller             | Pure Fabrication
|                |... serving as an intermediary between the UI layer and the Domain layer ? |Controller             | Controller direct communication between UI classes and domain classes must be avoided|
|                |... storing the centers?                                                   |VaccinationCenterStore | Pure Fabrication
|                |... returning the center store?                                            |Company                | All stores are owned by information expert
|                |... returning the receptionist's center?                                   |VaccinationCenterStore | All stores are owned by information expert
| Step 2  		 |... requesting SNSNumber to the receptionist?      						 |ArrivalTimeUI          | UI: All user interaction are supported in the UI.                           
| Step 3  		 |... typing SNS User Number?                                                |Receptionist           | Pure Fabrication
|                |... storing the SNS Users?                                                 |SNSUserStore           | Pure Fabrication
|                |... returning the SNS User store?                                          |Company                | All stores are owned by the information expert
|                |... returning the SNS Users?                                               |SNSUserStore           | All SNS Users are known by the information expert
|                |... storing the vaccination schedules?                                     |VaccinationScheduleStore| Pure Fabrication
|                |... returning the vaccination schedule store?                              |Company                | All stores are known by the information expert
|                |... verifying the schedule for the SNS User?                               |VaccinationScheduleStore|All vaccination schedules are known by the information expert
| Step 4  		 |... showing and asking for confirmation of the input data?                 |ArrivalTimeUI
| Step 5  		 |... confirming the data?                                                   |Receptionist
|                |... validating the data?                                                   |SNSUser                | All SNS Users are known by the information expert
|                |... saving the data?                                                       |SNSUser                | All SNS Users are known by the information expert
|                |... adding user to waiting list?                                           |SNSUserList            | All SNS Users are known by the information expert
| Step 6         |... informing operating (in)sucess                                         |ArrivalTimeUI          | UI: All user interaction are supported in the UI.


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

* Company
* Vaccination Center
* SNSNumber
* 
Other software classes (i.e. Pure Fabrication) identified:
* ArrivalTime
* ArrivalTimeController
* ArrivalTimeStore

## 3.2. Sequence Diagram (SD)

*In this section, it is suggested to present an UML dynamic view stating the sequence of domain related software objects' interactions that allows to fulfill the requirement.*

![US4-SD](US4_SD.svg)

## 3.3. Class Diagram (CD)

*In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods.*

![US4-CD](US4_CD.svg)

# 4. Test
NONE YET.
# 5. Construction (Implementation)
# 6. Integration and Demo
none.
# 7. Observations
*No observations*