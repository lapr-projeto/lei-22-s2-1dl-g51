# US 018 - List of all vaccines

## 1. Requirements Engineering

*In this US, we want you, as the center coordinator, to get a list of vaccines organized alphabetically and by type.For this to happen, we have to enter as a center coordinator and select the option to view the vaccines available at the vaccination center. The list of vaccines organized alphabetically and by type should appear.*


### 1.1. User Story Description

*As a center coordinator, I want to get a list of all vaccines.*

### 1.2. Customer Specifications and Clarifications 

*The vaccines should be grouped by their type and then
alphabetically listed by its name.*

### 1.3. Acceptance Criteria

*The vaccines should be grouped by their type and then
alphabetically listed by its name.*

### 1.4. Found out Dependencies

*US12 (As an administrator, I intend to specify a new vaccine type) and US13 (As an administrator, I intend to specify a new vaccine and its administration process)*

### 1.5 Input and Output Data

*Vaccines must be introduced through the work done at US's 12 and 13. First, the center coordinator must enter his/her credentials to request the vaccine list. When asking to see all vaccines, the center coordinator selects the option and the system shows the list organized by type and alphabetically*


### 1.6. System Sequence Diagram (SSD)



![USXXX-SSD](SSD_US18.svg)


### 1.7 Other Relevant Remarks

*The procedure that organizes the vaccines according to their type and alphabetically is not shown in the diagrams.*


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 


![USXXX-MD](DM_US18.svg)

### 2.2. Other Remarks

*The procedure that organizes the vaccines according to their type and alphabetically is not shown in the diagrams.*

## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1  		 |	... interacting with the actor?	|CenterCoordinatorMenuUI|Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model.|
| Step 2         |... coordinating the US?|CenterCoordinatorController| Controller|
| Step 3  		 |... instantiating a new Task?	     | Company            |                              |
| Step 4  		 |...Print Vaccines listed| Vaccine            |IE: owns its data.                              |
| Step 5  		 |... informing operation success?|CenterCoordinatorMenuUI|IE: is responsible for user interactions.                              |



### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * Company
 * VaccineType
 * Vaccine

Other software classes (i.e. Pure Fabrication) identified: 
 
* CenterCoordinatorMenuUI  
 * CenterCoordinatorController

## 3.2. Sequence Diagram (SD)

![USXXX-SD](SD_US18.svg)

## 3.3. Class Diagram (CD)

![USXXX-CD](CD_US18.svg)

# 4. Tests 

**Omitted / Not Provided.**

# 5. Construction (Implementation)

**Omitted / Not Provided.** 

# 6. Integration and Demo 

**Omitted / Not Provided.**

# 7. Observations

**Omitted / Not Provided.** 





